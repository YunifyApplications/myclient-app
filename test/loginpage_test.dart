// Flutter does not support using asset files in tests yet,
// so we are unable to validate possible error messages
// or values retrieved from a localization file.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/services/localization_service.dart';

Widget buildTestableWidget(Widget widget) {
  // https://docs.flutter.io/flutter/widgets/MediaQuery-class.html
  return new MediaQuery(
      data: new MediaQueryData(), child: new MaterialApp(home: widget));
}

void main() {
  loginSuccess();
  loginFailure();
  missingEmail();
  missingPassword();
}

void missingEmail() {
  testWidgets('missing email', (WidgetTester tester) async {
    final TestWidgetsFlutterBinding binding = tester.binding;
    if (binding is AutomatedTestWidgetsFlutterBinding)
      binding.addTime(const Duration(seconds: 5));

    // Build our app and trigger a frame.
    await tester.pumpWidget(buildTestableWidget(new MyClientApp()));
    LocalizationService.load();

    await tester.enterText(find.byKey(new Key('email_field')), '');

    await tester.tap(find.byKey(new Key('button_login')));
    await tester.pump(new Duration(seconds: 2));

    expect(find.text('Email is verplicht'), findsOneWidget);
  });
}

void missingPassword() {
  testWidgets('missing password', (WidgetTester tester) async {
    final TestWidgetsFlutterBinding binding = tester.binding;
    if (binding is AutomatedTestWidgetsFlutterBinding)
      binding.addTime(const Duration(seconds: 5));

    // Build our app and trigger a frame.
    await tester.pumpWidget(buildTestableWidget(new MyClientApp()));
    LocalizationService.load();

    await tester.enterText(find.byKey(new Key('password_field')), '');

    await tester.tap(find.byKey(new Key('button_login')));
    await tester.pump(new Duration(seconds: 2));

    expect(find.text('Wachtwoord is verplicht'), findsOneWidget);
  });
}

void loginFailure() {
  testWidgets('login failure', (WidgetTester tester) async {
    final TestWidgetsFlutterBinding binding = tester.binding;
    if (binding is AutomatedTestWidgetsFlutterBinding)
      binding.addTime(const Duration(seconds: 5));

    // Build our app and trigger a frame.
    await tester.pumpWidget(buildTestableWidget(new MyClientApp()));
    LocalizationService.load();

    await tester.enterText(find.byKey(new Key('email_field')), '1');

    await tester.tap(find.byKey(new Key('button_login')));
    await tester.pump(new Duration(seconds: 2));

    // Verify that we have not navigated
    expect(find.text('Ongeldige email/wachtwoord combinatie'), findsOneWidget);
  });
}

void loginSuccess() {
  testWidgets('login success', (WidgetTester tester) async {
    final TestWidgetsFlutterBinding binding = tester.binding;
    if (binding is AutomatedTestWidgetsFlutterBinding)
      binding.addTime(const Duration(seconds: 5));

    // Build our app and trigger a frame.
    await tester.pumpWidget(buildTestableWidget(new MyClientApp()));
    LocalizationService.load();

    await tester.tap(find.byKey(new Key('button_login')));
    await tester.pump(new Duration(seconds: 2));

    // Verify that we have navigated
    expect(find.text('Ruud Bruls'), findsOneWidget,
        reason: 'failed to navigate to dashboard');
  });
}
