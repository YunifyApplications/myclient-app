import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

class DownloadHelper {
  /// Holds a Future to a temporary cache directory.
  /// Returns the path of the cache directory.
  static final Future<String> cacheDirFuture = (() async {
    String cacheDir =
        (await getTemporaryDirectory()).path + '/flutter_pdf_viewer';
    await Directory(cacheDir).create();

    return cacheDir;
  })();

  /// Asynchronously checks whether or not the file [file] is invalid.
  /// Returns true of the file is invalid.
  static Future<bool> fileIsInvalid(File file) async =>
      (await file.stat()).type == FileSystemEntityType.notFound;

  /// Asynchronously downloads file from the url [url] into the cache directory.
  /// If the file is already present on disk and [cache] is true, the file retrieved from
  /// disk and not downloaded from the url.
  /// Returns the path on disk to the downloaded file.
  static Future<String> downloadAsFile(String url, {bool cache: true}) async {
    String filepath =
        '${await cacheDirFuture}/${sha1.convert(utf8.encode(url))}';
    File file = File(filepath);

    if (!cache || await fileIsInvalid(file))
      await file.writeAsBytes(await http.readBytes(url));

    return filepath;
  }

  /// Asynchronously reads all bytes from the given url [url].
  /// Returns a list of bytes.
  static Future<Uint8List> downloadAsBytes(String url) {
    return http.readBytes(url);
  }
}
