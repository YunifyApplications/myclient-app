import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/ehealth_dto.dart';

/// This class is responsible for providing useful helper functions for
/// converting strings into colors and converting expertise codes into colors.
class ColorHelper {
  /// Convert hex color string [colorStr] into a Color object.
  static Color fromHex(String colorStr) {
    try {
      colorStr = "FF" + colorStr;
      colorStr = colorStr.replaceAll("#", "");
      int val = 0;
      int len = colorStr.length;
      for (int i = 0; i < len; i++) {
        int hexDigit = colorStr.codeUnitAt(i);
        if (hexDigit >= 48 && hexDigit <= 57) {
          val += (hexDigit - 48) * (1 << (4 * (len - 1 - i)));
        } else if (hexDigit >= 65 && hexDigit <= 70) {
          // A..F
          val += (hexDigit - 55) * (1 << (4 * (len - 1 - i)));
        } else if (hexDigit >= 97 && hexDigit <= 102) {
          // a..f
          val += (hexDigit - 87) * (1 << (4 * (len - 1 - i)));
        } else {
          throw new FormatException(
              "An error occurred when converting a color");
        }
      }

      return Color(val);
    } catch (ex) {
      return Colors.red;
    }
  }

  /// Converts expertise code [code] into the hex representation of the corresponding color.
  static String expertiseCodeToHex(String code) {
    switch (code) {
      case "FT":
        return "#60C4F8"; //blue
      case "PS":
        return "#2ecc71"; //green
      case "CGT":
        return "#e74c3c"; //red
      case "OV":
        return "#e67e22"; //orange
      case "CSM":
        return "#9b59b6"; //purple
      case "HB":
        return "#d35400"; //dark orange
      case "DDE":
        return "#16a085"; //lime
      case "":
        return "#FFFFFF"; //white
      default:
        return "#F9F8F8";
    }
  }

  /// Converts expertise code [code] into the Color representation of the corresponding color.
  static Color expertiseCodeToColor(String code) {
    switch (code) {
      case "FT":
        return fromHex("#60C4F8"); //blue
      case "PS":
        return fromHex("#2ecc71"); //green
      case "CGT":
        return fromHex("#e74c3c"); //red
      case "OV":
        return fromHex("#e67e22"); //orange
      case "CSM":
        return fromHex("#9b59b6"); //purple
      case "HB":
        return fromHex("#d35400"); //dark orange
      case "DDE":
        return fromHex("#16a085"); //lime
      case "":
        return fromHex("#FFFFFF"); //white
      default:
        return fromHex("#F9F8F8");
    }
  }

  /// Get the corresponding background color for the given WorkflowState [state]
  static Color getBackgroundColorForWorkflowState(WorkflowState state) {
    if (state == WorkflowState.Completed) {
      return new Color.fromARGB(255, 196, 231, 196);
    } else if (state == WorkflowState.Closed) {
      return new Color.fromARGB(255, 244, 230, 230);
    }

    return new Color.fromARGB(255, 245, 245, 245);
  }

  /// Get the corresponding color for the given WorkflowState [state]
  static Color getColorForWorkflowState(WorkflowState state) {
    if (state == WorkflowState.WaitForClient) {
      return new Color.fromARGB(255, 196, 231, 196);
    } else if (state == WorkflowState.WaitForUserApproval) {
      return new Color.fromARGB(255, 252, 223, 182);
    } else if (state == WorkflowState.Completed) {
      return new Color.fromARGB(255, 196, 231, 196);
    } else if (state == WorkflowState.Closed) {
      return new Color.fromARGB(255, 244, 230, 230);
    }
    return Colors.red;
  }
}
