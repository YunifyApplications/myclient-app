import 'package:myclient_app/dtos/dashboard_info_dto.dart';

/// This class is responsible for storing the information displayed on the dashboard page.
class DashboardHelper {
  /// is set the first time the dashboard is loaded and is stored here so other
  /// modules can update their open item count.
  static DashboardInfoDTO dashboardInfo;
  static bool isFirstTime = false;
}
