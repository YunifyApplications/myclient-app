import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/dtos/program_dto.dart';
import 'package:myclient_app/dtos/results_dto.dart';

/// This class is responsible providing useful formatting functions.
class FormatHelper {
  static List<String> monthNames = [
    'Januari',
    'Februari',
    'Maart',
    'April',
    'Mei',
    'Juni',
    'Juli',
    'Augustus',
    'September',
    'Oktober',
    'November',
    'December'
  ];

  static List<String> dayNames = [
    'Maandag',
    'Dinsdag',
    'Woensdag',
    'Donderdag',
    'Vrijdag',
    'Zaterdag',
    'Zondag',
  ];

  /// Format the name [name] into a shorter representation of the same name.
  /// eg. Hans Klok turns into 'H. Klok'.
  static String formatName(String name) {
    var words = name.split(' ');
    String formattedName = '';

    int i = 0;
    for (var word in words) {
      if (i == 0) {
        formattedName += word.substring(0, 1) + '.';
      } else {
        formattedName += ' ' + word;
      }

      i++;
    }

    return formattedName;
  }

  /// Formats the datetime [time] into a string representation of the given date without the year.
  /// eg. 19-02-2018 turns into 'Dinsdag 19 februari'.
  static String formatDate(DateTime time) {
    var month = monthNames[time.month - 1].toLowerCase();
    var day = time.day;
    var dayName = dayNames[time.weekday - 1];
    return "$dayName $day $month";
  }

  /// Formats the datetime [time] into a string representation of the given date.
  /// eg. 19-2-2018 turns into '19 februari 2018'.
  static String formatDateNumerical(DateTime time) {
    var month = time.month;
    var day = time.day;
    var year = time.year;
    return "$day-$month-$year";
  }

  /// Formats the datetime [time] into a string representation of the given time.
  /// eg. 19-2-2018 17:40:12 turns into '17:40 uur'
  static String formatTime(DateTime time) {
    var h = time.hour;
    var m = time.minute;

    if (m < 10) return "$h:0$m uur";

    return "$h:$m uur";
  }

  /// Formats the datetime [time] into a short string representation of the datetime.
  /// if the datetime difference between [time] and now is below 24 hours it returns a string representation of the time.
  /// if the datetime difference  between [time] and now is below 48 hours it returns 'GISTEREN'.
  /// else it returns a string representation of the date in format: dd-mm-yy.
  static String formatTimeShort(DateTime time) {
    var diff = time.difference(DateTime.now());

    var h = time.hour;
    var m = time.minute;
    var d = time.day;
    var mo = time.month;
    var y = time.year;

    var yearStr = y.toString().substring(2, 4);

    if (diff.inDays > -1) {
      if (h < 10 && m < 10) return '0$h:0$m';
      if (h < 10) return '0$h:$m';
      if (m < 10) return '$h:0$m';

      return '$h:$m';
    }

    if (diff.inDays > -2) {
      return 'GISTEREN';
    }

    if (d < 10 && mo < 10) return '0$d-0$mo-$yearStr';
    if (d < 10) return '0$d-$mo-$yearStr';
    if (mo < 10) return '$d-0$mo-$yearStr';

    return '$d-$mo-$yearStr';
  }

  /// Formats the file extension of the fileData in [lib].
  static String formatProgramLibFileExtension(ProgramLib lib) {
    if (lib.fileData == null)
      return 'HTML';
    else
      return lib.fileData.extension.replaceAll('.', '').toUpperCase();
  }

  /// Formats the type of the given ExerciseLib [lib].
  static String formatExerciseLibFileExtension(ExerciseLib lib) {
    if (lib.isForm) {
      return 'INVULOEFENING';
    } else if (lib.isYoutube || lib.isVimeo || lib.url != null) {
      return 'VIDEO';
    } else if (lib.fileData != null) {
      return lib.fileData.extension.replaceAll('.', '').toUpperCase();
    }
    return '';
  }

  /// Formats the given module number [number] into the string representation,
  /// if the number is below 10 an extra 0 is added for padding.
  static String formatModuleNumber(int number) {
    if (number < 10) {
      return "0" + number.toString();
    } else {
      return number.toString();
    }
  }

  /// Get the version number in string representation from the given program title [title].
  static String getVersionFromProgramName(String title) {
    var split = title.trim().split(' ');
    return split[0];
  }

  /// Formats the type of the given TestResult [result] into a string representation.
  static String formatResultTypeString(TestResult result) {
    if (result.isPdf) {
      return 'PDF';
    } else {
      return 'GRAFIEK';
    }
  }
}
