/// This class is responsible for storing user data that can be used
/// across the application.
class UserHelper {
  static String avatarUrl;
  static String displayName;
  static String userId;
  static String authToken;

  static void clear() {
    avatarUrl = '';
    displayName = '';
    userId = '';
    authToken = '';
  }
}
