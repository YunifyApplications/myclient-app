import 'package:myclient_app/dtos/dashboard_info_dto.dart';

class DashboardInfoRetrievedEvent {
  final DashboardInfoDTO result;

  DashboardInfoRetrievedEvent(this.result);
}
