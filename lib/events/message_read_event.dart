class MessageReadEvent {
  final int messageId;
  final String recipientId;

  MessageReadEvent(this.messageId, this.recipientId);
}
