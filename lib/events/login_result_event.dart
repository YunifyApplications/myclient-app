import 'package:myclient_app/dtos/login_result_dto.dart';

class LoginResultEvent {
  final String errorMessage;
  final LoginResultDTO result;

  const LoginResultEvent(this.result, this.errorMessage);
}