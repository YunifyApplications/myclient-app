import 'package:myclient_app/dtos/message_dto.dart';

class MessageSentEvent {
  final Message message;

  MessageSentEvent(this.message);
}
