enum RequestResult { SUCCESS, SERVER_ERROR, UNAUTHORIZED }

class SessionCollectionDTO {
  final RequestResult resultStatus;
  final List<Session> sessions;

  SessionCollectionDTO(this.resultStatus, this.sessions);
}

class Session {
  final String name;
  final int id;

  final String treatmentOfficerName;
  final String treatmentOfficerAvatarUrl;
  final String treatmentOfficerId;

  final int status;
  final String description;
  final String location;
  final int type;

  final DateTime appointmentStartTime;
  final DateTime appointmentEndTime;

  final String expertiseName;
  final String expertiseCode;

  final bool allDay;

  Session(
      this.name,
      this.id,
      this.treatmentOfficerName,
      this.treatmentOfficerAvatarUrl,
      this.treatmentOfficerId,
      this.status,
      this.description,
      this.location,
      this.type,
      this.appointmentStartTime,
      this.appointmentEndTime,
      this.expertiseName,
      this.expertiseCode,
      this.allDay);
}
