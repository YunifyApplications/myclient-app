import 'package:myclient_app/dtos/session_dto.dart';

class ProgramCollectionDTO {
  final List<Program> programs;
  final RequestResult resultStatus;

  ProgramCollectionDTO({this.programs, this.resultStatus});
}

class Program {
  bool done;
  final int id;
  final int programLibId;
  final ProgramLib programLib;
  final String memo;
  final int assignedHtmlContentId;

  Program(
      {this.done,
      this.id,
      this.programLibId,
      this.programLib,
      this.memo,
      this.assignedHtmlContentId});
}

class ProgramLib {
  int id;
  String name;
  String description;
  int imageDataId;
  BinaryData imageData;
  int fileDataId;
  BinaryData fileData;
  int expertiseId;
  Expertise expertise;
  int subjectId;
  Subject subject;
  int sectionId;
  Section section;
  int htmlContentId;
  HtmlContent htmlContent;
  int moduleNumber;
  ProgramLib({
    this.id,
    this.name,
    this.description,
    this.imageDataId,
    this.imageData,
    this.fileDataId,
    this.fileData,
    this.expertiseId,
    this.expertise,
    this.subjectId,
    this.subject,
    this.sectionId,
    this.section,
    this.htmlContentId,
    this.htmlContent,
    this.moduleNumber,
  });
}

class BinaryData {
  int id;
  String fileName;
  String extension;
  String idFile;
  String retrieveUrl;

  BinaryData(
      {this.id, this.fileName, this.extension, this.idFile, this.retrieveUrl});
}

class Section {
  int id;
  String name;
  String description;
  bool isProgram;
  bool isExercise;
  bool isSurvey;

  Section(
      {this.id,
      this.name,
      this.description,
      this.isProgram,
      this.isExercise,
      this.isSurvey});
}

class Subject {
  int id;
  String name;
  String description;
  bool isProgram;
  bool isExercise;
  bool isSurvey;

  Subject(
      {this.id,
      this.name,
      this.description,
      this.isProgram,
      this.isExercise,
      this.isSurvey});
}

class Expertise {
  int id;
  String name;
  String code;
  String description;
  bool mustBookCareContact;

  Expertise(
      {this.id,
      this.name,
      this.code,
      this.description,
      this.mustBookCareContact});
}

class HtmlContent {
  int id;
  String title;
  String html;
  String description;
  String code;
  int version;

  HtmlContent(
      {this.id,
      this.title,
      this.html,
      this.description,
      this.code,
      this.version});
}
