enum LoginResult { SUCCESS, FAILURE, SERVER_ERROR }

class LoginResultDTO {
  final LoginResult result;
  final String authToken;

  const LoginResultDTO(this.result, this.authToken);
}
