import 'package:myclient_app/dtos/session_dto.dart';

class ProfileDTO {
  final RequestResult resultStatus;
  final String userName;
  final String firstName;
  final String lastName;
  final DateTime birthDate;
  final String gender;

  String email;
  String phoneNumber;
  bool receiveWeekOverviewPlannedAppointments;
  bool receiveEmail24HoursBeforeAppointment;
  bool receiveSms24HoursBeforeAppointment;
  bool receiveGeneralEmailNotifications;

  ProfileDTO(
      {this.userName,
      this.firstName,
      this.lastName,
      this.birthDate,
      this.gender,
      this.email,
      this.phoneNumber,
      this.receiveWeekOverviewPlannedAppointments,
      this.receiveEmail24HoursBeforeAppointment,
      this.receiveSms24HoursBeforeAppointment,
      this.receiveGeneralEmailNotifications,
      this.resultStatus});
}
