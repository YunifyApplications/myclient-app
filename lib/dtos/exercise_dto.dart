import 'ehealth_dto.dart';
import 'program_dto.dart';
import 'session_dto.dart';

// exercise can be:
// -  youtube video
// -  vimeo video
// -  survey (html)
// -  pdf
// -  docx
// -  mp3

// exercise only has to be handed in if it is a survey
// if the url is specified but isYoutube and isVimeo is false then it is a youtube url (???)
// if it is a pdf/docx/mp3 it can be retrieved with fileData

// question has a optionGroup which contains the possible answers for specific question types.

class ExerciseCollectionDTO {
  final List<Exercise> exercises;
  final RequestResult resultStatus;

  ExerciseCollectionDTO({this.exercises, this.resultStatus});
}

class SaveQuestionAnswerResultDTO {
  final RequestResult resultStatus;

  SaveQuestionAnswerResultDTO({this.resultStatus});
}

class FinishExerciseDTO {
  int exerciseLibId;

  FinishExerciseDTO({this.exerciseLibId});
}

class FinishExerciseResultDTO {
  final RequestResult resultStatus;

  FinishExerciseResultDTO({this.resultStatus});
}

enum FinishSurveyResult { SUCCESS, FAILURE }

class FinishSurveyResultDTO {
  final String errorMessage;
  final RequestResult resultStatus;
  final FinishSurveyResult finishResult;

  FinishSurveyResultDTO(
      this.errorMessage, this.resultStatus, this.finishResult);
}

class FinishSurveyDTO {
  final int surveyLibId;

  FinishSurveyDTO({this.surveyLibId});
}

class SaveQuestionAnswerDTO {
  final int questionId;
  final String answer;

  SaveQuestionAnswerDTO({this.questionId, this.answer});
}

class SaveSurveyTakeDurationDTO {
  final int surveyLibId;
  final int duration;

  SaveSurveyTakeDurationDTO({this.surveyLibId, this.duration});
}

class StartSurveyDTO {
  final int surveyId;

  StartSurveyDTO({this.surveyId});
}

class Exercise {
  int id;
  int exerciseLibId;
  ExerciseLib exerciseLib;
  int sessionId;
  Session session;
  bool reminder;
  DateTime reminderDate;
  bool done; // exercise should be readonly when done
  DateTime dateDone;
  String memo;

  Exercise(
      {this.id,
      this.exerciseLibId,
      this.exerciseLib,
      this.sessionId,
      this.session,
      this.reminder,
      this.reminderDate,
      this.done,
      this.dateDone,
      this.memo});
}

class ExerciseLib {
  int id;
  String name;
  String description;
  int subjectId;
  Subject subject;
  int sectionId;
  Section section;
  String url;
  bool isYoutube;
  bool isVimeo;
  int imageDataId;
  BinaryData imageData;
  int fileDataId;
  BinaryData fileData; // /api/exercise/file/{item.Id} url for retrieving data
  int expertiseId;
  Expertise expertise;
  int moduleNumber;
  String treatmentOfficerId;
  DateTime archived;
  String archivedById;
  bool emailNotification;
  bool isForm;
  Survey survey;
  WorkflowState currentWorkflowState;

  ExerciseLib(
      {this.id,
      this.name,
      this.description,
      this.subjectId,
      this.subject,
      this.sectionId,
      this.section,
      this.url,
      this.isYoutube,
      this.isVimeo,
      this.imageDataId,
      this.imageData,
      this.fileDataId,
      this.fileData,
      this.expertiseId,
      this.expertise,
      this.moduleNumber,
      this.treatmentOfficerId,
      this.archived,
      this.archivedById,
      this.emailNotification,
      this.isForm,
      this.survey,
      this.currentWorkflowState});
}

class Survey {
  int id;
  SurveyLib surveyLib;
  bool releaseScoreToClient;
  DateTime startedOn;
  int progressInSeconds;
  String conclusion;

  Survey(
      {this.id,
      this.surveyLib,
      this.releaseScoreToClient,
      this.startedOn,
      this.progressInSeconds,
      this.conclusion});
}

class SurveyLib {
  int id;
  String name;
  String code;
  String description;
  Expertise expertise;
  String instructions;
  int durationInMinutes; // null = unlimited time
  bool isForm; // important or not?
  List<SurveySection> sections;

  SurveyLib(
      {this.id,
      this.name,
      this.code,
      this.description,
      this.expertise,
      this.instructions,
      this.durationInMinutes,
      this.isForm,
      this.sections});
}

class SurveySection {
  int id;
  String name;
  String description; // the title that should be displayed, can be null
  String
      otherHeaderInfo; // the extra info that should be displayed, can be null
  List<Question> questions;
  int sortOrder;

  SurveySection(
      {this.id,
      this.name,
      this.description,
      this.otherHeaderInfo,
      this.questions,
      this.sortOrder}); // should be sorted before displayed

}

class Question {
  int id;
  String name; // the question
  String code;
  String questionNumber;
  int sortOrder; // should be sorted before displayed
  bool required; // null == false
  List<QuestionSubClass> questionSubclasses;
  OptionGroup optionGroup; // can be null
  QuestionType questionType; // should not be null
  QuestionTypeGroup questionTypeGroup; // can be null
  int selectedValue; // Used to store the selected answer !!!!!---ID----!!!!
  String answerText; // Used to store the text of an answer temporary
  double trackbarValue;
  Question
      parentQuestion; // some questions should only be displayed when the parent question has been answered
  OptionChoice parentOptionChoice; // can be null
  List<DataLocalization> dataLocalizationJsonCollection;

  Question(
      {this.id,
      this.name,
      this.code,
      this.questionNumber,
      this.sortOrder,
      this.required,
      this.questionSubclasses,
      this.optionGroup,
      this.questionType,
      this.questionTypeGroup,
      this.selectedValue,
      this.answerText,
      this.trackbarValue,
      this.parentQuestion,
      this.parentOptionChoice,
      this.dataLocalizationJsonCollection});
}

class QuestionSubClass {
  int id;
  int subClassId;

  QuestionSubClass({this.id, this.subClassId});
}

class OptionGroup {
  int id;
  String name;
  String description;
  List<OptionChoice> optionChoices;

  OptionGroup({this.id, this.name, this.description, this.optionChoices});
}

class QuestionType {
  int id;
  String name;
  List<QuestionTypeProperty> questionTypeProperties;

  QuestionType({this.id, this.name, this.questionTypeProperties});
}

class QuestionTypeProperty {
  int id;
  String name;

  QuestionTypeProperty({this.id, this.name});
}

class QuestionTypeGroup {
  int id;
  String name;
  String description;
  List<QuestionTypePropertyValue> questionTypePropertyValues;

  QuestionTypeGroup(
      {this.id, this.name, this.description, this.questionTypePropertyValues});
}

class QuestionTypePropertyValue {
  int id;
  String value;
  QuestionTypeProperty questionTypeProperty;

  QuestionTypePropertyValue({
    this.id,
    this.value,
    this.questionTypeProperty,
  });
}

class OptionChoice {
  int id;
  int sortOrder;
  String name;
  int value;
  List<DataLocalization> dataLocalizationJsonCollection;

  OptionChoice(
      {this.id,
      this.sortOrder,
      this.name,
      this.value,
      this.dataLocalizationJsonCollection});
}

class DataLocalization {
  String key;
  String value;
  String language;

  DataLocalization({this.key, this.value, this.language});
}
