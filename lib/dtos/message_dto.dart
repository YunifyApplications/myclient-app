import 'package:myclient_app/dtos/session_dto.dart';

class MessageCollectionDTO {
  final RequestResult resultStatus;
  final List<Message> messages;
  final List<Recipient> availableRecipients;

  MessageCollectionDTO(
      this.resultStatus, this.messages, this.availableRecipients);
}

class Message {
  int id;
  final String messageBody;

  final String subject;

  final int parentId;
  final Message parent;

  final DateTime sendDate;
  final String senderId;
  final String senderAvatarUrl;
  final String senderDisplayName;

  final bool sentToTeam;
  final List<Recipient> recipients;

  Message(
      {this.id,
      this.messageBody,
      this.subject,
      this.parentId,
      this.parent,
      this.sendDate,
      this.senderId,
      this.senderAvatarUrl,
      this.senderDisplayName,
      this.sentToTeam,
      this.recipients});
}

class Recipient {
  final String receiverId;
  final String displayName;
  final String avatarUrl;
  bool isRead;

  Recipient({this.receiverId, this.displayName, this.avatarUrl, this.isRead});
}
