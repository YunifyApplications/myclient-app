
class LoginAttemptDTO {
  final String email;
  final String password;

  const LoginAttemptDTO(this.email, this.password);
}