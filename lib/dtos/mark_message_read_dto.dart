class MarkMessageReadDTO {
  final int messageId;
  final String recipientId;

  MarkMessageReadDTO(this.messageId, this.recipientId);
}
