import 'package:myclient_app/dtos/session_dto.dart';

class EhealthModsCollection {
  final List<EhealthModsViewModel> ehealthMods;
  final RequestResult resultStatus;

  EhealthModsCollection({this.ehealthMods, this.resultStatus});
}

enum WorkflowState {
  // Is used to initialize enum
  NoState,

  /// <summary>
  /// An empty Ehealth module means that an Ehealth module exists for the client but has not been filled by the Treatmentofficer. Only the treatment officer has rights to fill the Module.
  /// </summary>
  Empty,

  /// <summary>
  /// The treatment officer is now assigned to the Ehealth module. The related Ehealth module has been partial (or is no) filled by the user.
  /// </summary>
  WaitForUser,

  /// <summary>
  /// An Ehealth module has been filled by the user and is assigned to client. The client has now the rights to give answers in the Module.
  /// </summary>
  WaitForClient,

  /// <summary>
  /// An Ehealth module has been opened filled/answered by the client and is waiting for an approval.
  /// </summary>
  WaitForUserApproval,

  /// <summary>
  /// An Ehealth module has been closed due several reasons like; client didn't respond to the Ehealth module within given time, client stopped treatment etc.
  /// </summary>
  Closed,

  /// <summary>
  /// An Ehealth module has been finished successfully and is therefore closed by the treatment officer.
  /// </summary>
  Completed
}

class EhealthModsViewModel {
  final int id;
  final String ehealthModLibName;
  final String moduleOrMoment;
  final String number;
  final String ehealthModLibDescription;
  final DateTime startedOn;
  final DateTime completedOn;
  final DateTime closedOn;
  final WorkflowState workflowState;
  final DateTime appointmentTime;
  final int messageCount;
  final String url;

  EhealthModsViewModel(
      {this.id,
      this.ehealthModLibName,
      this.moduleOrMoment,
      this.number,
      this.ehealthModLibDescription,
      this.startedOn,
      this.completedOn,
      this.closedOn,
      this.workflowState,
      this.appointmentTime,
      this.messageCount,
      this.url});
}

class EhealthModViewModel {
  final int id;
  final bool isUser;
  final bool actAsClient;
  final bool disableInput;
  final List<EhealthModMessageViewModel> messages;
  final List<PlaceholderViewModel> placeholders;
  final WorkflowState currentWorkflowState;
  final ApplicationUser sender;
  final String workflowState;

  EhealthModViewModel(
      {this.id,
      this.isUser,
      this.actAsClient,
      this.disableInput,
      this.messages,
      this.placeholders,
      this.currentWorkflowState,
      this.sender,
      this.workflowState});
}

class EhealthModMessageViewModel {
  final int id;
  final int ehealthModId;
  final DateTime timestamp;
  final String timestampAsShortDateString;
  final bool isUser;
  final ApplicationUser sender;
  final String text;
  final bool isRead;

  EhealthModMessageViewModel(
      {this.id,
      this.ehealthModId,
      this.timestamp,
      this.timestampAsShortDateString,
      this.isUser,
      this.sender,
      this.text,
      this.isRead});
}

class PlaceholderViewModel {
  final int id;
  final int ehealthModId;
  final String text;
  final String answer;
  final String placeHolderDisplayName;
  final String placeHolderTypeCode;
  final bool isEditable;
  final bool isUser;
  final int sortOrder;
  final bool hasParentPlaceholder;
  final int surveyLibId;
  final int surveyId;
  final String surveyLibName;
  final int sessionId;
  final String buttonText;

  PlaceholderViewModel(
      {this.id,
      this.ehealthModId,
      this.text,
      this.answer,
      this.placeHolderDisplayName,
      this.placeHolderTypeCode,
      this.isEditable,
      this.isUser,
      this.sortOrder,
      this.hasParentPlaceholder,
      this.surveyLibId,
      this.surveyId,
      this.surveyLibName,
      this.sessionId,
      this.buttonText});
}

class ApplicationUser {
  final String id;
  final String firstName;
  final String lastName;
  final String surnamePrefix;
  final String displayName;
  final int officeId;
  final int genderId;
  final DateTime birthDate;
  final int psn;

  ApplicationUser(
      {this.id,
      this.firstName,
      this.lastName,
      this.surnamePrefix,
      this.displayName,
      this.officeId,
      this.genderId,
      this.birthDate,
      this.psn});
}
