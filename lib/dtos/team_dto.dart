import 'package:myclient_app/dtos/session_dto.dart';

class TeamDTO {
  final List<TeamMember> members;
  final RequestResult resultStatus;

  TeamDTO(this.members, this.resultStatus);
}

class TeamMember {
  final String displayName;
  final String userId;
  final String expertiseDescription;
  final String expertiseDisplayName;
  final String avatarUrl;

  TeamMember(
      {this.displayName,
      this.userId,
      this.expertiseDescription,
      this.expertiseDisplayName,
      this.avatarUrl});
}
