import 'package:myclient_app/dtos/session_dto.dart';

class SendMessageResultDTO {
  final int messageId;
  final RequestResult resultStatus;

  SendMessageResultDTO(this.messageId, this.resultStatus);
}
