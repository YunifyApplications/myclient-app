import 'package:myclient_app/dtos/session_dto.dart';

// test results can be:
// (1) (RUG) ClinimetryDashboard (charts)
// (1) (RUG) VitalityDashboard (charts)
// (n) (RUG) Gevolgmodellen (info page)
// (1) Life Style Plan (PDF)
// (1) Resultaten en conclusies (PDF)
// (n) David test (PDF) named as 'David' + test type
// (n) Resultaten en conclusies snapshot (PDF) named as 'Resultaten en conclusies snapshot ' + date created

class ResultCollectionDTO {
  final List<TestResult> results;
  final RequestResult resultStatus;

  ResultCollectionDTO(this.results, this.resultStatus);
}

class TestResult {
  String name;
  String url; // contains the url to retrieve PDF files.
  bool isPdf;

  TestResult({this.name, this.url, this.isPdf});
}
