import 'dart:core';

import 'package:myclient_app/dtos/session_dto.dart';

class DashboardInfoDTO {
  String avatarUrl;
  String displayName;
  String email;
  String userId;

  double messageCount;
  double resultCount;
  double appointmentCount;
  double programCount;
  double exerciseCount;
  double ehealthCount;

  RequestResult resultStatus;

  DashboardInfoDTO(
      {this.avatarUrl,
      this.displayName,
      this.email,
      this.userId,
      this.messageCount,
      this.resultCount,
      this.appointmentCount,
      this.programCount,
      this.exerciseCount,
      this.ehealthCount,
      this.resultStatus});
}
