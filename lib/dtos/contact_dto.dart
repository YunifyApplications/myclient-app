import 'package:myclient_app/dtos/session_dto.dart';

class ContactDTO {
  final String phoneNumber;
  final String address;
  final RequestResult resultStatus;

  ContactDTO({this.phoneNumber, this.address, this.resultStatus});
}
