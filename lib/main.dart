import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:myclient_app/pages/login_page/login_page.dart';
import 'package:myclient_app/services/ihttp_service.dart';
import 'package:myclient_app/services/localization_service.dart';
import 'package:myclient_app/services/mock_http_service.dart';

void main() => runApp(new MyClientApp());

EventBus eventBus = new EventBus();

// TODO: implement real http service
// TODO: google authenticator + extra inlogbeveiliging checkbox op profile pagina.

/// services
IHTTPService httpService = new MockHTTPService();

class MyClientApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /// create services
    httpService = new MockHTTPService();

    return new MaterialApp(
      title:
          LocalizationService.tenant == Tenant.RUG ? 'Mijn Rughuis' : 'MijnPHI',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new LoginPage(),
    );
  }
}
