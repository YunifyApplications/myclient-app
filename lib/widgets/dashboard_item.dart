import 'package:flutter/material.dart';
import 'package:myclient_app/utils/color_helper.dart';

class DashboardItem extends StatelessWidget {
  final String itemTextColor;
  final String itemBackgroundColor;
  final double itemRadius;
  final String iconUrl;
  final String text;
  final VoidCallback onTap;
  final double itemCount;
  final String itemNotificationColor;

  DashboardItem(
      {Key key,
      @required this.itemTextColor,
      @required this.itemBackgroundColor,
      @required this.itemRadius,
      @required this.text,
      @required this.onTap,
      @required this.itemCount,
      @required this.iconUrl,
      @required this.itemNotificationColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.width * 0.0133,
        left: MediaQuery.of(context).size.width * 0.011,
      ),
      width: MediaQuery.of(context).size.width * 0.319,
      height: MediaQuery.of(context).size.width * 0.319,
      child: new RaisedButton(
        elevation: 0.0,
        highlightElevation: 2.0,
        onPressed: onTap,
        disabledColor: new Color.fromARGB(255, 220, 220, 220),
        color: ColorHelper.fromHex(itemBackgroundColor),
        padding: EdgeInsets.all(0.0),
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(itemRadius)),
        child: new Column(
          children: <Widget>[
            new Stack(
              children: <Widget>[
                // item count
                new Visibility(
                  visible: is_visible(itemCount),
                  child: new Padding(
                    padding: const EdgeInsets.only(right: 7.0, top: 5.0),
                    child: new Align(
                      alignment: Alignment.topRight,
                      child: new Container(
                        width: 25.0,
                        height: 25.0,
                        decoration: new BoxDecoration(
                          color: ColorHelper.fromHex(itemNotificationColor),
                          shape: BoxShape.circle,
                          border: new Border.all(
                            color: ColorHelper.fromHex(itemNotificationColor),
                            width: 1.0,
                          ),
                        ),
                        child: new Center(
                          child: new Text(
                            itemCount.truncate().toString(),
                            style: new TextStyle(
                                color: Colors.white, fontSize: 14.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.04),
                  child: new Align(
                    child: new Image.asset(
                      'assets/icons/' + iconUrl,
                      width: MediaQuery.of(context).size.width * 0.13,
                      height: MediaQuery.of(context).size.width * 0.13,
                    ),
                  ),
                ),
              ],
            ),
            new Container(
              margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 3.0),
              child: new Text(text,
                  textAlign: TextAlign.center,
                  style:
                      new TextStyle(color: ColorHelper.fromHex(itemTextColor))),
            ),
          ],
        ),
      ),
    );
  }

  bool is_visible(double val) {
    return val > 0;
  }
}
