import 'package:flutter/material.dart';

class BorderedTextInput extends StatefulWidget {
  final String placeHolder;
  final bool isPassword;
  final TextEditingController controller;
  final bool fullBorder;

  BorderedTextInput(
      {Key key,
      @required this.placeHolder,
      @required this.isPassword,
      @required this.controller,
      @required this.fullBorder})
      : super(key: key);

  @override
  _BorderedTextInput createState() => new _BorderedTextInput(
      placeHolder: placeHolder,
      isPassword: isPassword,
      controller: controller,
      fullBorder: fullBorder);
}

class _BorderedTextInput extends State<BorderedTextInput> {
  final String placeHolder;
  final bool isPassword;
  final TextEditingController controller;
  final bool fullBorder;

  _BorderedTextInput(
      {this.placeHolder,
      this.isPassword,
      @required this.controller,
      @required this.fullBorder});

  @override
  Widget build(BuildContext context) {
    if (!fullBorder) {
      return new Container(
        padding: const EdgeInsets.all(12.0),
        alignment: Alignment.center,
        height: 45.0,
        decoration: new BoxDecoration(
          color: Colors.transparent,
          border: new Border(
            bottom: new BorderSide(color: Colors.white, width: 3.0),
          ),
        ),
        child: new TextField(
          controller: controller,
          style: new TextStyle(
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.w300,
              fontFamily: "Roboto",
              decoration: TextDecoration.none,
              fontStyle: FontStyle.italic),
          autocorrect: false,
          cursorColor: Colors.white,
          obscureText: widget.isPassword,
          decoration: new InputDecoration.collapsed(
            hintText: widget.placeHolder,
            hintStyle: new TextStyle(
              fontSize: 18.0,
              color: Colors.white,
              fontWeight: FontWeight.w200,
              fontFamily: "Roboto",
            ),
          ),
        ),
      );
    } else {
      return new Container(
        padding: const EdgeInsets.all(12.0),
        alignment: Alignment.center,
        height: 45.0,
        decoration: new BoxDecoration(
          borderRadius: BorderRadius.circular(4.0),
          color: Colors.transparent,
          border: new Border.all(
            color: Colors.white,
            width: 2.0,
          ),
        ),
        child: new TextField(
          controller: controller,
          style: new TextStyle(
              fontSize: 16.0,
              color: Colors.white,
              fontWeight: FontWeight.w300,
              fontFamily: "Roboto",
              decoration: TextDecoration.none,
              fontStyle: FontStyle.italic),
          autocorrect: false,
          cursorColor: Colors.white,
          obscureText: widget.isPassword,
          decoration: new InputDecoration.collapsed(
            hintText: widget.placeHolder,
            hintStyle: new TextStyle(
              fontSize: 16.0,
              color: Colors.white,
              fontWeight: FontWeight.w200,
              fontFamily: "Roboto",
            ),
          ),
        ),
      );
    }
  }
}
