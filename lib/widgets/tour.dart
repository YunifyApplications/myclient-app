import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Highlight {
  final String text;
  final Rect highlightArea;
  final Offset textOffset;
  final double textWidth;

  Highlight(this.text, this.highlightArea, this.textOffset, this.textWidth);
}

class TourPainter extends CustomPainter {
  static int maxTourProgress;
  int tourProgress = 0;
  double itemRadius;
  double itemOffsetTop = 0.0;
  Size size;
  double tapImageOffsetY;
  ui.Image tapImage;

  String tourIntro;
  String tourSessions;
  String tourMessages;
  String tourResults;
  String tourPrograms;
  String tourExercises;
  String tourTeam;
  String tourEhealth;
  String tourProfile;
  String tourContact;
  String tourMenu;
  String tapToContinue;
  String tapToComplete;

  List<Highlight> highlights = new List();

  TourPainter(
      {@required this.tourProgress,
      @required this.size,
      @required this.itemOffsetTop,
      @required this.itemRadius,
      @required this.tapImage,
      @required this.tapImageOffsetY,
      @required this.tourIntro,
      @required this.tourSessions,
      @required this.tourMessages,
      @required this.tourResults,
      @required this.tourPrograms,
      @required this.tourExercises,
      @required this.tourTeam,
      @required this.tourEhealth,
      @required this.tourProfile,
      @required this.tourContact,
      @required this.tourMenu,
      @required this.tapToContinue,
      @required this.tapToComplete}) {
    createHighlights();
  }

  void createHighlights() {
    double topInfoBarHeight = 64.0 + 28.0;

    double marginLeft = size.width * 0.011;
    double marginTop = size.width * 0.0133;
    double width = size.width * 0.319;
    double height = size.width * 0.319;

    double offsetX = marginLeft;
    double offsetY = marginTop + topInfoBarHeight + itemOffsetTop;

    double col1x = (size.width / 2) + (size.width / 5);
    double col3x = (size.width / 2) - (size.width / 5);

    Highlight intro = new Highlight(
        tourIntro,
        new Rect.fromLTWH(0.0, 0.0, 0.0, 0.0),
        new Offset(size.width / 2, 80.0),
        size.width - 50.0);

    Highlight sessions = new Highlight(
        tourSessions,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset(col1x, offsetY - 20.0),
        (size.width / 2.5));

    offsetX += width + marginLeft;
    Highlight messages = new Highlight(
        tourMessages,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset((size.width / 2), offsetY + height + marginTop + 30.0),
        (size.width - 100.0));

    offsetX += width + marginLeft;
    Highlight results = new Highlight(
        tourResults,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset(col3x, offsetY),
        (size.width / 2.5));

    offsetX = marginLeft;
    offsetY += height + marginTop;
    Highlight programs = new Highlight(
        tourPrograms,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset(col1x, offsetY),
        (size.width / 2.5));

    offsetX += width + marginLeft;
    Highlight exercises = new Highlight(
        tourExercises,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset((size.width / 2), 80.0),
        (size.width - 100.0));

    offsetX += width + marginLeft;
    Highlight team = new Highlight(
        tourTeam,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset(col3x, offsetY),
        (size.width / 2.5));

    offsetX = marginLeft;
    offsetY += height + marginTop;
    Highlight ehealth = new Highlight(
        tourEhealth,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset(col1x, offsetY),
        (size.width / 2.5));

    offsetX += width + marginLeft;
    Highlight profile = new Highlight(
        tourProfile,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset((size.width / 2), offsetY - height - marginTop - 5.0),
        (size.width - 100.0));

    offsetX += width + marginLeft;
    Highlight contact = new Highlight(
        tourContact,
        new Rect.fromLTWH(offsetX, offsetY, width, height),
        new Offset(col3x, offsetY),
        (size.width / 2.5));

    Highlight menu = new Highlight(
        tourMenu,
        new Rect.fromLTWH(0.0, 0.0, 0.0, 0.0),
        new Offset(size.width / 2, 80.0),
        size.width - 50.0);

    highlights.add(intro);
    highlights.add(sessions);
    highlights.add(messages);
    highlights.add(results);
    highlights.add(programs);
    highlights.add(exercises);
    highlights.add(team);
    highlights.add(ehealth);
    highlights.add(profile);
    highlights.add(contact);
    highlights.add(menu);

    maxTourProgress = highlights.length;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  @override
  void paint(Canvas canvas, Size size) {
    drawBackground(canvas, size);
    drawText(canvas, size);
    drawTapHelper(canvas, size);
    drawProgress(canvas, size);
  }

  void drawProgress(Canvas canvas, Size size) {
    TextSpan span = new TextSpan(
        style: new TextStyle(
            color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16.0),
        text:
            (tourProgress + 1).toString() + ' / ' + maxTourProgress.toString());
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(canvas, new Offset(10.0, 10.0));
  }

  void drawTapHelper(Canvas canvas, Size size) {
    double offsetY = size.height - 130.0;

    if (tourProgress > 6 && tourProgress < 10) offsetY = 50.0;

    TextSpan span = new TextSpan(
        style: new TextStyle(
            color: Colors.white, fontWeight: FontWeight.w600, fontSize: 13.0),
        text:
            tourProgress < maxTourProgress - 1 ? tapToContinue : tapToComplete);
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr);
    tp.layout();

    tp.paint(canvas, new Offset((size.width / 2) - (tp.width / 2), offsetY));

    if (tapImage == null) return;

    double imgWidth = tapImage.width.ceilToDouble();
    double imgHeight = tapImage.height.ceilToDouble();
    double imgSize = 60.0;
    canvas.drawImageRect(
        tapImage,
        new Rect.fromLTWH(0.0, 0.0, imgWidth, imgHeight),
        new Rect.fromLTWH((size.width / 2) - (imgSize / 2),
            offsetY + tapImageOffsetY + 40.0, imgSize, imgSize),
        new Paint());
  }

  void drawText(Canvas canvas, Size size) {
    double width = highlights[tourProgress].textWidth;

    Offset textOffset = highlights[tourProgress].textOffset;

    ui.ParagraphBuilder paragraphBuilder = new ui.ParagraphBuilder(
        new ui.ParagraphStyle(
            maxLines: 10,
            fontWeight: FontWeight.w600,
            fontSize: 15.0,
            textAlign: TextAlign.center))
      ..pushStyle(new ui.TextStyle(color: Colors.white))
      ..addText(highlights[tourProgress].text);

    final paragraph = paragraphBuilder.build();
    paragraph.layout(new ui.ParagraphConstraints(width: width));
    canvas.drawParagraph(
        paragraph, new Offset(textOffset.dx - (width / 2), textOffset.dy));
  }

  void drawBackground(Canvas canvas, Size size) {
    /// draws dark background everywhere except for highlighted area
    Paint p = new Paint();
    p.color = new Color.fromARGB(210, 50, 50, 50);

    if (itemRadius == 0.0) itemRadius = 5.0;

    Rect highlightedRect = highlights[tourProgress].highlightArea;

    double margin = 20.0;
    if (highlightedRect.width == 0) margin = 0.0;

    double left = highlightedRect.left - margin;
    double top = highlightedRect.top + 1.0 - margin;
    double right = highlightedRect.right + margin;
    double bottom = highlightedRect.bottom + 1.0 + margin;

    if (left < 0.0) left = 0.0;
    if (top < 0.0) top = 0.0;
    if (right > size.width) right = size.width;
    if (bottom > size.height) bottom = size.height;

    canvas.drawDRRect(
        new RRect.fromLTRBAndCorners(0.0, 0.0, size.width, size.height),
        new RRect.fromLTRBAndCorners(left, top, right, bottom,
            topLeft: Radius.circular(itemRadius),
            topRight: Radius.circular(itemRadius),
            bottomLeft: Radius.circular(itemRadius),
            bottomRight: Radius.circular(itemRadius)),
        p);
  }
}
