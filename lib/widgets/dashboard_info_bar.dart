import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:myclient_app/utils/color_helper.dart';

class DashboardInfoBar extends StatefulWidget {
  final String primaryColor;
  final double profileAvatarBorderWidth;
  final String avatarUrl;
  final String infoColor;
  final String email;
  final String emailColor;
  final String displayName;
  final String displayNameColor;

  DashboardInfoBar(
      {Key key,
      @required this.primaryColor,
      @required this.profileAvatarBorderWidth,
      @required this.avatarUrl,
      @required this.infoColor,
      @required this.emailColor,
      @required this.email,
      @required this.displayName,
      @required this.displayNameColor})
      : super(key: key);

  @override
  _DashboardInfoBar createState() => new _DashboardInfoBar(
      primaryColor: primaryColor,
      profileAvatarBorderWidth: profileAvatarBorderWidth,
      avatarUrl: avatarUrl,
      infoColor: infoColor,
      emailColor: emailColor,
      email: email,
      displayName: displayName,
      displayNameColor: displayNameColor);
}

class _DashboardInfoBar extends State<DashboardInfoBar> {
  final String primaryColor;
  final double profileAvatarBorderWidth;
  final String avatarUrl;
  final String infoColor;
  final String email;
  final String emailColor;
  final String displayName;
  final String displayNameColor;

  _DashboardInfoBar(
      {this.primaryColor,
      @required this.profileAvatarBorderWidth,
      @required this.avatarUrl,
      @required this.infoColor,
      @required this.emailColor,
      @required this.email,
      @required this.displayName,
      @required this.displayNameColor});

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 0.0, right: 0.0),
        child: new Container(
          color: ColorHelper.fromHex(infoColor),
          child: Row(
            children: <Widget>[
              // profile image
              new Container(
                  margin: EdgeInsets.only(left: 14.0, bottom: 14.0, top: 14.0),
                  child: new CircleAvatar(
                    radius: 90.0,
                    backgroundColor: ColorHelper.fromHex(primaryColor),
                    backgroundImage: new AdvancedNetworkImage(
                      avatarUrl,
                      useDiskCache: true,
                    ),
                  ),
                  width: 64.0,
                  height: 64.0,
                  padding:
                      EdgeInsets.all(profileAvatarBorderWidth), // border width
                  decoration: new BoxDecoration(
                    color: ColorHelper.fromHex(primaryColor), // border color
                    shape: BoxShape.circle,
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Text(
                      displayName,
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: ColorHelper.fromHex(displayNameColor),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Roboto"),
                    ),
                    new Text(
                      email,
                      style: new TextStyle(
                          fontSize: 15.0,
                          color: ColorHelper.fromHex(emailColor),
                          fontWeight: FontWeight.w300,
                          fontFamily: "Roboto"),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
