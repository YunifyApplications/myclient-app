import 'package:flutter/material.dart';

class BorderedButton extends StatefulWidget {
  final bool fill;
  final VoidCallback onPressed;

  BorderedButton({Key key, @required this.fill, @required this.onPressed})
      : super(key: key);

  _BorderedButton createState() =>
      new _BorderedButton(fill: fill, onPressed: onPressed);
}

class _BorderedButton extends State<BorderedButton> {
  final bool fill;
  final VoidCallback onPressed;

  _BorderedButton({@required this.fill, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    if (fill) {
      return new Container(
        margin: const EdgeInsets.only(top: 40.0),
        child: new SizedBox(
          height: 45.0,
          width: double.infinity,
          child: new RaisedButton(
            onPressed: onPressed,
            textColor: Colors.black,
            color: Colors.white,
            padding: const EdgeInsets.all(8.0),
            child: new Text(
              'Aanmelden',
            ),
          ),
        ),
      );
    } else {
      return new Container(
        padding: const EdgeInsets.all(0.0),
        margin: const EdgeInsets.only(top: 40.0),
        height: 45.0,
        child: new OutlineButton(
          borderSide: new BorderSide(
            color: Colors.white,
            width: 2.0,
          ),
          onPressed: onPressed,
          textColor: Colors.white,
          highlightedBorderColor: Colors.white,
          splashColor: Colors.transparent,
          color: Color.fromARGB(1, 255, 255, 255),
          padding: const EdgeInsets.all(0.0),
          child: new Text(
            'Aanmelden',
          ),
        ),
      );
    }
  }
}
