import 'package:flutter/material.dart';
import 'package:myclient_app/utils/color_helper.dart';

class SideMenuItem extends StatefulWidget {
  final String text;
  final String iconUrl;
  final VoidCallback onTap;
  final String backgroundColor;

  SideMenuItem(
      {Key key,
      @required this.text,
      @required this.iconUrl,
      @required this.onTap,
      @required this.backgroundColor})
      : super(key: key);

  @override
  _SideMenuItem createState() => new _SideMenuItem(
      text: this.text,
      iconUrl: this.iconUrl,
      onTap: this.onTap,
      backgroundColor: this.backgroundColor);
}

class _SideMenuItem extends State<SideMenuItem> {
  final String text;
  final String iconUrl;
  final VoidCallback onTap;
  final String backgroundColor;

  _SideMenuItem(
      {@required this.text,
      @required this.iconUrl,
      @required this.onTap,
      @required this.backgroundColor});

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      onPressed: onTap,
      elevation: 0.0,
      highlightElevation: 0.0,
      color: ColorHelper.fromHex(backgroundColor),
      child: new Container(
        child: new Row(
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.only(left: 10.0, top: 15.0, bottom: 15.0),
              child: new Image.asset(
                iconUrl,
                width: MediaQuery.of(context).size.width * 0.10,
                height: MediaQuery.of(context).size.width * 0.10,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 28.0),
              child: new Text(
                text,
                style: new TextStyle(color: Colors.white, fontSize: 18.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
