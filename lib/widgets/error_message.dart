import 'package:flutter/material.dart';

class ErrorMessage {
  static void show(GlobalKey<ScaffoldState> state, String message) async {
    state.currentState.showSnackBar(
      // update start
      new SnackBar(
        backgroundColor: Colors.redAccent,
        duration: new Duration(seconds: 4),
        content: new Text(
          message,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
