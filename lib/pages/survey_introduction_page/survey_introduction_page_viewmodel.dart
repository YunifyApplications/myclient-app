import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class SurveyIntroductionPageViewModel {
  final Survey survey;
  final bool done;
  String primaryColor;
  String title;
  String introduction;
  String exercise;
  String exerciseDesc;
  String durationInMinutes;
  String startSurvey;
  String continueSurvey;

  SurveyIntroductionPageViewModel({this.survey, this.done});

  void loadLocalization() {
    primaryColor = LocalizationService.get("PrimaryColor");
    title = 'Survey Introductie';
    introduction = LocalizationService.get('FormIntroductionTitle');
    exercise = LocalizationService.get('Exercise');
    exerciseDesc = LocalizationService.get('ExerciseDesc');
    durationInMinutes = LocalizationService.get('DurationInMinutes');
    startSurvey = LocalizationService.get('StartForm');
    continueSurvey = LocalizationService.get('ResumeForm');
  }
}
