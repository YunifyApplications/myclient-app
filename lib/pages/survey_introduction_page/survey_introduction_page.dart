import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/survey_introduction_page/survey_introduction_page_viewmodel.dart';
import 'package:myclient_app/pages/take_survey_page/take_survey_page.dart';
import 'package:myclient_app/utils/color_helper.dart';

class SurveyIntroductionPage extends StatefulWidget {
  final Survey survey;
  final bool done;
  SurveyIntroductionPage({Key key, @required this.survey, @required this.done})
      : super(key: key);

  @override
  _SurveyIntroductionPage createState() =>
      new _SurveyIntroductionPage(survey: this.survey, done: this.done);
}

class _SurveyIntroductionPage extends State<SurveyIntroductionPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  SurveyIntroductionPageViewModel model;
  TabController tc;
  TabController tcTop;

  _SurveyIntroductionPage({@required Survey survey, @required bool done}) {
    model = new SurveyIntroductionPageViewModel(survey: survey, done: done);
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  void takeSurvey() {
    try {
      if (model.survey.progressInSeconds == null) {
        httpService.startSurvey(new StartSurveyDTO(surveyId: model.survey.id));

        model.survey.startedOn = DateTime.now();
      }
    } catch (ex) {}

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => new TakeSurveyPage(
              survey: model.survey,
              done: model.done,
            ),
      ),
    );
  }

  void takeSurveyWeb() {
//    final flutterWebviewPlugin = new FlutterWebviewPlugin();
//
//    flutterWebviewPlugin.launch("http://demo.hetrughuis.nl/Survey/Take/8304",
//        headers: null,
//        withJavascript: true,
//        clearCache: false,
//        clearCookies: false,
//        hidden: false,
//        enableAppScheme: true,
//        rect: null,
//        userAgent: null,
//        withZoom: false,
//        withLocalStorage: true,
//        scrollBar: true);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => new WebviewScaffold(
              url: "http://demo.hetrughuis.nl/Survey/Take/8304",
              scrollBar: true,
              withLocalStorage: true,
              withJavascript: false,
              appBar: new AppBar(
                title: new Text("Widget webview"),
              ),
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: new Key('survey_introduction_page'),
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new Container(
        child: new ListView(
          children: <Widget>[
            new Container(
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              decoration: new BoxDecoration(
                color: new Color.fromARGB(255, 245, 245, 245),
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                  color: new Color.fromARGB(255, 240, 240, 240),
                  width: 1.0,
                ),
              ),
              child: new Column(
                children: <Widget>[
                  new Center(
                    child: new Text(
                      model.introduction,
                      style: new TextStyle(
                        color: ColorHelper.fromHex(model.primaryColor),
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 20.0, right: 20.0, bottom: 10.0),
                    child: new SizedBox(
                        width: double.infinity,
                        height: 1.0,
                        // height: double.infinity,
                        child: new Container(
                            color: new Color.fromARGB(255, 227, 227, 227))),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                  width: 120.0,
                                  child: new Text(
                                    model.exercise,
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w600),
                                  ),
                                  margin: EdgeInsets.only(bottom: 10.0),
                                ),
                                new Container(
                                  width:
                                      MediaQuery.of(context).size.width - 190.0,
                                  child: new Text(
                                    model.survey.surveyLib.name,
                                  ),
                                  margin: EdgeInsets.only(bottom: 10.0),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                  width: 120.0,
                                  child: new Text(
                                    model.exerciseDesc,
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w600),
                                  ),
                                  margin: EdgeInsets.only(bottom: 10.0),
                                ),
                                new Container(
                                  width:
                                      MediaQuery.of(context).size.width - 190.0,
                                  child: new Text(
                                    model.survey.surveyLib.description,
                                  ),
                                  margin: EdgeInsets.only(bottom: 10.0),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Container(
                                  width: 120.0,
                                  child: new Text(
                                    model.durationInMinutes,
                                    style: new TextStyle(
                                        fontWeight: FontWeight.w600),
                                  ),
                                  margin: EdgeInsets.only(bottom: 10.0),
                                ),
                                new Container(
                                  width:
                                      MediaQuery.of(context).size.width - 190.0,
                                  child: new Text(
                                    model.survey.surveyLib.durationInMinutes ==
                                            null
                                        ? 'N.v.t.'
                                        : model.survey.surveyLib
                                                .durationInMinutes
                                                .toString() +
                                            " Minuten",
                                  ),
                                  margin: EdgeInsets.only(bottom: 10.0),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        bottom: 10.0, left: 10.0, right: 10.0, top: 25.0),
                    padding: const EdgeInsets.only(),
                    height: 30.0,
                    width: double.infinity,
                    decoration: new BoxDecoration(
                      color: ColorHelper.fromHex(model.primaryColor),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child: new FlatButton(
                      onPressed: () => takeSurvey(),
                      textColor: Colors.white,
                      color: Colors.transparent,
                      padding: const EdgeInsets.all(0.0),
                      child: new Text(
                        model.survey.startedOn == null
                            ? model.startSurvey
                            : model.continueSurvey,
                        style: new TextStyle(fontSize: 12.0),
                      ),
                    ),
                  ),
//                  new Container(
//                    margin: EdgeInsets.only(
//                        bottom: 10.0, left: 10.0, right: 10.0, top: 5.0),
//                    padding: const EdgeInsets.only(),
//                    height: 30.0,
//                    width: double.infinity,
//                    decoration: new BoxDecoration(
//                      color: ColorHelper.fromHex(model.primaryColor),
//                      borderRadius: BorderRadius.circular(4.0),
//                    ),
//                    child: new FlatButton(
//                      onPressed: () => takeSurveyWeb(),
//                      textColor: Colors.white,
//                      color: Colors.transparent,
//                      padding: const EdgeInsets.all(0.0),
//                      child: new Text(
//                        model.done ? 'Bekijken web' : 'Start oefening web',
//                        style: new TextStyle(fontSize: 12.0),
//                      ),
//                    ),
//                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
