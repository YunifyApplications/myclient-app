import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/events/message_sent_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/conversation_detail_page/conversation_detail_page.dart';
import 'package:myclient_app/pages/create_conversation_page/create_conversation_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';

class CreateConversationPage extends StatefulWidget {
  final String selectedRecipientId;
  final List<Recipient> availableRecipients;

  CreateConversationPage(
      {Key key,
      @required this.availableRecipients,
      this.selectedRecipientId = null})
      : super(key: key);

  @override
  _CreateConversationPage createState() =>
      new _CreateConversationPage(availableRecipients, selectedRecipientId);
}

class _CreateConversationPage extends State<CreateConversationPage> {
  CreateConversationPageViewModel model;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _CreateConversationPage(availableRecipients, selectedRecipientId) {
    model = new CreateConversationPageViewModel(
        availableRecipients: availableRecipients,
        selectedRecipientId: selectedRecipientId);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  void sendMessage() async {
    if (model.selectedRecipient == null) {
      ErrorMessage.show(_scaffoldKey, model.noParticipantsSelected);
      return;
    }

    // if inputform is empty or only contains whitespaces, clear and return.
    if (model.textMessageController.text.trim().isEmpty) {
      // hide keyboard
      FocusScope.of(context).requestFocus(new FocusNode());

      ErrorMessage.show(_scaffoldKey, model.messageEmpty);

      // clear text
      model.textMessageController.clear();
      return;
    }

    // if inputform is empty or only contains whitespaces, clear and return.
    if (model.textSubjectController.text.trim().isEmpty) {
      // hide keyboard
      FocusScope.of(context).requestFocus(new FocusNode());

      ErrorMessage.show(_scaffoldKey, model.subjectEmpty);
      // clear text
      model.textSubjectController.clear();
      return;
    }

    bool sentToTeam = model.selectedRecipient.receiverId == 'team';

    List<Recipient> recipients = new List();
    if (sentToTeam) {
      for (var recipient in model.availableRecipients) {
        if (recipient.receiverId != 'team') {
          recipients.add(new Recipient(
            displayName: recipient.displayName,
            receiverId: recipient.receiverId,
            isRead: recipient.isRead,
            avatarUrl: recipient.avatarUrl,
          ));
        }
      }
    } else {
      recipients.add(new Recipient(
        displayName: model.selectedRecipient.displayName,
        receiverId: model.selectedRecipient.receiverId,
        isRead: model.selectedRecipient.isRead,
        avatarUrl: model.selectedRecipient.avatarUrl,
      ));
    }

    Message newMessage = new Message(
      id: 0,
      messageBody: model.textMessageController.text,
      parentId: null,
      parent: null,
      sendDate: DateTime.now(),
      senderId: UserHelper.userId,
      senderAvatarUrl: UserHelper.avatarUrl,
      senderDisplayName: UserHelper.displayName,
      sentToTeam: sentToTeam,
      recipients: recipients,
      subject: model.textSubjectController.text,
    );

    try {
      // send new message to server.
      var result = await httpService.sendMessage(newMessage);
      newMessage.id = result.messageId;

      // fire new message event for conversations page.
      eventBus.fire(new MessageSentEvent(newMessage));

      // hide keyboard
      FocusScope.of(context).requestFocus(new FocusNode());

      // clear text
      model.textSubjectController.clear();
      model.textMessageController.clear();

      // navigate to conversation detail page for new conversation
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => new ConversationDetailPage(dto: newMessage)));
    } catch (ex) {
      // show error because sending the message failed
      ErrorMessage.show(_scaffoldKey, model.sendMessageFailed);

      // hide keyboard
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

  void recipientSelected(Recipient recipient) {
    setState(() {
      model.selectedRecipient = recipient;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      bottomNavigationBar: new FlatButton(
        onPressed: sendMessage,
        color: ColorHelper.fromHex(model.primaryColor),
        child: new Container(
          color: Colors.transparent,
          height: 50.0,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  model.envelopeIcon,
                  width: MediaQuery.of(context).size.width * 0.13,
                  height: MediaQuery.of(context).size.width * 0.13,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: new Text(
                    model.sendMessage,
                    style: new TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w300),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      body: new Container(
        child: new ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(15.0),
              child: new Center(
                child: new DropdownButton<Recipient>(
                  items: model.availableRecipients
                      .map(
                        (recipient) => new DropdownMenuItem<Recipient>(
                              child: new Container(
                                margin: EdgeInsets.only(bottom: 7.0),
                                child: new Row(
                                  children: <Widget>[
                                    new Container(
                                      margin: EdgeInsets.only(
                                          right: 6.0, left: 5.0),
                                      child: new CircleAvatar(
                                        radius: 90.0,
                                        backgroundColor: new Color.fromARGB(
                                            255, 227, 227, 227),
                                        backgroundImage:
                                            new AdvancedNetworkImage(
                                          recipient.avatarUrl,
                                          useDiskCache: true,
                                        ),
                                      ),
                                      width: 30.0,
                                      height: 30.0,
                                      padding:
                                          EdgeInsets.all(1.0), // border width
                                      decoration: new BoxDecoration(
                                        color: new Color.fromARGB(
                                            255, 227, 227, 227), // border color
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                    new Text(recipient.displayName),
                                  ],
                                ),
                              ),
                              value: recipient,
                            ),
                      )
                      .toList(),
                  hint: new Text(model.messageParticipants),
                  value: model.selectedRecipient,
                  onChanged: recipientSelected,
                  style: new TextStyle(color: Colors.black87, fontSize: 18.0),
                  isExpanded: true,
                ),
              ),
            ),
            new Container(
              margin: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
              height: 45.0,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                      color: new Color.fromARGB(255, 229, 229, 229),
                      width: 1.0),
                  borderRadius: BorderRadius.circular(4.0)),
              child: new Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                child: new TextField(
                  controller: model.textSubjectController,
                  keyboardType: TextInputType.multiline,
                  maxLines: 128,
                  cursorColor: Colors.black87,
                  style: new TextStyle(
                      fontSize: 16.0,
                      color: Colors.black87,
                      decoration: TextDecoration.none),
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    hintText: model.messageSubject,
                    hintStyle: new TextStyle(
                      color: new Color.fromARGB(255, 150, 150, 150),
                    ),
                  ),
                ),
              ),
            ),
            new Container(
              margin: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
              height: MediaQuery.of(context).size.height - 310,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                      color: new Color.fromARGB(255, 229, 229, 229),
                      width: 1.0),
                  borderRadius: BorderRadius.circular(4.0)),
              child: new Padding(
                padding:
                    const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 20.0),
                child: new TextField(
                  controller: model.textMessageController,
                  keyboardType: TextInputType.multiline,
                  maxLines: 128,
                  cursorColor: Colors.black87,
                  style: new TextStyle(
                      fontSize: 16.0,
                      color: Colors.black87,
                      decoration: TextDecoration.none),
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    focusedErrorBorder: InputBorder.none,
                    hintText: model.typeMessage,
                    hintStyle: new TextStyle(
                      color: new Color.fromARGB(255, 150, 150, 150),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
