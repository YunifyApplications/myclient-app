import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class CreateConversationPageViewModel {
  String primaryColor;
  String title;
  String envelopeIcon;
  String sendMessage;
  String messageParticipants;
  String messageSubject;
  String typeMessage;
  String sendMessageFailed;
  String subjectEmpty;
  String messageEmpty;
  String noParticipantsSelected;

  Recipient selectedRecipient;
  String selectedRecipientId;
  TextEditingController textMessageController;
  TextEditingController textSubjectController;
  final List<Recipient> availableRecipients;

  CreateConversationPageViewModel(
      {this.availableRecipients, this.selectedRecipientId = null}) {
    // add team as recipient
    if (availableRecipients.where((e) => e.receiverId == 'team').length == 0) {
      availableRecipients.add(new Recipient(
          receiverId: 'team',
          isRead: false,
          avatarUrl: '',
          displayName: 'Mijn Team'));
    }

    // select recipient of passed as argument
    if (selectedRecipientId != null) {
      for (var recipient in availableRecipients) {
        if (recipient.receiverId == selectedRecipientId) {
          selectedRecipient = recipient;
        }
      }
    }

    textMessageController = new TextEditingController();
    textSubjectController = new TextEditingController();
  }

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');

    title = 'Nieuw gesprek';
    envelopeIcon = 'assets/icons/envelope_64px_White.png';
    sendMessage = LocalizationService.get('MessageSend');
    messageParticipants = LocalizationService.get('MessageParticipants');
    messageSubject = LocalizationService.get('MessageSubject');
    typeMessage = LocalizationService.get('TypeMessage');
    sendMessageFailed = LocalizationService.get('SendMessageFailed');
    subjectEmpty = LocalizationService.get('SubjectEmpty');
    messageEmpty = LocalizationService.get('MessageEmpty');
    noParticipantsSelected = LocalizationService.get('NoParticipantsSelected');
  }
}
