import 'package:flutter/material.dart';
import 'package:flutter_pdf_viewer/flutter_pdf_viewer.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:myclient_app/dtos/results_dto.dart';
import 'package:myclient_app/pages/results_page/results_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/download_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';
import 'package:progress_hud/progress_hud.dart';

class ResultsPage extends StatefulWidget {
  final ResultCollectionDTO dto;
  ResultsPage({Key key, @required this.dto}) : super(key: key);

  @override
  _ResultsPage createState() => new _ResultsPage(dto: this.dto);
}

class _ResultsPage extends State<ResultsPage> {
  ResultsPageViewModel model;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _ResultsPage({@required ResultCollectionDTO dto}) {
    model = new ResultsPageViewModel(dto: dto);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  void openResult(TestResult result) async {
    if (result.isPdf) {
      displayPDF(result.url);
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new WebviewScaffold(
                url: result.url,
                scrollBar: false,
                withZoom: false,
                withLocalStorage: true,
                withJavascript: true,
                clearCache: false,
                clearCookies: false,
                appBar: new AppBar(
                  title: new Text(result.name),
                  backgroundColor: ColorHelper.fromHex(model.primaryColor),
                  centerTitle: true,
                ),
              ),
        ),
      );
    }
  }

  Future<void> displayPDF(String retrieveUrl) async {
    try {
      // show loading animation while downloading file
      var progress = new ProgressHUD(
        backgroundColor: Colors.black12,
        color: Colors.white,
        containerColor: ColorHelper.fromHex(model.primaryColor),
        borderRadius: 5.0,
      );
      showDialog(context: context, child: progress);

      var result =
          await DownloadHelper.downloadAsFile(retrieveUrl, cache: true);

      // pop loading animation
      Navigator.pop(context);

      await FlutterPdfViewer.loadFilePath('file://' + result);
    } catch (ex) {
      // pop loading animation
      Navigator.pop(context);
      ErrorMessage.show(_scaffoldKey, model.fileRetrievalFailed);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new Container(
        child: new ListView(
          padding: EdgeInsets.only(bottom: 10.0),
          children: createPanels(),
        ),
      ),
    );
  }

  List<Widget> createPanels() {
    return model.dto.results
        .map(
          (result) => new Container(
                margin: EdgeInsets.only(
                    bottom: 0.0, left: 10.0, right: 10.0, top: 10.0),
                padding: const EdgeInsets.only(),
                height: 60.0,
                width: double.infinity,
                decoration: new BoxDecoration(
                  color: ColorHelper.fromHex(model.primaryColor),
                  borderRadius: BorderRadius.circular(4.0),
                ),
                child: new Stack(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.only(top: 6.0),
                      child: new Text(
                        result.name,
                        textAlign: TextAlign.center,
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                        ),
                      ),
                      width: double.infinity,
                      height: 30.0,
                    ),
                    new Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      child: new Container(
                        width: MediaQuery.of(context).size.width - 20.0,
                        decoration: new BoxDecoration(
                          color: ColorHelper.fromHex(model.primaryColorDark),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: new Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(6.0),
                              child: new Container(
                                padding: EdgeInsets.only(left: 8.0, right: 8.0),
                                height: 17.0,
                                decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color:
                                      ColorHelper.fromHex(model.primaryColor),
                                ),
                                child: Center(
                                  child: new Text(
                                    FormatHelper.formatResultTypeString(result),
                                    textAlign: TextAlign.center,
                                    style: new TextStyle(
                                        color: Colors.white, fontSize: 11.0),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    new Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: new FlatButton(
                        onPressed: () => openResult(result),
                        textColor: Colors.white,
                        color: Colors.transparent,
                        padding: const EdgeInsets.all(0.0),
                        child: new Text(''),
                      ),
                    ),
                  ],
                ),
              ),
        )
        .toList();
  }
}
