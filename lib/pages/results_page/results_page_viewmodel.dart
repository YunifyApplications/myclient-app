import 'package:myclient_app/dtos/results_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class ResultsPageViewModel {
  String primaryColor;
  String primaryColorDark;
  String title;
  String fileRetrievalFailed;
  final ResultCollectionDTO dto;

  ResultsPageViewModel({this.dto});

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');
    title = 'Mijn Resultaten';
    primaryColorDark = LocalizationService.get('PrimaryColorDark');
    fileRetrievalFailed = LocalizationService.get('FileRetrievalFailed');
  }
}
