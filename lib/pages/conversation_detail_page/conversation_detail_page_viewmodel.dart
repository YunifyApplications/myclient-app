import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class ConversationDetailPageViewModel {
  Message dto;
  String primaryColor;

  String conversationStart;
  String typeMessage;
  String sendMessageFailed;

  ConversationDetailPageViewModel({this.dto});

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');

    conversationStart = LocalizationService.get('ConversationStart');
    typeMessage = LocalizationService.get('TypeMessage');
    sendMessageFailed = LocalizationService.get('SendMessageFailed');
  }
}
