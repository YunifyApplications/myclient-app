import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/mark_message_read_dto.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/events/dashboard_module_item_count_changed_event.dart';
import 'package:myclient_app/events/message_read_event.dart';
import 'package:myclient_app/events/message_sent_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/conversation_detail_page/conversation_detail_page_viewmodel.dart';
import 'package:myclient_app/pages/message_detail_page/message_detail_page.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/dashboard_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';
import 'package:myclient_app/widgets/message_bubble.dart';

class ConversationDetailPage extends StatefulWidget {
  final Message dto;

  ConversationDetailPage({Key key, @required this.dto}) : super(key: key);

  @override
  _ConversationDetailPage createState() =>
      new _ConversationDetailPage(this.dto);
}

class _ConversationDetailPage extends State<ConversationDetailPage> {
  ConversationDetailPageViewModel model;
  TextEditingController textController;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _ConversationDetailPage(Message dto) {
    model = new ConversationDetailPageViewModel(dto: dto);
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    textController = new TextEditingController();
    super.initState();
  }

  void navigateToMessageDetail(Message message) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => new MessageDetailPage(dto: message)));
  }

  void sendMessage() async {
    // if inputform is empty or only contains whitespaces, clear and return.
    if (textController.text.trim().isEmpty) {
      // hide keyboard
      FocusScope.of(context).requestFocus(new FocusNode());

      // clear text
      textController.clear();
      return;
    }

    List<Recipient> recipients = new List();

    // if we sent the parent message we can keep the same recipients list.
    if (model.dto.senderId == UserHelper.userId) {
      for (var recipient in model.dto.recipients) {
        recipients.add(new Recipient(
            displayName: recipient.displayName,
            isRead: false,
            avatarUrl: recipient.avatarUrl,
            receiverId: recipient.receiverId));
      }
    } else {
      // add sender of previous message if not the sender.
      recipients.add(new Recipient(
          displayName: model.dto.senderDisplayName,
          isRead: false,
          avatarUrl: model.dto.senderAvatarUrl,
          receiverId: model.dto.senderId));

      for (var recipient in model.dto.recipients) {
        if (recipient.receiverId != UserHelper.userId) {
          recipients.add(new Recipient(
              displayName: recipient.displayName,
              isRead: false,
              avatarUrl: recipient.avatarUrl,
              receiverId: recipient.receiverId));
        }
      }
    }

    Message newMessage = new Message(
      id: 0,
      messageBody: textController.text,
      parentId: model.dto.id,
      parent: model.dto,
      sendDate: DateTime.now(),
      senderId: UserHelper.userId,
      senderAvatarUrl: UserHelper.avatarUrl,
      senderDisplayName: UserHelper.displayName,
      sentToTeam: model.dto.sentToTeam,
      recipients: recipients,
      subject: model.dto.subject,
    );

    try {
      // send new message to server.
      var result = await httpService.sendMessage(newMessage);
      newMessage.id = result.messageId;

      // fire new message event for conversations page.
      eventBus.fire(new MessageSentEvent(newMessage));

      setState(() {
        model.dto = newMessage;
      });

      // hide keyboard
      FocusScope.of(context).requestFocus(new FocusNode());

      // clear text
      textController.clear();
    } catch (ex) {
      // show error when sending the message failed
      ErrorMessage.show(_scaffoldKey, model.sendMessageFailed);

      // hide keyboard
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

  void markMessageAsRead(Message message) {
    // if the message is not sent by the current user.
    if (message.senderId != UserHelper.userId) {
      for (var rec in message.recipients) {
        if (rec.receiverId == UserHelper.userId && !rec.isRead) {
          try {
            httpService.markMessageRead(
                new MarkMessageReadDTO(message.id, rec.receiverId));

            // fire message read event for conversations page.
            eventBus.fire(new MessageReadEvent(message.id, rec.receiverId));

            // update dashboard item count
            if (DashboardHelper.dashboardInfo.messageCount > 0) {
              DashboardHelper.dashboardInfo.messageCount--;
            }

            eventBus.fire(new DashboardModuleItemCountChangedEvent());
          } catch (ex) {
            // sending request failed so we should not mark the message as read.
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(
          model.dto.subject,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        centerTitle: true,
      ),
      body: new Container(
        color: new Color.fromARGB(255, 240, 240, 240),
        child: new Stack(
          children: <Widget>[
            new Container(
              margin: new EdgeInsets.only(bottom: 60.0),
              child: new ListView(
                reverse: true,
                children: createMessageBubbles(),
              ),
            ),
            new Positioned(
              bottom: 0.0,
              right: 0.0,
              child: new Container(
                height: 60.0,
                width: MediaQuery.of(context).size.width,
                decoration: new BoxDecoration(
                  color: new Color.fromARGB(255, 230, 230, 230),
                  border: new Border(
                    top: new BorderSide(
                      color: new Color.fromARGB(255, 210, 210, 210),
                    ),
                  ),
                ),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Container(
                      margin: new EdgeInsets.only(left: 10.0),
                      height: 42.0,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      width: MediaQuery.of(context).size.width - 75,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 19.0, right: 19.0, top: 0.0, bottom: 15.0),
                        child: new TextField(
                          controller: textController,
                          keyboardType: TextInputType.multiline,
                          maxLines: 128,
                          cursorColor: Colors.black87,
                          style: new TextStyle(
                              fontSize: 16.0,
                              color: Colors.black87,
                              decoration: TextDecoration.none),
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            focusedErrorBorder: InputBorder.none,
                            hintText: model.typeMessage,
                            hintStyle: new TextStyle(
                              color: new Color.fromARGB(255, 150, 150, 150),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 10.0),
                      child: new Container(
                        width: 42.0,
                        height: 42.0,
                        child: new RaisedButton(
                          color: ColorHelper.fromHex(model.primaryColor),
                          padding: EdgeInsets.all(0.0),
                          onPressed: () => sendMessage(),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(90.0)),
                          child: new Container(
                            width: 42.0,
                            height: 42.0,
                            decoration: new BoxDecoration(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(190.0),
                            ),
                            child: new Icon(
                              Icons.send,
                              size: 20.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        alignment: Alignment(0.0, 0.0),
      ),
    );
  }

  List<Material> createMessageBubbles() {
    List<Message> messages = new List();

    messages.add(model.dto);

    var currentMessage = model.dto;
    while (currentMessage.parent != null) {
      currentMessage = currentMessage.parent;
      messages.add(currentMessage);
    }

    // mark messages as read
    messages.forEach((msg) => markMessageAsRead(msg));

    var listToReturn = messages
        .map(
          (msg) => new Material(
                color: Colors.transparent,
                child: new InkWell(
                  onLongPress: () => navigateToMessageDetail(msg),
                  child: new MessageBubble(
                    message: msg.messageBody,
                    time: FormatHelper.formatTimeShort(msg.sendDate),
                    senderDisplayName: msg.senderDisplayName,
                    isMe: msg.senderId == UserHelper.userId,
                    isRead: msg.recipients.every((e) => e.isRead) ||
                        msg.senderId != UserHelper.userId,
                  ),
                ),
              ),
        )
        .toList();

    listToReturn.add(new Material(
      color: Colors.transparent,
      child: new Center(
        child: new Container(
          margin: new EdgeInsets.all(10.0),
          child: new Text(
            model.conversationStart,
            style: new TextStyle(
              fontSize: 13.0,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
      ),
    ));

    listToReturn.insert(
      0,
      new Material(
        color: Colors.transparent,
        child: new Padding(
          padding: EdgeInsets.only(bottom: 30.0),
        ),
      ),
    );

    return listToReturn;
  }
}
