import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/pages/create_conversation_page/create_conversation_page.dart';
import 'package:myclient_app/pages/session_detail_page/session_detail_page.dart';
import 'package:myclient_app/pages/sessions_page/sessions_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';

class SessionsPage extends StatefulWidget {
  final SessionCollectionDTO dto;

  SessionsPage({Key key, @required this.dto}) : super(key: key);

  @override
  _SessionsPage createState() => new _SessionsPage(dto);
}

class _SessionsPage extends State<SessionsPage> {
  SessionsPageViewModel model;

  _SessionsPage(SessionCollectionDTO dto) {
    model = new SessionsPageViewModel(dto);
  }

  @override
  void initState() {
    setState(() {
      model.loadLocalization();
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void navigateLeft() {
    setState(() {
      model.selectedDate = model.selectedDate.subtract(new Duration(days: 7));
      model.setDate(false);
    });
  }

  void navigateRight() {
    setState(() {
      model.selectedDate = model.selectedDate.add(new Duration(days: 7));
      model.setDate(false);
    });
  }

  void navigateToSessionDetail(Session session) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new SessionDetailPage(dto: session, availableRecipients: [
                  new Recipient(
                      isRead: false,
                      avatarUrl: session.treatmentOfficerAvatarUrl,
                      displayName: session.treatmentOfficerName,
                      receiverId: session.treatmentOfficerId)
                ])));
  }

  void sendEmailToTreatmentOfficer(Session session) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new CreateConversationPage(availableRecipients: [
                  new Recipient(
                      isRead: false,
                      avatarUrl: session.treatmentOfficerAvatarUrl,
                      displayName: session.treatmentOfficerName,
                      receiverId: session.treatmentOfficerId)
                ], selectedRecipientId: session.treatmentOfficerId)));
  }

  void calendarPanEnded(DragEndDetails detail) {
    if (detail.primaryVelocity == 0.0) return;

    if (!detail.primaryVelocity.isNegative) {
      setState(() {
        model.selectedDate = model.selectedDate.subtract(new Duration(days: 7));
        model.setDate(false);
      });
    } else
      setState(() {
        model.selectedDate = model.selectedDate.add(new Duration(days: 7));
        model.setDate(false);
      });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: new Key('sessions_page'),
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new Container(
          color: new Color.fromARGB(255, 237, 235, 237),
          child: new Column(
            children: <Widget>[
              // top navigation bar
              createSessionsNavigationBar(),

              // separator
              new SizedBox(
                  width: double.infinity,
                  height: 1.0,
                  // height: double.infinity,
                  child: new Container(
                      color: new Color.fromARGB(255, 227, 227, 227))),

              // calendar bar
              createSessionsCalendarBar(),

              // separator
              new SizedBox(
                  width: double.infinity,
                  height: 1.0,
                  // height: double.infinity,
                  child: new Container(
                      color: new Color.fromARGB(255, 227, 227, 227))),

              new Expanded(
                  child: new ListView(children: [
                new Column(children: createSessionPanels()),
                new Padding(padding: EdgeInsets.only(top: 20.0))
              ])),
            ],
          )),
    );
  }

  Widget createSessionsCalendarBar() {
    return new GestureDetector(
      onHorizontalDragEnd: (detail) => calendarPanEnded(detail),
      child: new Container(
        height: 85.0,
        color: Colors.white,
        child: Column(
          children: <Widget>[
            // top row
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 25.0, top: 12.0),
                  child: new Center(
                    child: new Text('M',
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: new Color.fromARGB(255, 143, 143, 143))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12.0),
                  child: new Center(
                    child: new Text('D',
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: new Color.fromARGB(255, 143, 143, 143))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12.0),
                  child: new Center(
                    child: new Text('W',
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: new Color.fromARGB(255, 143, 143, 143))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12.0),
                  child: new Center(
                    child: new Text('D',
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: new Color.fromARGB(255, 143, 143, 143))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12.0),
                  child: new Center(
                    child: new Text('V',
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: new Color.fromARGB(255, 143, 143, 143))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12.0),
                  child: new Center(
                    child: new Text('Z',
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: new Color.fromARGB(255, 143, 143, 143))),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12.0, right: 28.0),
                  child: new Center(
                    child: new Text('Z',
                        style: new TextStyle(
                            fontSize: 15.0,
                            color: new Color.fromARGB(255, 143, 143, 143))),
                  ),
                ),
              ],
            ),

            // bottom row
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                createSessionCalendarBarItem(
                  itemDay: model.mondayDay,
                  itemColor: model.mondayColor,
                  isOccupied: model.mondayOccupied,
                  isMonday: true,
                ),
                createSessionCalendarBarItem(
                  itemDay: model.tuesdayDay,
                  itemColor: model.tuesdayColor,
                  isOccupied: model.tuesdayOccupied,
                ),
                createSessionCalendarBarItem(
                  itemDay: model.wednesdayDay,
                  itemColor: model.wednesdayColor,
                  isOccupied: model.wednesdayOccupied,
                ),
                createSessionCalendarBarItem(
                  itemDay: model.thursdayDay,
                  itemColor: model.thursdayColor,
                  isOccupied: model.thursdayOccupied,
                ),
                createSessionCalendarBarItem(
                  itemDay: model.fridayDay,
                  itemColor: model.fridayColor,
                  isOccupied: model.fridayOccupied,
                ),
                createSessionCalendarBarItem(
                  itemDay: model.saturdayDay,
                  itemColor: model.saturdayColor,
                  isOccupied: model.saturdayOccupied,
                ),
                createSessionCalendarBarItem(
                  itemDay: model.sundayDay,
                  itemColor: model.sundayColor,
                  isOccupied: model.sundayOccupied,
                  isSunday: true,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  List<FlatButton> createSessionPanels() {
    return model.selectedWeekSessions
        .map((session) => new FlatButton(
              padding: EdgeInsets.all(0.0),
              onPressed: () => navigateToSessionDetail(session),
              child: new Slidable(
                delegate: new SlidableDrawerDelegate(),
                actionExtentRatio: 0.25,
                secondaryActions: <Widget>[
                  new FlatButton(
                    onPressed: () => sendEmailToTreatmentOfficer(session),
                    color: new Color.fromARGB(255, 137, 136, 136),
                    child: new IconSlideAction(
                      caption: model.email,
                      color: Colors.transparent,
                      onTap: () => sendEmailToTreatmentOfficer(session),
                      icon: Icons.email,
                    ),
                  )
                ],
                child: new Container(
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Container(
                      decoration: new BoxDecoration(
                          color: new Color.fromARGB(255, 249, 249, 249),
                          borderRadius: BorderRadius.circular(2.0)),
                      height: 100.0,
                      width: double.infinity,
                      child: new Column(
                        children: <Widget>[
                          // top row
                          new Container(
                            height: 23.0,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new Container(
                                  child: new Row(
                                    children: <Widget>[
                                      new Center(
                                        child: new Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10.0, top: 2.0),
                                          child: new Container(
                                            width: 9.0,
                                            height: 9.0,
                                            decoration: new BoxDecoration(
                                              color: ColorHelper
                                                  .expertiseCodeToColor(
                                                      session.expertiseCode),
                                              shape: BoxShape.circle,
                                              border: new Border.all(
                                                color: ColorHelper
                                                    .expertiseCodeToColor(
                                                        session.expertiseCode),
                                                width: 0.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      new Center(
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10.0, top: 0.0),
                                          child: new Text(
                                            FormatHelper.formatDate(
                                                session.appointmentStartTime),
                                            style: new TextStyle(
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.w700,
                                                color: new Color.fromARGB(
                                                    255, 85, 85, 85)),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                new Container(
                                  child: new Row(
                                    children: <Widget>[
                                      new Center(
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10.0,
                                              top: 0.0,
                                              right: 10.0),
                                          child: new Text(
                                            FormatHelper.formatTime(
                                                session.appointmentStartTime),
                                            style: new TextStyle(
                                                fontSize: 14.0,
                                                fontWeight: FontWeight.w700,
                                                color: new Color.fromARGB(
                                                    255, 85, 85, 85)),
                                          ),
                                        ),
                                      ),
                                      new Center(
                                        child: new Padding(
                                          padding: const EdgeInsets.only(
                                              left: 0.0, top: 0.0),
                                          child: new Container(
                                            width: 20.0,
                                            height: 23.0,
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 2.0),
                                              child: new Text(
                                                '>',
                                                textAlign: TextAlign.center,
                                                style: new TextStyle(
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            decoration: new BoxDecoration(
                                              color: ColorHelper
                                                  .expertiseCodeToColor(
                                                      session.expertiseCode),
                                              borderRadius:
                                                  new BorderRadius.only(
                                                      topRight:
                                                          const Radius.circular(
                                                              2.0)),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                          // separator
                          Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: new SizedBox(
                                width: double.infinity,
                                height: 1.0,
                                // height: double.infinity,
                                child: new Container(
                                    color: new Color.fromARGB(
                                        255, 235, 235, 235))),
                          ),

                          Row(
                            children: <Widget>[
                              new Container(
                                margin: EdgeInsets.only(
                                    left: 10.0, bottom: 0.0, top: 10.0),
                                child: new CircleAvatar(
                                  radius: 90.0,
                                  backgroundColor:
                                      new Color.fromARGB(255, 233, 233, 233),
                                  backgroundImage: new AdvancedNetworkImage(
                                    session.treatmentOfficerAvatarUrl,
                                    useDiskCache: true,
                                  ),
                                ),
                                width: 52.0,
                                height: 52.0,
                                padding: EdgeInsets.all(1.0), // border width
                                decoration: new BoxDecoration(
                                  color: new Color.fromARGB(
                                      255, 233, 233, 233), // border color
                                  shape: BoxShape.circle,
                                ),
                              ),
                              new Flexible(
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15.0, right: 5.0, top: 4.0),
                                      child: new Text(session.description,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: new TextStyle(
                                              fontWeight: FontWeight.w400,
                                              color: new Color.fromARGB(
                                                  255, 87, 87, 87))),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 15.0, top: 5.0),
                                      child: new Text(
                                        session.treatmentOfficerName,
                                        textAlign: TextAlign.left,
                                        maxLines: 1,
                                        style: new TextStyle(
                                            color: new Color.fromARGB(
                                                255, 97, 196, 248)),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          // avatar
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ))
        .toList();
  }

  Widget createSessionCalendarBarItem(
      {int itemDay,
      Color itemColor,
      bool isOccupied,
      bool isMonday,
      bool isSunday}) {
    return new Padding(
      padding: EdgeInsets.only(
          left: mondayPadding(isMonday),
          top: 9.0,
          right: sundayPadding(isSunday)),
      child: new Container(
        width: 37.0,
        height: 37.0,
        decoration: new BoxDecoration(
          color: isOccupiedColor(isOccupied),
          shape: BoxShape.circle,
          border: new Border.all(
            color: Colors.white,
            width: 0.0,
          ),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 2.0),
              child: new Text(
                itemDay.toString(),
                textAlign: TextAlign.center,
                style: new TextStyle(
                    fontWeight: FontWeight.w500,
                    color: new Color.fromARGB(255, 72, 71, 72),
                    fontSize: 17.0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: new Container(
                width: 4.0,
                height: 4.0,
                decoration: new BoxDecoration(
                  color: isOccupiedItemColor(isOccupied, itemColor),
                  shape: BoxShape.circle,
                  border: new Border.all(
                    color: isOccupiedItemColor(isOccupied, itemColor),
                    width: 0.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Color isOccupiedColor(bool isOccupied) {
    if (isOccupied) {
      return new Color.fromARGB(255, 249, 248, 248);
    } else {
      return Colors.transparent;
    }
  }

  double mondayPadding(bool isMonday) {
    if (isMonday != null && isMonday) {
      return 12.5;
    } else {
      return 0.0;
    }
  }

  double sundayPadding(bool isSunday) {
    if (isSunday != null && isSunday) {
      return 11.0;
    } else {
      return 0.0;
    }
  }

  Color isOccupiedItemColor(bool isOccupied, Color itemColor) {
    if (isOccupied) {
      return itemColor;
    } else {
      return Colors.transparent;
    }
  }

  Widget createSessionsNavigationBar() {
    return new Container(
      height: 50.0,
      color: new Color.fromARGB(255, 249, 248, 248),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new GestureDetector(
            child: Padding(
              padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
              child: new Align(
                child: new Image.asset(
                  model.chevronLeftIcon,
                  width: MediaQuery.of(context).size.width * 0.13,
                  height: MediaQuery.of(context).size.width * 0.13,
                ),
              ),
            ),
            onTap: navigateLeft,
          ),
          new Center(
            child: new Text(model.monthName,
                style: new TextStyle(
                    fontSize: 15.0,
                    color: new Color.fromARGB(255, 85, 85, 85))),
          ),
          new GestureDetector(
            onTap: navigateRight,
            child: Padding(
              padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
              child: new Align(
                child: new Image.asset(
                  model.chevronRightIcon,
                  width: MediaQuery.of(context).size.width * 0.13,
                  height: MediaQuery.of(context).size.width * 0.13,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
