import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/services/localization_service.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';

class SessionsPageViewModel {
  final SessionCollectionDTO sessionsData;
  String primaryColor;
  String title;
  String monthName;
  String email;
  String chevronLeftIcon;
  String chevronRightIcon;

  int mondayDay;
  int tuesdayDay;
  int wednesdayDay;
  int thursdayDay;
  int fridayDay;
  int saturdayDay;
  int sundayDay;

  bool mondayOccupied = false;
  bool tuesdayOccupied = false;
  bool wednesdayOccupied = false;
  bool thursdayOccupied = false;
  bool fridayOccupied = false;
  bool saturdayOccupied = false;
  bool sundayOccupied = false;

  Color mondayColor = Colors.transparent;
  Color tuesdayColor = Colors.transparent;
  Color wednesdayColor = Colors.transparent;
  Color thursdayColor = Colors.transparent;
  Color fridayColor = Colors.transparent;
  Color saturdayColor = Colors.transparent;
  Color sundayColor = Colors.transparent;

  DateTime selectedDate;

  SessionsPageViewModel(this.sessionsData);

  List<Session> selectedWeekSessions;

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');
    title = 'Mijn Afspraken';
    email = LocalizationService.get('Email');
    chevronLeftIcon = 'assets/icons/left-chevron.png';
    chevronRightIcon = 'assets/icons/right-chevron.png';
    setDate(true);
  }

  void setDate(bool reset) {
    if (reset) {
      selectedDate = DateTime.now();
    }
    selectedWeekSessions = new List<Session>();

    // go to monday
    while (selectedDate.weekday != DateTime.monday) {
      selectedDate = selectedDate.subtract(new Duration(days: 1));
    }

    mondayOccupied = false;
    tuesdayOccupied = false;
    wednesdayOccupied = false;
    thursdayOccupied = false;
    fridayOccupied = false;
    saturdayOccupied = false;
    sundayOccupied = false;

    // selectedDate.month can be 1..12
    monthName = FormatHelper.monthNames.elementAt(selectedDate.month - 1);

    mondayDay = selectedDate.day;
    tuesdayDay = selectedDate.add(new Duration(days: 1)).day;
    wednesdayDay = selectedDate.add(new Duration(days: 2)).day;
    thursdayDay = selectedDate.add(new Duration(days: 3)).day;
    fridayDay = selectedDate.add(new Duration(days: 4)).day;
    saturdayDay = selectedDate.add(new Duration(days: 5)).day;
    sundayDay = selectedDate.add(new Duration(days: 6)).day;

    // add selected week sessions to list
    for (final session in sessionsData.sessions) {
      var diff = session.appointmentStartTime.difference(selectedDate);
      var day = diff.inDays;
      if (day < 7 && !diff.isNegative) {
        selectedWeekSessions.add(session);
        // set colors
        if (session.appointmentStartTime.weekday == DateTime.monday) {
          mondayOccupied = true;
          mondayColor = ColorHelper.expertiseCodeToColor(session.expertiseCode);
        } else if (session.appointmentStartTime.weekday == DateTime.tuesday) {
          tuesdayOccupied = true;
          tuesdayColor =
              ColorHelper.expertiseCodeToColor(session.expertiseCode);
        } else if (session.appointmentStartTime.weekday == DateTime.wednesday) {
          wednesdayOccupied = true;
          wednesdayColor =
              ColorHelper.expertiseCodeToColor(session.expertiseCode);
        } else if (session.appointmentStartTime.weekday == DateTime.thursday) {
          thursdayOccupied = true;
          thursdayColor =
              ColorHelper.expertiseCodeToColor(session.expertiseCode);
        } else if (session.appointmentStartTime.weekday == DateTime.friday) {
          fridayOccupied = true;
          fridayColor = ColorHelper.expertiseCodeToColor(session.expertiseCode);
        } else if (session.appointmentStartTime.weekday == DateTime.saturday) {
          saturdayOccupied = true;
          saturdayColor =
              ColorHelper.expertiseCodeToColor(session.expertiseCode);
        } else if (session.appointmentStartTime.weekday == DateTime.sunday) {
          sundayOccupied = true;
          sundayColor = ColorHelper.expertiseCodeToColor(session.expertiseCode);
        }
      }
    }
  }
}
