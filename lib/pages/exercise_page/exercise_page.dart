import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flutter_pdf_viewer/flutter_pdf_viewer.dart';
import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/events/dashboard_module_item_count_changed_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/exercise_page/exercise_page_viewmodel.dart';
import 'package:myclient_app/pages/login_page/login_page.dart';
import 'package:myclient_app/pages/survey_introduction_page/survey_introduction_page.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/dashboard_helper.dart';
import 'package:myclient_app/utils/download_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';
import 'package:open_file/open_file.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:url_launcher/url_launcher.dart';

class ExercisePage extends StatefulWidget {
  final ExerciseCollectionDTO dto;
  ExercisePage({Key key, @required this.dto}) : super(key: key);

  @override
  _ExercisePage createState() => new _ExercisePage(dto: this.dto);
}

class _ExercisePage extends State<ExercisePage> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ExercisePageViewModel model;
  TabController tc;
  TabController tcTop;

  _ExercisePage({@required ExerciseCollectionDTO dto}) {
    model = new ExercisePageViewModel(dto: dto);
  }

  @override
  void dispose() {
    tc.dispose();
    tcTop.dispose();
    super.dispose();
  }

  void moduleNumberSelected() {
    setState(() {});
  }

  void expertiseSelected() {
    setState(() {
      tc.index = 0;
    });
  }

  void markExerciseAsDone(Exercise exercise) async {
    if (exercise.done) return;

    try {
      var result = await httpService.finishExercise(
          new FinishExerciseDTO(exerciseLibId: exercise.exerciseLib.id));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {}

    setState(() {
      exercise.done = true;

      // update dashboard item count
      if (DashboardHelper.dashboardInfo.exerciseCount > 0) {
        DashboardHelper.dashboardInfo.exerciseCount--;
      }

      eventBus.fire(new DashboardModuleItemCountChangedEvent());
    });
  }

  void openExerciseFile(Exercise exercise) async {
    try {
      if (exercise.exerciseLib.isForm && exercise.exerciseLib.survey != null) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new SurveyIntroductionPage(
                      survey: exercise.exerciseLib.survey,
                      done: exercise.done,
                    )));
      } else if (exercise.exerciseLib.isYoutube) {
        launch('https://www.youtube.com/watch?v=' + exercise.exerciseLib.url);
        markExerciseAsDone(exercise);
      } else if (exercise.exerciseLib.isVimeo) {
        launch('https://vimeo.com/' +
            exercise.exerciseLib.url); // not sure if this is the correct url
        markExerciseAsDone(exercise);
      } else if (exercise.exerciseLib.url != null) {
        launch('https://www.youtube.com/watch?v=' + exercise.exerciseLib.url);
        markExerciseAsDone(exercise);
      } else if (exercise.exerciseLib.fileData != null) {
        switch (exercise.exerciseLib.fileData.extension) {
          case '.pdf':
            await displayPDF(exercise.exerciseLib.fileData.retrieveUrl);
            markExerciseAsDone(exercise);
            break;
          default:
            await openFile(exercise.exerciseLib.fileData.retrieveUrl);
            markExerciseAsDone(exercise);
            break;
        }
      }
    } catch (ex) {
      // tja..
    }
  }

  Future<void> openFile(String retrieveUrl) async {
    var progress = new ProgressHUD(
      backgroundColor: Colors.black12,
      color: Colors.white,
      containerColor: ColorHelper.fromHex(model.primaryColor),
      borderRadius: 5.0,
    );
    showDialog(context: context, child: progress);

    var result = await DownloadHelper.downloadAsFile(retrieveUrl, cache: true);

    // pop loading animation
    Navigator.pop(context);

    OpenFile.open(result);
  }

  Future<void> displayPDF(String retrieveUrl) async {
    try {
      // show loading animation while downloading file
      var progress = new ProgressHUD(
        backgroundColor: Colors.black12,
        color: Colors.white,
        containerColor: ColorHelper.fromHex(model.primaryColor),
        borderRadius: 5.0,
      );
      showDialog(context: context, child: progress);

      var result =
          await DownloadHelper.downloadAsFile(retrieveUrl, cache: true);

      // pop loading animation
      Navigator.pop(context);

      await FlutterPdfViewer.loadFilePath('file://' + result);
    } catch (ex) {
      // pop loading animation
      Navigator.pop(context);
      ErrorMessage.show(_scaffoldKey, model.exerciseRetrievalFailed);
    }
  }

  @override
  void initState() {
    model.loadLocalization();
    tc = new TabController(length: 8, vsync: this);
    tcTop = new TabController(length: 8, vsync: this);

    tc.addListener(moduleNumberSelected);
    tcTop.addListener(expertiseSelected);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 5,
      child: new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          backgroundColor: ColorHelper.fromHex(model.primaryColor),
          title: new Text(model.title),
          centerTitle: true,
          bottom: new TabBar(
            isScrollable: true,
            controller: tcTop,
            tabs: getTabs(),
          ),
        ),
        body: new TabBarView(children: [
          getContentTabs(),
        ]),
      ),
    );
  }

  Container getContentTabs() {
    var expertise = model.expertises[tcTop.index];

    return new Container(
      color: new Color.fromARGB(255, 172, 168, 172),
      child: new ListView(
        children: [
          new Container(
            height: 40.0,
            color: new Color.fromARGB(255, 58, 57, 57),
            child: new TabBar(
              controller: tc,
              tabs: getModuleNumbers(expertise)
                  .map((moduleNr) => new Text(moduleNr))
                  .toList(),
            ),
          ),
          new Container(
            height: 10.0,
            width: double.infinity,
            color: new Color.fromARGB(255, 58, 57, 57),
          ),
          new Column(
            children: createPanels(expertise),
          ),
        ],
      ),
    );
  }

  List<Stack> createPanels(String expertiseCode) {
    var content = expertiseCode == 'Alle'
        ? model.selectedExercises
        : model.selectedExercises
            .where((e) => e.exerciseLib.expertise.name == expertiseCode);

    // if not all modules are selected
    if (tc.index != 0) {
      var moduleNr = getModuleNumbers(expertiseCode)[tc.index];
      content = content
          .where((e) => e.exerciseLib.moduleNumber == int.parse(moduleNr));
    }

    return content
        .map((exercise) => new Stack(
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(bottom: 10.0),
                  height: 190.0,
                  width: double.infinity,
                  color: new Color.fromARGB(205, 251, 251, 251),
                  child: new Stack(
                    children: <Widget>[
                      new Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        child: new Image(
                          image: new AdvancedNetworkImage(
                            exercise.exerciseLib.imageData.retrieveUrl,
                            useDiskCache: true,
                          ),
                          width: MediaQuery.of(context).size.width,
                          height: 190.0,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      new Container(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Text(
                            exercise.exerciseLib.name,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                        width: double.infinity,
                        color: new Color.fromARGB(255, 75, 75, 75),
                      ),
                      new Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        child: new Column(
                          children: <Widget>[
                            new Container(
                              width: MediaQuery.of(context).size.width,
                              color: getColorForStatus(exercise),
                              child: new Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new Text(
                                  getStatusText(exercise),
                                  textAlign: TextAlign.center,
                                  style: new TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            new Container(
                              width: MediaQuery.of(context).size.width,
                              color: new Color.fromARGB(255, 198, 198, 198),
                              child: new Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new Row(
                                  children: <Widget>[
                                    new Container(
                                      padding: EdgeInsets.only(
                                          left: 8.0, right: 8.0),
                                      height: 17.0,
                                      decoration: new BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        color: new Color.fromARGB(
                                            255, 170, 170, 170),
                                      ),
                                      child: Center(
                                        child: new Text(
                                          FormatHelper
                                              .formatExerciseLibFileExtension(
                                                  exercise.exerciseLib),
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                              color: Colors.white,
                                              fontSize: 11.0),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 3.0),
                                      child: new Container(
                                        width: 50.0,
                                        height: 17.0,
                                        decoration: new BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                          color: new Color.fromARGB(
                                              255, 170, 170, 170),
                                        ),
                                        child: Center(
                                          child: new Text(
                                            FormatHelper.formatModuleNumber(
                                              exercise.exerciseLib.moduleNumber,
                                            ),
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                                color: Colors.white,
                                                fontSize: 11.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                new Container(
                  height: 190.0,
                  width: double.infinity,
                  child: new FlatButton(
                    onPressed: () => openExerciseFile(exercise),
                    child: new Text(''),
                  ),
                ),
              ],
            ))
        .toList();
  }

  String getStatusText(Exercise exercise) {
    if (exercise.exerciseLib.isForm) {
      if (exercise.done) {
        return 'Afgerond';
      } else {
        if (exercise.exerciseLib.survey.progressInSeconds != null ||
            exercise.exerciseLib.survey.startedOn != null) {
          return 'Hervatten';
        } else {
          return 'Nog beginnen';
        }
      }
    } else {
      if (exercise.done) {
        return 'Afgerond';
      } else {
        return 'Nog beginnen';
      }
    }
  }

  Color getColorForStatus(Exercise exercise) {
    String status = getStatusText(exercise);

    switch (status) {
      case 'Afgerond':
        return new Color.fromARGB(255, 76, 175, 80);

      case 'Hervatten':
        return new Color.fromARGB(255, 255, 193, 7);

      case 'Nog beginnen':
        return new Color.fromARGB(255, 187, 187, 187);
    }
  }

  List<String> getModuleNumbers(String expertiseCode) {
    var content = expertiseCode == 'Alle'
        ? model.selectedExercises
        : model.selectedExercises
            .where((e) => e.exerciseLib.expertise.name == expertiseCode);

    List<String> modules = new List();
    modules.add('Alle');
    for (var c in content) {
      if (c.exerciseLib.moduleNumber < 9) {
        if (!modules.contains("0" + c.exerciseLib.moduleNumber.toString())) {
          modules.add("0" + c.exerciseLib.moduleNumber.toString());
        }
      } else {
        if (!modules.contains(c.exerciseLib.moduleNumber.toString())) {
          modules.add(c.exerciseLib.moduleNumber.toString());
        }
      }
    }

    return modules;
  }

  List<Tab> getTabs() {
    return model.expertises
        .map((expertiseCode) => new Tab(
              text: expertiseCode,
            ))
        .toList();
  }
}
