import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class ExercisePageViewModel {
  final ExerciseCollectionDTO dto;
  String primaryColor;
  String title;
  String exerciseRetrievalFailed;

  List<String> expertises = new List();

  List<Exercise> selectedExercises = new List();

  ExercisePageViewModel({this.dto}) {
    getExpertises();

    selectedExercises = this.dto.exercises;
  }

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');

    title = 'Mijn Oefeningen';
    exerciseRetrievalFailed =
        LocalizationService.get('ExerciseRetrievalFailed');
  }

  void getExpertises() {
    expertises.add('Alle');

    for (var program in dto.exercises) {
      if (!expertises.contains(program.exerciseLib.expertise.name)) {
        expertises.add(program.exerciseLib.expertise.name);
      }
    }
  }
}
