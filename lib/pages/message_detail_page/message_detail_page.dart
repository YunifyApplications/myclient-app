import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/pages/message_detail_page/message_detail_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/message_bubble.dart';

class MessageDetailPage extends StatefulWidget {
  final Message dto;

  MessageDetailPage({Key key, @required this.dto}) : super(key: key);

  @override
  _MessageDetailPage createState() => new _MessageDetailPage(this.dto);
}

class _MessageDetailPage extends State<MessageDetailPage> {
  MessageDetailPageViewModel model;

  _MessageDetailPage(Message dto) {
    model = new MessageDetailPageViewModel(dto: dto);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: new Key('message_detail_page'),
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(
          model.messageDetail,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        centerTitle: true,
      ),
      body: new Container(
        child: new ListView(
          children: <Widget>[
            new Container(
              color: new Color.fromARGB(255, 240, 240, 240),
              child: new MessageBubble(
                time: FormatHelper.formatTimeShort(model.dto.sendDate),
                message: model.dto.messageBody,
                senderDisplayName: model.dto.senderDisplayName,
                isRead: model.dto.recipients.every((e) => e.isRead) ||
                    model.dto.senderId != UserHelper.userId,
                isMe: model.dto.senderId == UserHelper.userId,
              ),
              padding: new EdgeInsets.only(top: 15.0, bottom: 15.0),
            ),
            new Padding(
              padding: const EdgeInsets.only(top: 0.0, left: 0.0),
              child: new SizedBox(
                  width: double.infinity,
                  height: 1.0,
                  // height: double.infinity,
                  child: new Container(
                      color: new Color.fromARGB(255, 227, 227, 227))),
            ),
            new Padding(
              padding:
                  const EdgeInsets.only(top: 12.0, left: 12.0, right: 12.0),
              child: new Container(
                decoration: new BoxDecoration(
                  color: new Color.fromARGB(255, 240, 240, 240),
                  borderRadius: BorderRadius.circular(4.0),
                  border: Border.all(
                    color: new Color.fromARGB(255, 227, 227, 227),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: getReadRecipients(),
                  ),
                ),
              ),
            ),
            new Padding(
              padding: const EdgeInsets.all(12.0),
              child: new Container(
                decoration: new BoxDecoration(
                  color: new Color.fromARGB(255, 240, 240, 240),
                  borderRadius: BorderRadius.circular(4.0),
                  border: Border.all(
                    color: new Color.fromARGB(255, 227, 227, 227),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: getUnreadRecipients(),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  List<Container> getReadRecipients() {
    List<Container> completeList = new List();

    completeList.add(
      new Container(
        child: new Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 0.0),
              child: new Icon(
                Icons.done_all,
                size: 16.0,
                color: Colors.black38,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 6.0, bottom: 0.0),
              child: new Text(model.read),
            )
          ],
        ),
      ),
    );

    completeList.add(
      new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 6.0, left: 0.0, bottom: 6.0),
          child: new SizedBox(
              width: double.infinity,
              height: 1.0,
              // height: double.infinity,
              child:
                  new Container(color: new Color.fromARGB(255, 227, 227, 227))),
        ),
      ),
    );

    completeList.addAll(model.dto.recipients
        .where((e) => e.isRead)
        .map(
          (recipient) => new Container(
                child: getRecipientItem(recipient),
              ),
        )
        .toList());

    return completeList;
  }

  List<Container> getUnreadRecipients() {
    List<Container> completeList = new List();

    completeList.add(new Container(
      child: new Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 0.0),
            child: new Icon(
              Icons.markunread,
              size: 16.0,
              color: Colors.black38,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 6.0, bottom: 0.0),
            child: new Text(model.notRead),
          )
        ],
      ),
    ));

    completeList.add(
      new Container(
        child: new Padding(
          padding: const EdgeInsets.only(top: 6.0, left: 0.0, bottom: 6.0),
          child: new SizedBox(
              width: double.infinity,
              height: 1.0,
              // height: double.infinity,
              child:
                  new Container(color: new Color.fromARGB(255, 227, 227, 227))),
        ),
      ),
    );

    completeList.addAll(model.dto.recipients
        .where((e) => !e.isRead)
        .map(
          (recipient) => new Container(
                child: getRecipientItem(recipient),
              ),
        )
        .toList());

    return completeList;
  }

  Container getRecipientItem(Recipient recipient) {
    return new Container(
      height: 50.0,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              new Container(
                margin: EdgeInsets.only(
                    left: 0.0, bottom: 0.0, top: 0.0, right: 0.0),
                child: new CircleAvatar(
                  radius: 90.0,
                  backgroundColor: new Color.fromARGB(255, 227, 227, 227),
                  backgroundImage: new AdvancedNetworkImage(
                    recipient.avatarUrl,
                    useDiskCache: true,
                  ),
                ),
                width: 35.0,
                height: 35.0,
                padding: EdgeInsets.all(1.0), // border width
                decoration: new BoxDecoration(
                  color: new Color.fromARGB(255, 227, 227, 227), // border color
                  shape: BoxShape.circle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: new Text(
                  recipient.displayName,
                  style: new TextStyle(
                      color: new Color.fromARGB(255, 120, 120, 120)),
                ),
              ),
            ],
          ),
          new Padding(
            padding: const EdgeInsets.only(top: 6.0, left: 30.0, bottom: 6.0),
            child: new SizedBox(
                width: double.infinity,
                height: 1.0,
                // height: double.infinity,
                child: new Container(
                    color: new Color.fromARGB(255, 227, 227, 227))),
          ),
        ],
      ),
    );
  }
}
