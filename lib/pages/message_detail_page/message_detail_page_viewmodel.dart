import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class MessageDetailPageViewModel {
  Message dto;
  String primaryColor;
  String messageDetail;
  String read;
  String notRead;

  MessageDetailPageViewModel({this.dto});

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');
    messageDetail = LocalizationService.get('MessageDetail');
    read = LocalizationService.get('Read');
    notRead = LocalizationService.get('NotRead');
  }
}
