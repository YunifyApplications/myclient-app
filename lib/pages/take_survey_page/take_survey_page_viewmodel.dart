import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class TakeSurveyPageViewModel {
  final Survey survey;
  bool done;
  String title;
  String primaryColor;
  String answersCantBeSaved;
  String surveyCantBeCompleted;
  String surveyCantBeCompletedError;

  TakeSurveyPageViewModel({this.survey, this.done}) {
    this
        .survey
        .surveyLib
        .sections
        .sort((a, b) => a.sortOrder.compareTo(b.sortOrder));

    for (var section in survey.surveyLib.sections) {
      section.questions.sort((a, b) => a.sortOrder.compareTo(b.sortOrder));
    }
  }

  void loadLocalization() {
    title = 'Survey afnemen';
    primaryColor = LocalizationService.get('PrimaryColor');
    answersCantBeSaved = LocalizationService.get('AnswersCantBeSaved');
    surveyCantBeCompleted = LocalizationService.get('SurveyCantBeCompleted');
    surveyCantBeCompletedError =
        LocalizationService.get('SurveyCantBeCompletedError');
  }
}
