import 'dart:async';

import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/login_page/login_page.dart';
import 'package:myclient_app/pages/take_survey_page/take_survey_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';

/*
done = don't submit or edit anything

- input options are:
SingleLineTextBox
X Header
X SingleSelect
X MultiSelect
X SingleSelectMatrix
X MultiLineTextBox
X Trackbar
X HeaderInfo
X DropDownList
X IntegerNumericBox
X DropDownListWithInput

- input to do:

*/

class MultiLineTextBoxController {
  final TextEditingController controller;
  final Question question;
  MultiLineTextBoxController({this.controller, this.question});
}

class SingleLineTextBoxController {
  final TextEditingController controller;
  final Question question;
  SingleLineTextBoxController({this.controller, this.question});
}

class SingleSelectController {
  OptionChoice selectedOption;
  final Question question;
  SingleSelectController({this.selectedOption, this.question});
}

class SingleSelectMatrixController {
  OptionChoice selectedOption;
  final Question question;
  SingleSelectMatrixController({this.selectedOption, this.question});
}

class MultiSelectController {
  List<OptionChoice> selectedOptions;
  final Question question;
  MultiSelectController({this.selectedOptions, this.question});
}

class TrackbarController {
  double trackbarValue;
  final Question question;
  TrackbarController({this.trackbarValue, this.question});
}

class DropDownListController {
  OptionChoice selectedOption;
  final Question question;
  DropDownListController({this.selectedOption, this.question});
}

class DropDownListWithInputController {
  OptionChoice selectedOption;
  final TextEditingController controller;
  final Question question;
  DropDownListWithInputController(
      {this.selectedOption, this.controller, this.question});
}

class IntegerNumericBoxController {
  TextEditingController controller;
  final Question question;
  IntegerNumericBoxController({this.controller, this.question});
}

class TakeSurveyPage extends StatefulWidget {
  final Survey survey;
  final bool done;
  TakeSurveyPage({Key key, @required this.survey, @required this.done})
      : super(key: key);

  @override
  _TakeSurveyPage createState() =>
      new _TakeSurveyPage(survey: this.survey, done: this.done);
}

class _TakeSurveyPage extends State<TakeSurveyPage> {
  TakeSurveyPageViewModel model;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  // contains text controllers for multiLineTextBox questions
  List<MultiLineTextBoxController> multiLineTextBoxControllers = new List();
  // contains the single select values for single select questions
  List<SingleSelectController> singleSelectControllers = new List();
  // contains the single select values for single select matrix questions
  List<SingleSelectMatrixController> singleSelectMatrixControllers = new List();
  // contains the trackbar values for trackbar questions
  List<TrackbarController> trackbarControllers = new List();
  // contains the dropdownlist values for dropdownlist questions
  List<DropDownListController> dropDownListControllers = new List();
  // contains the text controllers for singleLineTextBox questions
  List<SingleLineTextBoxController> singleLineTextBoxControllers = new List();
  // contains the text controllers for IntegerNumericBox questions
  List<IntegerNumericBoxController> integerNumericBoxControllers = new List();
  // contains the text controllers for dropdownList with input questions
  List<DropDownListWithInputController> dropdownListWithInputControllers =
      new List();
  // contains the selected values for multi select questions
  List<MultiSelectController> multiSelectControllers = new List();

  _TakeSurveyPage({@required Survey survey, @required bool done}) {
    model = new TakeSurveyPageViewModel(survey: survey, done: done);
  }

  Timer timer;

  @override
  void initState() {
    model.loadLocalization();
    createControllers();

    // start timer for keeping track of take duration
    timer =
        Timer.periodic(new Duration(seconds: 5), sendSurveyDurationToServer);

    super.initState();
  }

  void sendSurveyDurationToServer(Timer t) {
    if (model.survey.progressInSeconds == null) {
      model.survey.progressInSeconds = 0;
    }

    model.survey.progressInSeconds += 5;
    httpService.saveSurveyTakeDuration(new SaveSurveyTakeDurationDTO(
        surveyLibId: model.survey.surveyLib.id,
        duration: model.survey.progressInSeconds));
  }

  @override
  void dispose() {
    for (var c in multiLineTextBoxControllers) c.controller.dispose();
    for (var c in singleLineTextBoxControllers) c.controller.dispose();
    for (var c in integerNumericBoxControllers) c.controller.dispose();
    for (var c in dropdownListWithInputControllers) c.controller.dispose();

    timer.cancel();

    super.dispose();
  }

  // create controllers for questions that need them
  void createControllers() {
    List<Question> questions = new List();
    for (var section in model.survey.surveyLib.sections) {
      questions.addAll(section.questions);
    }

    for (var question in questions) {
      // create controllers

      ///////////////////////////////////////////////////////////////
      if (question.questionType.name == 'MultiLineTextBox') {
        var controller = new TextEditingController(
            text: (question.answerText == null ? '' : question.answerText));
        multiLineTextBoxControllers.add(new MultiLineTextBoxController(
            controller: controller, question: question));
      }
      ///////////////////////////////////////////////////////////////
      else if (question.questionType.name == 'SingleLineTextBox') {
        var controller = new TextEditingController(
            text: (question.answerText == null ? '' : question.answerText));
        singleLineTextBoxControllers.add(new SingleLineTextBoxController(
            controller: controller, question: question));
      }
      ///////////////////////////////////////////////////////////////
      else if (question.questionType.name == 'IntegerNumericBox') {
        var controller = new TextEditingController(
            text: (question.answerText == null ? '' : question.answerText));
        integerNumericBoxControllers.add(new IntegerNumericBoxController(
            controller: controller, question: question));
      }
      ///////////////////////////////////////////////////////////////
      else if (question.questionType.name == 'SingleSelect') {
        var selectedVal = question.selectedValue;
        var selectedOption = null;
        if (selectedVal != null) {
          var result = question.optionGroup.optionChoices
              .where((e) => e.id == selectedVal);
          if (result.length > 0) selectedOption = result.first;
        }

        SingleSelectController controller = new SingleSelectController(
            selectedOption: selectedOption, question: question);
        singleSelectControllers.add(controller);
      }
      ///////////////////////////////////////////////////////////////
      else if (question.questionType.name == 'SingleSelectMatrix') {
        var selectedVal = question.selectedValue;
        var selectedOption = null;
        if (selectedVal != null) {
          var result = question.optionGroup.optionChoices
              .where((e) => e.id == selectedVal);
          if (result.length > 0) selectedOption = result.first;
        }

        SingleSelectMatrixController controller =
            new SingleSelectMatrixController(
                selectedOption: selectedOption, question: question);
        singleSelectMatrixControllers.add(controller);
      }
      ///////////////////////////////////////////////////////////////
      else if (question.questionType.name == 'MultiSelect') {
        if (question.answerText != null) {
          List<String> selectedIds = question.answerText.split(',');
          List<OptionChoice> selectedOptions = new List();

          for (var id in selectedIds) {
            var results = question.optionGroup.optionChoices
                .where((e) => e.id.toString() == id);

            if (results.length > 0) {
              selectedOptions.add(results.elementAt(0));
            }
          }

          MultiSelectController controller = new MultiSelectController(
              selectedOptions: selectedOptions, question: question);
          multiSelectControllers.add(controller);
        } else {
          MultiSelectController controller = new MultiSelectController(
              selectedOptions: new List(), question: question);
          multiSelectControllers.add(controller);
        }
      }
      ///////////////////////////////////////////////////////////////
      else if (question.questionType.name == 'Trackbar') {
        trackbarControllers.add(
          new TrackbarController(
            trackbarValue:
                question.trackbarValue == null ? 0.0 : question.trackbarValue,
            question: question,
          ),
        );
      }
      ///////////////////////////////////////////////////////////////
      else if (question.questionType.name == 'DropDownList') {
        var selectedVal = question.selectedValue;
        var selectedOption = null;
        if (selectedVal != null) {
          var result = question.optionGroup.optionChoices
              .where((e) => e.id == selectedVal);
          if (result.length > 0) selectedOption = result.first;
        }

        dropDownListControllers.add(
          new DropDownListController(
            selectedOption: selectedOption,
            question: question,
          ),
        );
      }
      ///////////////////////////////////////////////////////////////
      else if (question.questionType.name == 'DropDownListWithInput') {
        var selectedVal = question.selectedValue;
        var selectedOption = null;
        if (selectedVal != null) {
          var result = question.optionGroup.optionChoices
              .where((e) => e.id == selectedVal);
          if (result.length > 0) selectedOption = result.first;
        }

        var controller = new TextEditingController(
            text: (question.answerText == null ? '' : question.answerText));

        dropdownListWithInputControllers.add(
          new DropDownListWithInputController(
            selectedOption: selectedOption,
            controller: controller,
            question: question,
          ),
        );
      }
    }
  }

  void finishSurvey() async {
    try {
      var result = await httpService.finishSurvey(
          new FinishSurveyDTO(surveyLibId: model.survey.surveyLib.id));

      if (result.resultStatus == RequestResult.SUCCESS) {
        if (result.finishResult == FinishSurveyResult.SUCCESS) {
          model.done = true;
          Navigator.pop(context);
        } else {
          ErrorMessage.show(_scaffoldKey, result.errorMessage);
        }
      } else {
        ErrorMessage.show(_scaffoldKey, model.surveyCantBeCompleted);
      }
    } catch (ex) {
      ErrorMessage.show(_scaffoldKey, model.surveyCantBeCompletedError);
    }
  }

  void saveSingleLineTextBoxToServer(Question question, String val) async {
    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              qu.answerText = val;
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(questionId: question.id, answer: val));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveMultiLineTextBoxToServer(Question question, String val) async {
    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              qu.answerText = val;
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(questionId: question.id, answer: val));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveDropdownListToServer(Question question, String val) async {
    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              qu.selectedValue = int.parse(val);
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(questionId: question.id, answer: val));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveDropdownListWithInputToServer(Question question, String val) async {
    var controllers = dropdownListWithInputControllers
        .where((e) => e.question.id == question.id);
    DropDownListWithInputController controller = null;
    if (controllers.length > 0) {
      controller = controllers.first;
    }

    if (controller == null) {
      return;
    }

    String valToSave = '';
    bool isId = false;
    if (controller.controller.text == '') {
      valToSave = controller.selectedOption.id.toString();
      isId = true;
    } else {
      valToSave = controller.controller.text;
    }

    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              if (isId) {
                qu.selectedValue = int.parse(valToSave);
              } else {
                qu.selectedValue = controller.selectedOption.id;
                qu.answerText = valToSave;
              }
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(
              questionId: question.id, answer: valToSave));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveSingleSelectToServer(Question question, OptionChoice val) async {
    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              qu.selectedValue = val.id;
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(
              questionId: question.id, answer: val.id.toString()));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveSingleSelectMatrixToServer(
      Question question, OptionChoice val) async {
    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              qu.selectedValue = val.id;
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(
              questionId: question.id, answer: val.id.toString()));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveIntegerNumericBoxToServer(Question question, String val) async {
    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              qu.answerText = val;
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(questionId: question.id, answer: val));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveTrackbarToServer(Question question, double val) async {
    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              qu.trackbarValue = val;
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(
              questionId: question.id, answer: val.toString()));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveMultiSelectToServer(Question question, bool val) async {
    var controllers =
        multiSelectControllers.where((e) => e.question.id == question.id);

    String valToSave = '';
    if (controllers.length > 0) {
      int i = 0;
      for (var c in controllers.first.selectedOptions) {
        if (i == 0) {
          valToSave += c.id.toString();
        } else {
          valToSave += ',' + c.id.toString();
        }
        i++;
      }
    }

    setState(() {
      for (var sec in model.survey.surveyLib.sections) {
        for (var qu in sec.questions) {
          if (qu.id == question.id) {
            setState(() {
              qu.answerText = valToSave;
            });
          }
        }
      }
    });

    try {
      var result = await httpService.saveQuestionAnswer(
          new SaveQuestionAnswerDTO(
              questionId: question.id, answer: valToSave));

      if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      }
    } catch (ex) {
      // show format error
      ErrorMessage.show(_scaffoldKey, model.answersCantBeSaved);
    }
  }

  void saveQuestion(Question question, newValue) {
    //    type:                   |   param:                            |   server wants:
    //    SingleLineTextBox       |   String                            |   String with text
    //    Header                  |                                     |
    //    SingleSelect            |   OptionChoice                      |   String with id
    //    MultiSelect             |   OptionChoice                      |   String with comma separated ids
    //    SingleSelectMatrix      |   OptionChoice                      |   String with id
    //    MultiLineTextBox        |   String                            |   String with text
    //    Trackbar                |   Int value                         |   String with value
    //    HeaderInfo              |                                     |
    //    DropDownList            |   Int selected id                   |   String with optionChoice id
    //    IntegerNumericBox       |   String value                      |   String with value
    //    DropDownListWithInput   |   String text or id of optionChoice |   String text or optionChoice id

    switch (question.questionType.name) {
      case 'HeaderInfo':
        // do nothing
        break;
      case 'MultiLineTextBox':
        saveMultiLineTextBoxToServer(question, newValue);
        break;
      case 'SingleSelect':
        saveSingleSelectToServer(question, newValue);
        break;
      case 'MultiSelect':
        saveMultiSelectToServer(question, newValue);
        break;
      case 'Trackbar':
        saveTrackbarToServer(question, newValue);
        break;
      case 'DropDownList':
        saveDropdownListToServer(question, newValue);
        break;
      case 'DropDownListWithInput':
        saveDropdownListWithInputToServer(question, newValue);
        break;
      case 'Header':
        // do nothing
        break;
      case 'SingleLineTextBox':
        saveSingleLineTextBoxToServer(question, newValue);
        break;
      case 'IntegerNumericBox':
        saveIntegerNumericBoxToServer(question, newValue.toString());
        break;
      case 'SingleSelectMatrix':
        saveSingleSelectMatrixToServer(question, newValue);
        break;
      default:
        // do nothing
        break;
    }
  }

  void setSingleSelectValue(
      SingleSelectController controller, OptionChoice value) {
    setState(() {
      controller.selectedOption = value;
    });
  }

  void setSingleSelectMatrixValue(
      SingleSelectMatrixController controller, OptionChoice value) {
    setState(() {
      controller.selectedOption = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new Container(
        child: new ListView(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.all(10.0),
              child: new Text(
                model.survey.surveyLib.name,
                style: new TextStyle(
                  color: new Color.fromARGB(255, 60, 60, 60),
                  fontSize: 20.0,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15.0),
              child: separator(),
            ),
            new Column(
              children: [
                new Column(
                  children: createQuestions(),
                ),
                new Container(
                  width: MediaQuery.of(context).size.width - 20,
                  margin: EdgeInsets.only(bottom: 10.0),
                  child: new FlatButton(
                    onPressed: model.done ? null : finishSurvey,
                    child: new Text(
                      model.done ? 'Al ingeleverd' : 'Inleveren',
                      style: new TextStyle(color: Colors.white),
                    ),
                    color: ColorHelper.fromHex(model.primaryColor),
                    disabledColor: ColorHelper.fromHex(model.primaryColor),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Question getQuestionById(int id) {
    List<Question> question = new List();
    for (var section in model.survey.surveyLib.sections) {
      question.addAll(section.questions);
    }
    return question.where((q) => q.id == id).first;
  }

  List<Widget> createQuestions() {
    List<Question> question = new List();
    for (var section in model.survey.surveyLib.sections) {
      question.addAll(section.questions);
    }

    return question
        .map((question) => getWidgetWithSeparator(question))
        .toList();
  }

  bool shouldDisplayWidget(Question question) {
    if (question.parentQuestion != null) {
      var controllers = multiLineTextBoxControllers
          .where((e) => e.question.id == question.parentQuestion.id)
          .toList();
      if (controllers.length > 0) {
        if (controllers.first.controller.text == '') {
          return false;
        }
      }

      var optionControllers = singleSelectControllers
          .where((e) => e.question.id == question.parentQuestion.id)
          .toList();
      if (optionControllers.length > 0) {
        if (question.parentOptionChoice != null) {
          if (optionControllers.first.selectedOption.id ==
              question.parentOptionChoice.id) {
            return false;
          }
        }
      }

      var dropdownControllers = dropDownListControllers
          .where((e) => e.question.id == question.parentQuestion.id)
          .toList();
      if (dropdownControllers.length > 0) {
        if (question.parentOptionChoice != null) {
          if (dropdownControllers.first.selectedOption.id ==
              question.parentOptionChoice.id) {
            return false;
          }
        }
      }
    }

    return true;
  }

  Widget getWidgetWithSeparator(Question question) {
    if (!shouldDisplayWidget(question)) {
      return new Container();
    }

    return new Column(
      children: <Widget>[
        getWidgetForQuestion(question),
        Padding(
          padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
          child: separator(),
        ),
      ],
    );
  }

  Widget getWidgetForQuestion(Question question) {
    switch (question.questionType.name) {
      case 'HeaderInfo':
        return createHeaderInfo(question);
      case 'MultiLineTextBox':
        return createMultiLineTextBox(question);
      case 'SingleSelect':
        return createSingleSelect(question);
      case 'MultiSelect':
        return createMultiSelect(question);
      case 'Trackbar':
        return createTrackbar(question);
      case 'DropDownList':
        return createDropDownList(question);
      case 'DropDownListWithInput':
        return createDropDownListWithInput(question);
      case 'Header':
        return createHeader(question);
      case 'SingleLineTextBox':
        return createSingleLineTextBox(question);
      case 'IntegerNumericBox':
        return createIntegerNumericBox(question);
      case 'SingleSelectMatrix':
        return createSingleSelectMatrix(question);
      default:
        return new Text(question.questionType.name + ' not implemented');
    }
  }

  int validateIntegerNumericBox(
      String val, int min, int max, TextEditingController controller) {
    int v = int.parse(val);
    if (v > max) {
      v = max;
      controller.text = v.toString();
    }
    if (v < min) {
      v = min;
      controller.text = v.toString();
    }

    return v;
  }

  void setMultiSelectValue(
      MultiSelectController controller, OptionChoice choice) {
    setState(() {
      if (controller.selectedOptions.contains(choice)) {
        controller.selectedOptions.remove(choice);
      } else {
        controller.selectedOptions.add(choice);
      }
    });
  }

  Widget createMultiSelect(Question question) {
    var controller =
        multiSelectControllers.where((e) => e.question.id == question.id).first;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getWidgetName(question),
        new Column(
          children: question.optionGroup.optionChoices
              .map((choice) => new Row(
                    children: <Widget>[
                      new Checkbox(
                        value: controller.selectedOptions.contains(choice),
                        onChanged: (val) {
                          if (!model.done) {
                            setMultiSelectValue(controller, choice);
                            saveQuestion(question, val);
                          }
                        },
                      ),
                      new Container(
                        width: MediaQuery.of(context).size.width - 60,
                        child: new Text(choice.name),
                      ),
                    ],
                  ))
              .toList(),
        ),
      ],
    );
  }

  Widget createIntegerNumericBox(Question question) {
    TextEditingController controller = integerNumericBoxControllers
        .where((e) => e.question.id == question.id)
        .first
        .controller;

    String minVal = null;
    String maxVal = null;

    try {
      minVal = question?.questionTypeGroup?.questionTypePropertyValues
          ?.where((e) => e.questionTypeProperty.name == 'NumMinVal')
          ?.first
          ?.value;
    } catch (ex) {}
    try {
      maxVal = question?.questionTypeGroup?.questionTypePropertyValues
          ?.where((e) => e.questionTypeProperty.name == 'NumMaxVal')
          ?.first
          ?.value;
    } catch (ex) {}

    int min = minVal != null ? int.parse(minVal) : 0;
    int max = maxVal != null ? int.parse(maxVal) : 999;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getWidgetName(question),
        new Container(
          margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 0.0),
          padding: EdgeInsets.only(left: 5.0, right: 5.0),
          child: new TextField(
            enabled: !model.done,
            onChanged: (val) {
              saveQuestion(question,
                  validateIntegerNumericBox(val, min, max, controller));
            },
            controller: controller,
            keyboardType: TextInputType.number,
            maxLines: 1,
            cursorColor: Colors.black87,
            style: new TextStyle(
                fontSize: 16.0,
                color: Colors.black87,
                decoration: TextDecoration.none),
            decoration: new InputDecoration(
              border: InputBorder.none,
              disabledBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
              hintText: '',
              hintStyle: new TextStyle(
                color: new Color.fromARGB(255, 150, 150, 150),
              ),
            ),
          ),
          decoration: new BoxDecoration(
            border: Border.all(
                color: new Color.fromARGB(255, 207, 214, 217), width: 2.0),
          ),
        ),
      ],
    );
  }

  Widget createDropDownListWithInput(Question question) {
    var controller = dropdownListWithInputControllers
        .where((e) => e.question.id == question.id)
        .first;

    var val = null;
    if (controller != null) {
      if (controller.selectedOption != null) {
        val = controller.selectedOption.id.toString();
      }
    }

    Widget tb = new Container(
      margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
      padding: EdgeInsets.only(left: 5.0, right: 5.0),
      child: new TextField(
        enabled: !model.done,
        onChanged: (val) => saveQuestion(question, val),
        controller: controller.controller,
        keyboardType: TextInputType.text,
        maxLines: 1,
        cursorColor: Colors.black87,
        style: new TextStyle(
            fontSize: 16.0,
            color: Colors.black87,
            decoration: TextDecoration.none),
        decoration: new InputDecoration(
          border: InputBorder.none,
          disabledBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
          focusedErrorBorder: InputBorder.none,
          hintText: '',
          hintStyle: new TextStyle(
            color: new Color.fromARGB(255, 150, 150, 150),
          ),
        ),
      ),
      decoration: new BoxDecoration(
        border: Border.all(
            color: new Color.fromARGB(255, 207, 214, 217), width: 2.0),
      ),
    );

    if (val == null) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getWidgetName(question),
          tb,
          new DropdownButton<String>(
            items: question.optionGroup.optionChoices.map((OptionChoice value) {
              return new DropdownMenuItem<String>(
                value: value.id.toString(),
                child: new Container(
                  width: MediaQuery.of(context).size.width - 45,
                  child: new Text(value.name),
                ),
              );
            }).toList(),
            onChanged: (newValue) {
              controller.selectedOption = question.optionGroup.optionChoices
                  .where((e) => e.id.toString() == newValue)
                  .first;

              if (!model.done) {
                saveQuestion(question, newValue);
              }
            },
          ),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getWidgetName(question),
          tb,
          new DropdownButton<String>(
            items: question.optionGroup.optionChoices.map((OptionChoice value) {
              return new DropdownMenuItem<String>(
                value: value.id.toString(),
                child: new Container(
                  width: MediaQuery.of(context).size.width - 45,
                  child: new Text(value.name),
                ),
              );
            }).toList(),
            onChanged: (newValue) {
              controller.selectedOption = question.optionGroup.optionChoices
                  .where((e) => e.id.toString() == newValue)
                  .first;

              if (!model.done) {
                saveQuestion(question, newValue);
              }
            },
            value: val,
          ),
        ],
      );
    }
  }

  Widget createDropDownList(Question question) {
    var controller = dropDownListControllers
        .where((e) => e.question.id == question.id)
        .first;

    var val = null;
    if (controller != null) {
      if (controller.selectedOption != null) {
        val = controller.selectedOption.id.toString();
      }
    }

    if (val == null) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getWidgetName(question),
          new DropdownButton<String>(
            items: question.optionGroup.optionChoices.map((OptionChoice value) {
              return new DropdownMenuItem<String>(
                value: value.id.toString(),
                child: new Container(
                  width: MediaQuery.of(context).size.width - 45,
                  child: new Text(value.name),
                ),
              );
            }).toList(),
            onChanged: (newValue) {
              controller.selectedOption = question.optionGroup.optionChoices
                  .where((e) => e.id.toString() == newValue)
                  .first;

              if (!model.done) {
                saveQuestion(question, newValue);
              }
            },
          ),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          getWidgetName(question),
          new DropdownButton<String>(
            items: question.optionGroup.optionChoices.map((OptionChoice value) {
              return new DropdownMenuItem<String>(
                value: value.id.toString(),
                child: new Container(
                  width: MediaQuery.of(context).size.width - 45,
                  child: new Text(value.name),
                ),
              );
            }).toList(),
            onChanged: (newValue) {
              controller.selectedOption = question.optionGroup.optionChoices
                  .where((e) => e.id.toString() == newValue)
                  .first;

              if (!model.done) {
                saveQuestion(question, newValue);
              }
            },
            value: val,
          ),
        ],
      );
    }
  }

  Widget createHeader(Question question) {
    return new Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 0.0),
      child: new Text(
        question.questionNumber + '.  ' + question.name,
        style: new TextStyle(
          color: new Color.fromARGB(255, 60, 60, 60),
          fontSize: 18.0,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Widget createTrackbar(Question question) {
    var controller =
        trackbarControllers.where((e) => e.question.id == question.id).first;

    String minVal = null;
    String maxVal = null;
    String largeTickInterval = null;

    try {
      minVal = question?.questionTypeGroup?.questionTypePropertyValues
          ?.where((e) => e.questionTypeProperty.name == 'MinVal')
          ?.first
          ?.value;
    } catch (ex) {}
    try {
      maxVal = question?.questionTypeGroup?.questionTypePropertyValues
          ?.where((e) => e.questionTypeProperty.name == 'MaxVal')
          ?.first
          ?.value;
    } catch (ex) {}
    try {
      largeTickInterval = question
          ?.questionTypeGroup?.questionTypePropertyValues
          ?.where((e) => e.questionTypeProperty.name == 'LargeTickInterval')
          ?.first
          ?.value;
    } catch (ex) {}

    minVal = (minVal == null ? '0' : minVal);
    maxVal = (minVal == null ? '10' : maxVal);
    largeTickInterval = (largeTickInterval == null ? '1' : largeTickInterval);

    var val = ((controller.trackbarValue >= double.parse(minVal) &&
            controller.trackbarValue <= double.parse(maxVal))
        ? controller.trackbarValue
        : (double.parse(maxVal) - double.parse(minVal)) / 2);

    double min = double.parse(minVal);
    double max = double.parse(maxVal);
    int interval = int.parse(largeTickInterval);
    double devisions = (max - min) / interval.toDouble();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getWidgetName(question),
        new Slider(
          value: val,
          onChanged: (e) {
            saveQuestion(question, e);
            setState(() {
              if (!model.done) {
                controller.trackbarValue = e;
              }
            });
          },
          min: min,
          max: max,
          divisions: devisions.toInt(),
        )
      ],
    );
  }

  Widget getWidgetName(Question question) {
    return new Container(
      width: MediaQuery.of(context).size.width - 20.0,
      padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
      child: new Text(
        question.questionNumber +
            '.  ' +
            question.name +
            (question.required ? '*' : ''),
        textAlign: TextAlign.left,
        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16.0),
      ),
    );
  }

  Widget createSingleSelectMatrix(Question question) {
    var controller = singleSelectMatrixControllers
        .where((e) => e.question.id == question.id)
        .first;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getWidgetName(question),
        new Column(
          children: question.optionGroup.optionChoices
              .map((choice) => new Row(
                    children: <Widget>[
                      new Radio(
                        value: choice,
                        groupValue: controller.selectedOption,
                        onChanged: (val) {
                          if (!model.done) {
                            saveQuestion(question, val);
                            setSingleSelectMatrixValue(controller, val);
                          }
                        },
                      ),
                      new Container(
                        width: MediaQuery.of(context).size.width - 60,
                        child: new Text(choice.name),
                      ),
                    ],
                  ))
              .toList(),
        ),
      ],
    );
  }

  Widget createSingleSelect(Question question) {
    var controller = singleSelectControllers
        .where((e) => e.question.id == question.id)
        .first;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getWidgetName(question),
        new Column(
          children: question.optionGroup.optionChoices
              .map((choice) => new Row(
                    children: <Widget>[
                      new Radio(
                        value: choice,
                        groupValue: controller.selectedOption,
                        onChanged: (val) {
                          if (!model.done) {
                            saveQuestion(question, val);
                            setSingleSelectValue(controller, val);
                          }
                        },
                      ),
                      new Container(
                        width: MediaQuery.of(context).size.width - 60,
                        child: new Text(choice.name),
                      ),
                    ],
                  ))
              .toList(),
        ),
      ],
    );
  }

  Widget createSingleLineTextBox(Question question) {
    TextEditingController controller = singleLineTextBoxControllers
        .where((e) => e.question.id == question.id)
        .first
        .controller;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getWidgetName(question),
        new Container(
          margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 0.0),
          padding: EdgeInsets.only(left: 5.0, right: 5.0),
          child: new TextField(
            enabled: !model.done,
            onChanged: (val) => saveQuestion(question, val),
            controller: controller,
            keyboardType: TextInputType.text,
            maxLines: 1,
            cursorColor: Colors.black87,
            style: new TextStyle(
                fontSize: 16.0,
                color: Colors.black87,
                decoration: TextDecoration.none),
            decoration: new InputDecoration(
              border: InputBorder.none,
              disabledBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
              hintText: '',
              hintStyle: new TextStyle(
                color: new Color.fromARGB(255, 150, 150, 150),
              ),
            ),
          ),
          decoration: new BoxDecoration(
            border: Border.all(
                color: new Color.fromARGB(255, 207, 214, 217), width: 2.0),
          ),
        ),
      ],
    );
  }

  Widget createMultiLineTextBox(Question question) {
    TextEditingController controller = multiLineTextBoxControllers
        .where((e) => e.question.id == question.id)
        .first
        .controller;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        getWidgetName(question),
        new Container(
          margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 0.0),
          padding: EdgeInsets.only(left: 5.0, right: 5.0),
          child: new TextField(
            enabled: !model.done,
            onChanged: (val) => saveQuestion(question, val),
            controller: controller,
            keyboardType: TextInputType.multiline,
            maxLines: 2,
            cursorColor: Colors.black87,
            style: new TextStyle(
                fontSize: 16.0,
                color: Colors.black87,
                decoration: TextDecoration.none),
            decoration: new InputDecoration(
              border: InputBorder.none,
              disabledBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              focusedErrorBorder: InputBorder.none,
              hintText: '',
              hintStyle: new TextStyle(
                color: new Color.fromARGB(255, 150, 150, 150),
              ),
            ),
          ),
          decoration: new BoxDecoration(
            border: Border.all(
                color: new Color.fromARGB(255, 207, 214, 217), width: 2.0),
          ),
        ),
      ],
    );
  }

  Widget createHeaderInfo(Question question) {
    return new Container(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 0.0),
      child: new Text(question.questionNumber + '.  ' + question.name),
    );
  }

  Widget separator() {
    return new Container(
      margin: EdgeInsets.only(bottom: 0.0, left: 10.0, right: 10.0),
      width: double.infinity,
      height: 0.5,
      color: new Color.fromARGB(255, 180, 180, 180),
    );
  }
}
