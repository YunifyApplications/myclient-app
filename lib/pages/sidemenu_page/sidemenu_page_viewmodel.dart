import 'package:myclient_app/services/localization_service.dart';

class SideMenuPageViewModel {
  String backgroundColor;
  String dataRetrievalFailed;
  String profileIcon;
  String calenderIcon;
  String browserIcon;
  String lineGraphIcon;
  String logoutIcon;
  String manualIcon;

  SideMenuPageViewModel() {
    backgroundColor = LocalizationService.get('PrimaryColor');
    dataRetrievalFailed = LocalizationService.get('DataRetrievalFailed');
    profileIcon = 'assets/icons/profile-male_64px_White.png';
    calenderIcon = 'assets/icons/calendar_64px_White.png';
    browserIcon = 'assets/icons/browser_64px_White.png';
    lineGraphIcon = 'assets/icons/linegraph_64px_White.png';
    logoutIcon = 'assets/icons/logout.png';
    manualIcon = 'assets/icons/book_64px_White.png';
  }
}
