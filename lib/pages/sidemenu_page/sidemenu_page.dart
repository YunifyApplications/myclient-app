import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/ehealth_page/ehealth_page.dart';
import 'package:myclient_app/pages/login_page/login_page.dart';
import 'package:myclient_app/pages/profile_page/profile_page.dart';
import 'package:myclient_app/pages/programs_page/programs_page.dart';
import 'package:myclient_app/pages/sessions_page/sessions_page.dart';
import 'package:myclient_app/pages/sidemenu_page/sidemenu_page_viewmodel.dart';
import 'package:myclient_app/services/localization_service.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/dashboard_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';
import 'package:myclient_app/widgets/sidemenu_item.dart';

class SideMenuPage extends StatefulWidget {
  SideMenuPage({Key key}) : super(key: key);

  @override
  _SideMenuPage createState() => new _SideMenuPage();
}

class _SideMenuPage extends State<SideMenuPage> {
  SideMenuPageViewModel model = new SideMenuPageViewModel();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void dispose() {
    super.dispose();
  }

  void navigateToSessionsPage() async {
    try {
      var result = await httpService.getSessions();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.of(context).pop(); // pop side menu
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new SessionsPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error because retrieving data failed
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error because retrieving data failed
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToEhealthPage() async {
    try {
      var result = await httpService.getEhealthMods();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.of(context).pop(); // pop side menu
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new EhealthPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToProfilePage() async {
    try {
      var result = await httpService.getProfile();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.of(context).pop(); // pop side menu
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new ProfilePage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error because retrieving data failed
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error because retrieving data failed
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToProgramsPage() async {
    try {
      var result = await httpService.getPrograms();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.of(context).pop(); // pop side menu
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new ProgramsPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error because retrieving data failed
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void takeTour() {
    DashboardHelper.isFirstTime = true;
    Navigator.of(context).pop();
  }

  void logout() {
    UserHelper.clear();

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => new LoginPage()));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: new Container(
        color: ColorHelper.fromHex(model.backgroundColor),
        child: new ListView(
          children: <Widget>[
            // close button
            new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 8.0, top: 8.0, right: 20.0, bottom: 8.0),
                    child: new Container(
                      width: 35.0,
                      height: 35.0,
                      decoration: new BoxDecoration(
                        color: Colors.transparent,
                        shape: BoxShape.circle,
                        border: new Border.all(
                          color: Colors.white,
                          width: 1.0,
                        ),
                      ),
                      child: new Center(
                        child: new Text(
                          'X',
                          style: new TextStyle(
                              color: Colors.white, fontSize: 20.0),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),

            new SideMenuItem(
              text: 'Mijn Profiel',
              iconUrl: model.profileIcon,
              onTap: navigateToProfilePage,
              backgroundColor: model.backgroundColor,
            ),

            new SideMenuItem(
              text: 'Mijn Afspraken',
              iconUrl: model.calenderIcon,
              onTap: navigateToSessionsPage,
              backgroundColor: model.backgroundColor,
            ),

            new SideMenuItem(
              text: 'Mijn Programma\'s',
              iconUrl: model.browserIcon,
              onTap: navigateToProgramsPage,
              backgroundColor: model.backgroundColor,
            ),

            new SideMenuItem(
              text: 'Mijn E-Health',
              iconUrl: model.lineGraphIcon,
              onTap: navigateToEhealthPage,
              backgroundColor: model.backgroundColor,
            ),

            new SideMenuItem(
              text: 'Handleiding',
              iconUrl: model.manualIcon,
              onTap: takeTour,
              backgroundColor: model.backgroundColor,
            ),

            new SideMenuItem(
              text: 'Uitloggen',
              iconUrl: model.logoutIcon,
              onTap: () => logout(),
              backgroundColor: model.backgroundColor,
            ),

            // TODO: remove this button when finished with app
            new FlatButton(
                onPressed: () {
                  if (LocalizationService.tenant == Tenant.PHI) {
                    LocalizationService.tenant = Tenant.RUG;
                  } else if (LocalizationService.tenant == Tenant.RUG) {
                    LocalizationService.tenant = Tenant.PHI;
                  }

                  Navigator.pushReplacement(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new LoginPage()));
                },
                child: new Text('Change Skin')),
          ],
        ),
      ),
    );
  }
}
