import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/pages/create_conversation_page/create_conversation_page.dart';
import 'package:myclient_app/pages/session_detail_page/session_detail_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';

class SessionDetailPage extends StatefulWidget {
  final Session dto;
  final List<Recipient> availableRecipients;

  SessionDetailPage(
      {Key key, @required this.dto, @required this.availableRecipients})
      : super(key: key);

  @override
  _SessionDetailPage createState() =>
      new _SessionDetailPage(this.dto, this.availableRecipients);
}

class _SessionDetailPage extends State<SessionDetailPage> {
  SessionDetailPageViewModel model;

  _SessionDetailPage(Session dto, List<Recipient> availableRecipients) {
    model = new SessionDetailPageViewModel(
        dto: dto, availableRecipients: availableRecipients);
  }

  @override
  void initState() {
    setState(() {
      model.loadLocalization();
    });
    super.initState();
  }

  void sendEmailToTreatmentOfficer(String treatmentOfficerId) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => new CreateConversationPage(
                availableRecipients: model.availableRecipients,
                selectedRecipientId: treatmentOfficerId)));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: new Key('session_detail_page'),
        bottomNavigationBar: new FlatButton(
          onPressed: () =>
              sendEmailToTreatmentOfficer(model.dto.treatmentOfficerId),
          color: ColorHelper.fromHex(model.primaryColor),
          child: new Container(
            color: Colors.transparent,
            height: 50.0,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Image.asset(
                    model.envelopeIcon,
                    width: MediaQuery.of(context).size.width * 0.13,
                    height: MediaQuery.of(context).size.width * 0.13,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: new Text(
                      model.emailPractitioner,
                      style: new TextStyle(
                          fontSize: 18.0,
                          color: Colors.white,
                          fontWeight: FontWeight.w300),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        appBar: new AppBar(
          backgroundColor: ColorHelper.fromHex(model.primaryColor),
          title: new Text(model.title),
          centerTitle: true,
        ),
        body: new Container(
          // Add box decoration
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
              // Where the linear gradient begins and ends
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [0.0, 1.0],
              colors: [
                new Color.fromARGB(255, 249, 248, 248),
                new Color.fromARGB(255, 226, 216, 226),
              ],
            ),
          ),

          child: new Column(
            children: <Widget>[
              // images
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    margin: EdgeInsets.only(
                        left: 15.0, bottom: 0.0, top: 40.0, right: 15.0),
                    child: new CircleAvatar(
                      radius: 90.0,
                      backgroundColor: new Color.fromARGB(255, 227, 227, 227),
                      backgroundImage: new AdvancedNetworkImage(
                        UserHelper.avatarUrl,
                        useDiskCache: true,
                      ),
                    ),
                    width: 90.0,
                    height: 90.0,
                    padding: EdgeInsets.all(1.0), // border width
                    decoration: new BoxDecoration(
                      color: new Color.fromARGB(
                          255, 227, 227, 227), // border color
                      shape: BoxShape.circle,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 70.0),
                    child: new Image.asset(
                      model.linkIcon,
                      fit: BoxFit.fitHeight,
                      width: 40.0,
                      height: 40.0,
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(
                            left: 15.0, bottom: 0.0, top: 40.0, right: 15.0),
                        child: new CircleAvatar(
                          radius: 90.0,
                          backgroundColor:
                              new Color.fromARGB(255, 227, 227, 227),
                          backgroundImage: new AdvancedNetworkImage(
                            model.dto.treatmentOfficerAvatarUrl,
                            useDiskCache: true,
                          ),
                        ),
                        width: 90.0,
                        height: 90.0,
                        padding: EdgeInsets.all(1.0), // border width
                        decoration: new BoxDecoration(
                          color: new Color.fromARGB(
                              255, 227, 227, 227), // border color
                          shape: BoxShape.circle,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          FormatHelper.formatName(
                              model.dto.treatmentOfficerName),
                          textAlign: TextAlign.left,
                          style: new TextStyle(
                              color: new Color.fromARGB(255, 97, 196, 248)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),

              new Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: new SizedBox(
                    width: double.infinity,
                    height: 1.0,
                    // height: double.infinity,
                    child: new Container(
                        color: new Color.fromARGB(255, 227, 227, 227))),
              ),

              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                    child: new Row(
                      children: <Widget>[
                        new Container(
                          height: 30.0,
                          child: new Center(
                            child: new Padding(
                              padding:
                                  const EdgeInsets.only(left: 20.0, top: 0.0),
                              child: new Container(
                                width: 9.0,
                                height: 9.0,
                                decoration: new BoxDecoration(
                                  color: ColorHelper.expertiseCodeToColor(
                                      model.dto.expertiseCode),
                                  shape: BoxShape.circle,
                                  border: new Border.all(
                                    color: ColorHelper.expertiseCodeToColor(
                                        model.dto.expertiseCode),
                                    width: 0.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        new Center(
                          child: new Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 20.0, top: 0.0),
                              child: new Text(
                                FormatHelper.formatDate(
                                    model.dto.appointmentStartTime),
                                style: new TextStyle(
                                    fontSize: 14.0,
                                    fontWeight: FontWeight.w700,
                                    color: new Color.fromARGB(255, 85, 85, 85)),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    child: new Center(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, top: 0.0, right: 30.0),
                        child: new Text(
                          FormatHelper.formatTime(
                              model.dto.appointmentStartTime),
                          style: new TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.w700,
                              color: new Color.fromARGB(255, 85, 85, 85)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),

              // separator
              new Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: new SizedBox(
                    width: double.infinity,
                    height: 1.0,
                    // height: double.infinity,
                    child: new Container(
                        color: new Color.fromARGB(255, 227, 227, 227))),
              ),

              new Expanded(
                child: new SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: new Text(
                      model.dto.description,
                      style: new TextStyle(
                          color: new Color.fromARGB(255, 98, 98, 98)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
