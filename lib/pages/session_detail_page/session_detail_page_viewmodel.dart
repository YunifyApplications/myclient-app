import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class SessionDetailPageViewModel {
  String primaryColor;
  final Session dto;
  String envelopeIcon;
  String linkIcon;
  String emailPractitioner;
  String title;
  final List<Recipient> availableRecipients;

  SessionDetailPageViewModel({this.dto, this.availableRecipients});

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');
    envelopeIcon = 'assets/icons/envelope_64px_White.png';
    linkIcon = 'assets/icons/link_64px.png';
    emailPractitioner = LocalizationService.get('EmailPractitioner');
    title = 'Afspraak detail';
  }
}
