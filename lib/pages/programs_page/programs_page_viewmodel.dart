import 'package:myclient_app/dtos/program_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class ProgramsPageViewModel {
  String primaryColor;
  final ProgramCollectionDTO dto;
  String title;
  String programRetrievalFailed;

  final String expertiseCodeAll = 'Alle';

  List<String> expertises = new List();

  List<Program> selectedPrograms = new List();

  ProgramsPageViewModel({this.dto}) {
    getExpertises();

    selectedPrograms = this.dto.programs;
  }

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');
    title = 'Mijn Programma\'s';
    programRetrievalFailed = LocalizationService.get('ProgramRetrievalFailed');
  }

  void getExpertises() {
    expertises.add(expertiseCodeAll);

    for (var program in dto.programs) {
      if (!expertises.contains(program.programLib.expertise.name)) {
        expertises.add(program.programLib.expertise.name);
      }
    }
  }
}
