import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:flutter_pdf_viewer/flutter_pdf_viewer.dart';
import 'package:myclient_app/dtos/program_dto.dart';
import 'package:myclient_app/events/dashboard_module_item_count_changed_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/programs_page/programs_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/dashboard_helper.dart';
import 'package:myclient_app/utils/download_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';
import 'package:progress_hud/progress_hud.dart';

class ProgramsPage extends StatefulWidget {
  final ProgramCollectionDTO dto;
  ProgramsPage({Key key, @required this.dto}) : super(key: key);

  @override
  _ProgramsPage createState() => new _ProgramsPage(dto: this.dto);
}

class _ProgramsPage extends State<ProgramsPage> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ProgramsPageViewModel model;
  TabController tc;
  TabController tcTop;

  _ProgramsPage({@required ProgramCollectionDTO dto}) {
    model = new ProgramsPageViewModel(dto: dto);
  }

  void moduleNumberSelected() {
    setState(() {});
  }

  void expertiseSelected() {
    setState(() {
      tc.index = 0;
    });
  }

  @override
  void dispose() {
    tc.dispose();
    tcTop.dispose();
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    tc = new TabController(length: 8, vsync: this);
    tcTop = new TabController(length: 8, vsync: this);

    tc.addListener(moduleNumberSelected);
    tcTop.addListener(expertiseSelected);
    super.initState();
  }

  void openProgramFile(Program program) async {
    try {
      if (program.programLib.fileData != null &&
          program.programLib.fileData.extension == '.pdf') {
        // Display PDF file
        await displayPDF(program.programLib.fileData.retrieveUrl);

        // Mark program as read
        await httpService.markProgramAsRead(program.id);
      } else {
        // Display HTML view
        HtmlView v = new HtmlView(data: program.programLib.htmlContent.code);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => new Scaffold(
                  appBar: new AppBar(
                    backgroundColor: ColorHelper.fromHex(model.primaryColor),
                    title: new Text(program.programLib.htmlContent.title),
                  ),
                  body: new SingleChildScrollView(
                    child: v,
                  ),
                ),
          ),
        );

        // Mark program as read
        await httpService.markProgramAsRead(program.id);
      }

      if (!program.done) {
        setState(() {
          program.done = true;

          // update dashboard item count
          if (DashboardHelper.dashboardInfo.programCount > 0) {
            DashboardHelper.dashboardInfo.programCount--;
          }

          eventBus.fire(new DashboardModuleItemCountChangedEvent());
        });
      }
    } catch (ex) {
      // marking message as read failed
    }
  }

  Future<void> displayPDF(String retrieveUrl) async {
    try {
      // show loading animation while downloading file
      var progress = new ProgressHUD(
        backgroundColor: Colors.black12,
        color: Colors.white,
        containerColor: ColorHelper.fromHex(model.primaryColor),
        borderRadius: 5.0,
      );
      showDialog(context: context, child: progress);

      var result =
          await DownloadHelper.downloadAsFile(retrieveUrl, cache: true);

      // pop loading animation
      Navigator.pop(context);

      await FlutterPdfViewer.loadFilePath('file://' + result);
    } catch (ex) {
      // pop loading animation
      Navigator.pop(context);
      ErrorMessage.show(_scaffoldKey, model.programRetrievalFailed);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 5,
      child: new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          backgroundColor: ColorHelper.fromHex(model.primaryColor),
          title: new Text(model.title),
          centerTitle: true,
          bottom: new TabBar(
            isScrollable: true,
            controller: tcTop,
            tabs: getTabs(),
          ),
        ),
        body: new TabBarView(children: [
          getContentTabs(),
        ]),
      ),
    );
  }

  List<Stack> createPanels(String expertiseCode) {
    var content = expertiseCode == model.expertiseCodeAll
        ? model.selectedPrograms
        : model.selectedPrograms
            .where((e) => e.programLib.expertise.name == expertiseCode);

    // if not all modules are selected
    if (tc.index != 0) {
      var moduleNr = getModuleNumbers(expertiseCode)[tc.index];
      content = content
          .where((e) => e.programLib.moduleNumber == int.parse(moduleNr));
    }

    return content
        .map((program) => new Stack(
              children: <Widget>[
                new Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  height: 160.0,
                  width: double.infinity,
                  child: new Stack(
                    children: <Widget>[
                      new Image(
                        image: new AdvancedNetworkImage(
                          program.programLib.imageData.retrieveUrl,
                          useDiskCache: true,
                        ),
                        width: double.infinity,
                        height: 160.0,
                        fit: BoxFit.fitWidth,
                      ),
                      new Positioned(
                        bottom: 0.0,
                        child: new Container(
                          height: 70.0,
                          width: MediaQuery.of(context).size.width,
                          color: new Color.fromARGB(229, 58, 57, 57),
                          padding: EdgeInsets.all(10.0),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 3.0),
                                        child: new Text(
                                          FormatHelper
                                              .getVersionFromProgramName(
                                                  program.programLib.name),
                                          style: new TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w300,
                                            fontSize: 13.0,
                                            decoration:
                                                TextDecoration.underline,
                                            decorationColor: ColorHelper
                                                .expertiseCodeToColor(program
                                                    .programLib.expertise.code),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Padding(
                                        padding:
                                            const EdgeInsets.only(left: 12.0),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              85,
                                          child: new Text(
                                            getTitleFromName(
                                                program.programLib.name),
                                            style: new TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 17.0,
                                            ),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 12.0, top: 10.0),
                                        child: Row(
                                          children: <Widget>[
                                            new Container(
                                              width: 50.0,
                                              height: 17.0,
                                              decoration: new BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5.0),
                                                color: new Color.fromARGB(
                                                    255, 170, 170, 170),
                                              ),
                                              child: Center(
                                                child: new Text(
                                                  FormatHelper
                                                      .formatProgramLibFileExtension(
                                                          program.programLib),
                                                  textAlign: TextAlign.center,
                                                  style: new TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 11.0),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 3.0),
                                              child: new Container(
                                                width: 50.0,
                                                height: 17.0,
                                                decoration: new BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          5.0),
                                                  color: ColorHelper
                                                      .expertiseCodeToColor(
                                                          program.programLib
                                                              .expertise.code),
                                                ),
                                                child: Center(
                                                  child: new Text(
                                                    FormatHelper
                                                        .formatModuleNumber(
                                                      program.programLib
                                                          .moduleNumber,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                    style: new TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 11.0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(11.0),
                                    child: new Container(
                                      width: 0.0,
                                      height: 0.0,
                                      child: new Checkbox(
                                          value: program.done, onChanged: null),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                new Container(
                  height: 160.0,
                  width: double.infinity,
                  child: new FlatButton(
                    onPressed: () => openProgramFile(program),
                    child: new Text(''),
                  ),
                ),
              ],
            ))
        .toList();
  }

  List<Tab> getTabs() {
    return model.expertises
        .map((expertiseCode) => new Tab(
              text: expertiseCode,
            ))
        .toList();
  }

  Container getContentTabs() {
    var expertise = model.expertises[tcTop.index];

    return new Container(
      color: new Color.fromARGB(255, 172, 168, 172),
      child: new ListView(
        children: [
          new Container(
            height: 40.0,
            color: new Color.fromARGB(255, 58, 57, 57),
            child: new TabBar(
              controller: tc,
              tabs: getModuleNumbers(expertise)
                  .map((moduleNr) => new Text(moduleNr))
                  .toList(),
            ),
          ),
          new Container(
            height: 10.0,
            width: double.infinity,
            color: new Color.fromARGB(255, 58, 57, 57),
          ),
          new Column(
            children: createPanels(expertise),
          ),
        ],
      ),
    );
  }

  String getTitleFromName(String title) {
    var split = title.trim().split(' ');
    if (split.length > 1) {
      return title.substring(split[0].length + 1);
    } else {
      return title;
    }
  }

  List<String> getModuleNumbers(String expertiseCode) {
    var content = expertiseCode == model.expertiseCodeAll
        ? model.selectedPrograms
        : model.selectedPrograms
            .where((e) => e.programLib.expertise.name == expertiseCode);

    List<String> modules = new List();
    modules.add(model.expertiseCodeAll);
    for (var c in content) {
      if (c.programLib.moduleNumber < 9) {
        if (!modules.contains("0" + c.programLib.moduleNumber.toString())) {
          modules.add("0" + c.programLib.moduleNumber.toString());
        }
      } else {
        if (!modules.contains(c.programLib.moduleNumber.toString())) {
          modules.add(c.programLib.moduleNumber.toString());
        }
      }
    }

    return modules;
  }
}
