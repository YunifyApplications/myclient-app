import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:myclient_app/dtos/dashboard_info_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/events/dashboard_module_item_count_changed_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/contact_page/contact_page.dart';
import 'package:myclient_app/pages/conversations_page/conversations_page.dart';
import 'package:myclient_app/pages/dashboard_page/dashboard_page_viewmodel.dart';
import 'package:myclient_app/pages/ehealth_page/ehealth_page.dart';
import 'package:myclient_app/pages/exercise_page/exercise_page.dart';
import 'package:myclient_app/pages/login_page/login_page.dart';
import 'package:myclient_app/pages/profile_page/profile_page.dart';
import 'package:myclient_app/pages/programs_page/programs_page.dart';
import 'package:myclient_app/pages/results_page/results_page.dart';
import 'package:myclient_app/pages/sessions_page/sessions_page.dart';
import 'package:myclient_app/pages/sidemenu_page/sidemenu_page.dart';
import 'package:myclient_app/pages/team_page/team_page.dart';
import 'package:myclient_app/services/file_service.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/dashboard_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/dashboard_info_bar.dart';
import 'package:myclient_app/widgets/dashboard_item.dart';
import 'package:myclient_app/widgets/error_message.dart';
import 'package:myclient_app/widgets/tour.dart';

class DashboardPage extends StatefulWidget {
  final DashboardInfoDTO dto;

  DashboardPage({Key key, @required this.dto}) : super(key: key);

  @override
  _DashboardPage createState() => new _DashboardPage(this.dto);
}

class _DashboardPage extends State<DashboardPage>
    with TickerProviderStateMixin {
  DashboardPageViewModel model = new DashboardPageViewModel();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  StreamSubscription itemUpdatedEvent;

  _DashboardPage(DashboardInfoDTO dto) {
    DashboardHelper.dashboardInfo = dto;
    model.dashboardInfo = DashboardHelper.dashboardInfo;
  }

  @override
  void initState() {
    model.loadLocalization();

    itemUpdatedEvent =
        eventBus.on<DashboardModuleItemCountChangedEvent>().listen((e) {
      setState(() {});
    });

    imageWithoutDraw().then((img) => setState(() {
          tapImage = img;
        }));

    model.tapImageWiggleAnimationController = AnimationController(
      duration: const Duration(milliseconds: 600),
      vsync: this,
    )
      ..addListener(
        () => setState(
              () {
                if (this.mounted) {
                  double maxDiff = 5.0;
                  double speed = 0.5;
                  if (wiggleUp) {
                    tapImageOffsetY -= speed;
                    if (tapImageOffsetY < -maxDiff) wiggleUp = false;
                  } else {
                    tapImageOffsetY += speed;
                    if (tapImageOffsetY > maxDiff) wiggleUp = true;
                  }
                }
              },
            ),
      )
      ..repeat().orCancel;

    super.initState();
  }

  bool wiggleUp = true;
  @override
  void dispose() {
    itemUpdatedEvent.cancel();
    model.tapImageWiggleAnimationController?.dispose();
    super.dispose();
  }

  void navigateToSessionsPage() async {
    try {
      var result = await httpService.getSessions();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new SessionsPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToResultsPage() async {
    try {
      var result = await httpService.getResults();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new ResultsPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToEhealthPage() async {
    try {
      var result = await httpService.getEhealthMods();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new EhealthPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToExercisesPage() async {
    try {
      var result = await httpService.getExercises();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new ExercisePage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToProgramsPage() async {
    try {
      var result = await httpService.getPrograms();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new ProgramsPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToProfilePage() async {
    try {
      var result = await httpService.getProfile();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new ProfilePage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToTeamPage() async {
    try {
      var result = await httpService.getTeam();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => new TeamPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToContactPage() async {
    try {
      var result = await httpService.getContactInfo();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new ContactPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  void navigateToConversationsPage() async {
    try {
      var result = await httpService.getMessages();

      if (result.resultStatus == RequestResult.SUCCESS) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => new ConversationsPage(dto: result)));
      } else if (result.resultStatus == RequestResult.UNAUTHORIZED) {
        UserHelper.clear();
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    new LoginPage(showUnauthorizedError: true)));
      } else {
        // show error
        ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
      }
    } catch (ex) {
      // show error
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.tenantName),
        centerTitle: true,
      ),
      drawer: new Drawer(
        child: new SideMenuPage(),
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            color: ColorHelper.fromHex(model.backgroundColor),
            child: Column(
              children: <Widget>[
                // user info bar
                new DashboardInfoBar(
                  avatarUrl: model.dashboardInfo.avatarUrl,
                  profileAvatarBorderWidth: model.profileAvatarBorderWidth,
                  displayName: model.dashboardInfo.displayName,
                  displayNameColor: model.displayNameColor,
                  email: model.dashboardInfo.email,
                  emailColor: model.emailColor,
                  infoColor: model.infoColor,
                  primaryColor: model.primaryColor,
                ),

                // contains everything under info
                new Column(
                  children: <Widget>[
                    // separator
                    new Container(
                      margin: EdgeInsets.only(bottom: model.itemsOffsetTop),
                      color: ColorHelper.fromHex("#E5E5E5"),
                      height: 1.0,
                      width: double.infinity,
                    ),
                    // first row
                    new Row(
                      children: <Widget>[
                        // appointments
                        new DashboardItem(
                            key: new Key('dashboard_sessions_item'),
                            itemCount:
                                DashboardHelper.dashboardInfo.appointmentCount,
                            onTap: navigateToSessionsPage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "Mijn\nAfspraken",
                            itemTextColor: model.itemTextColor,
                            itemBackgroundColor: model.itemBackgroundColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.appointmentsIcon),

                        // messages
                        new DashboardItem(
                            itemCount:
                                DashboardHelper.dashboardInfo.messageCount,
                            onTap: navigateToConversationsPage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "Mijn\nBerichten",
                            itemTextColor: model.itemTextColor,
                            itemBackgroundColor: model.itemBackgroundColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.messagesIcon),

                        // results
                        new DashboardItem(
                            itemCount:
                                DashboardHelper.dashboardInfo.resultCount,
                            onTap: navigateToResultsPage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "Mijn\nResultaten",
                            itemTextColor: model.itemTextColor,
                            itemBackgroundColor: model.itemBackgroundColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.resultsIcon),
                      ],
                    ),

                    // second row
                    new Row(
                      children: <Widget>[
                        // programs
                        new DashboardItem(
                            itemCount:
                                DashboardHelper.dashboardInfo.programCount,
                            onTap: navigateToProgramsPage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "Mijn\nProgramma's",
                            itemTextColor: model.itemTextColor,
                            itemBackgroundColor: model.itemBackgroundColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.programsIcon),

                        // exercises
                        new DashboardItem(
                            itemCount:
                                DashboardHelper.dashboardInfo.exerciseCount,
                            onTap: navigateToExercisesPage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "Mijn\nOefeningen",
                            itemTextColor: model.itemTextColor,
                            itemBackgroundColor: model.itemBackgroundColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.exercisesIcon),

                        // team
                        new DashboardItem(
                            itemCount: 0.0,
                            onTap: navigateToTeamPage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "Mijn\nTeam",
                            itemTextColor: model.itemTextColor,
                            itemBackgroundColor: model.itemBackgroundColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.teamIcon),
                      ],
                    ),

                    // third row
                    new Row(
                      children: <Widget>[
                        // e-health
                        new DashboardItem(
                            itemCount:
                                DashboardHelper.dashboardInfo.ehealthCount,
                            onTap: navigateToEhealthPage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "Mijn\nE-Health",
                            itemTextColor: model.itemTextColor,
                            itemBackgroundColor: model.itemBackgroundColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.ehealthIcon),

                        // profile
                        new DashboardItem(
                            itemCount: 0.0,
                            onTap: navigateToProfilePage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "Mijn\nProfiel",
                            itemTextColor: "#FFFFFF",
                            itemBackgroundColor: model.primaryColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.profileIcon),

                        // contact
                        new DashboardItem(
                            itemCount: 0.0,
                            onTap: navigateToContactPage,
                            itemNotificationColor: model.itemNotificationColor,
                            text: "\nContact",
                            itemTextColor: model.itemTextColor,
                            itemBackgroundColor: model.itemBackgroundColor,
                            itemRadius: model.itemRadius,
                            iconUrl: model.contactIcon),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),

          /// tour
          new Visibility(
            visible: DashboardHelper.isFirstTime,
            child: new Container(
              child: new Builder(
                builder: (context) {
                  return new GestureDetector(
                    onPanDown: (details) {
                      RenderBox object = context.findRenderObject();
                      var _p = object.globalToLocal(details.globalPosition);
                      onTourClick(_p);
                    },
                    child: new CustomPaint(
                      painter: new TourPainter(
                        tapImage: tapImage,
                        tourProgress: tourProgress,
                        tapImageOffsetY: tapImageOffsetY,
                        itemRadius: model.itemRadius,
                        itemOffsetTop: model.itemsOffsetTop,
                        size: MediaQuery.of(context).size,
                        tourIntro: model.tourIntro,
                        tourSessions: model.tourSessions,
                        tourContact: model.tourContact,
                        tourEhealth: model.tourEhealth,
                        tourExercises: model.tourExercises,
                        tourPrograms: model.tourPrograms,
                        tourProfile: model.tourProfile,
                        tourMessages: model.tourMessages,
                        tourTeam: model.tourTeam,
                        tourResults: model.tourResults,
                        tourMenu: model.tourMenu,
                        tapToComplete: model.tapToComplete,
                        tapToContinue: model.tapToContinue,
                      ),
                      willChange: true,
                      size: MediaQuery.of(context).size,
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  ui.Image tapImage;
  int tourProgress = 0;
  double tapImageOffsetY = 0.0;
  void onTourClick(position) {
    setState(() {
      tourProgress += 1;
      if (tourProgress >= TourPainter.maxTourProgress) {
        DashboardHelper.isFirstTime = false;
        tourProgress = 0;
        FileService.writeCacheFile('first_time.txt', '1');
      }
    });
  }

  Future<ui.Image> imageWithoutDraw() async {
    final ByteData data = await getAssetImage();
    var image = await loadImage(data);
    return image;
  }

  Future<ByteData> getAssetImage() async {
    var assetImage = ExactAssetImage('assets/icons/tap.png');
    var key = await assetImage.obtainKey(ImageConfiguration());
    final ByteData data = await key.bundle.load(key.name);
    return data;
  }

  Future<ui.Image> loadImage(ByteData data) async {
    var codec = await ui.instantiateImageCodec(data.buffer.asUint8List());
    var frameInfo = await codec.getNextFrame();
    return frameInfo.image;
  }
}
