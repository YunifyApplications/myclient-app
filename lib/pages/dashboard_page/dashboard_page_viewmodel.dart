import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/dashboard_info_dto.dart';
import 'package:myclient_app/services/file_service.dart';
import 'package:myclient_app/services/localization_service.dart';
import 'package:myclient_app/utils/dashboard_helper.dart';

class DashboardPageViewModel {
  String tenantName;
  String primaryColor;
  double profileAvatarBorderWidth = 0.0;
  String infoColor;
  String backgroundColor;
  String itemBackgroundColor;
  double itemRadius;
  String displayNameColor;
  String emailColor;
  String itemTextColor;
  String itemNotificationColor;
  String dataRetrievalFailed;

  String appointmentsIcon;
  String messagesIcon;
  String resultsIcon;
  String programsIcon;
  String exercisesIcon;
  String teamIcon;
  String ehealthIcon;
  String profileIcon;
  String contactIcon;

  String tourIntro;
  String tourSessions;
  String tourMessages;
  String tourResults;
  String tourPrograms;
  String tourExercises;
  String tourTeam;
  String tourEhealth;
  String tourProfile;
  String tourContact;
  String tourMenu;
  String tapToContinue;
  String tapToComplete;

  double itemsOffsetTop;

  DashboardInfoDTO dashboardInfo;

  // wiggle animations
  AnimationController tapImageWiggleAnimationController;

  void loadLocalization() async {
    switch (LocalizationService.tenant) {
      case Tenant.PHI:
        {
          profileAvatarBorderWidth = 1.0;
          infoColor = "#F3F3F3";
          backgroundColor = "#E5E5E5";
          itemBackgroundColor = "#F9F8F8";
          itemRadius = 80.0;
          emailColor = "#7E7E7E";
          displayNameColor = "#4D4D4D";
          itemTextColor = "#5D5D5D";
          itemNotificationColor = LocalizationService.get("PrimaryColor");

          appointmentsIcon = "calendar_64px_PHI.png";
          messagesIcon = "envelope_64px_PHI.png";
          resultsIcon = "browser_64px_PHI.png";
          programsIcon = "linegraph_64px_PHI.png";
          exercisesIcon = "pencil_64px_PHI.png";
          teamIcon = "profile-male_64px_PHI.png";
          ehealthIcon = "heart_64px_PHI.png";
          profileIcon = "profile-male-circle_64px_White.png";
          contactIcon = "chat_64px_PHI.png";

          itemsOffsetTop = 20.0;
        }
        break;
      case Tenant.RUG:
        {
          profileAvatarBorderWidth = 0.0;
          infoColor = "#F7F7F7";
          backgroundColor = "#FCFCFC";
          itemBackgroundColor = "#F7F7F7";
          itemRadius = 0.0;
          emailColor = "#7E7E7E";
          displayNameColor = LocalizationService.get('PrimaryColor');
          itemTextColor = LocalizationService.get('PrimaryColor');
          itemNotificationColor = "#009FE3";

          appointmentsIcon = "calendar_64px_RUG.png";
          messagesIcon = "envelope_64px_RUG.png";
          resultsIcon = "browser_64px_RUG.png";
          programsIcon = "linegraph_64px_RUG.png";
          exercisesIcon = "pencil_64px_RUG.png";
          teamIcon = "profile-male_64px_RUG.png";
          ehealthIcon = "heart_64px_RUG.png";
          profileIcon = "profile-male-circle_64px_White.png";
          contactIcon = "chat_64px_RUG.png";

          itemsOffsetTop = 0.0;
        }
        break;
    }

    tourIntro = LocalizationService.get('TourIntro');
    tourSessions = LocalizationService.get('TourSessions');
    tourMessages = LocalizationService.get('TourMessages');
    tourResults = LocalizationService.get('TourResults');
    tourPrograms = LocalizationService.get('TourPrograms');
    tourExercises = LocalizationService.get('TourExercises');
    tourTeam = LocalizationService.get('TourTeam');
    tourEhealth = LocalizationService.get('TourEhealth');
    tourProfile = LocalizationService.get('TourProfile');
    tourContact = LocalizationService.get('TourContact');
    tourMenu = LocalizationService.get('TourMenu');
    tapToContinue = LocalizationService.get('TapToContinue');
    tapToComplete = LocalizationService.get('TapToComplete');

    tenantName = LocalizationService.get('BrandName');
    primaryColor = LocalizationService.get('PrimaryColor');
    dataRetrievalFailed = LocalizationService.get('DataRetrievalFailed');

    DashboardHelper.isFirstTime =
        !await FileService.cacheFileExists('first_time.txt');
  }
}
