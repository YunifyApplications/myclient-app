import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/profile_dto.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/profile_page/profile_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';

class ProfilePage extends StatefulWidget {
  final ProfileDTO dto;
  ProfilePage({Key key, @required this.dto}) : super(key: key);

  @override
  _ProfilePage createState() => new _ProfilePage(dto: this.dto);
}

class _ProfilePage extends State<ProfilePage> {
  ProfilePageViewModel model;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _ProfilePage({ProfileDTO dto}) {
    model = new ProfilePageViewModel(dto: dto);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  void saveData() async {
    try {
      await httpService.updateProfile(model.dto);

      // go back to dashboard
      Navigator.pop(context);
    } catch (ex) {
      // pop loading animation
      ErrorMessage.show(_scaffoldKey, model.dataSaveFailed);
    }
  }

  void receiveWeekOverviewPlannedAppointmentsChanged(bool newVal) {
    setState(() {
      model.dto.receiveWeekOverviewPlannedAppointments =
          !model.dto.receiveWeekOverviewPlannedAppointments;
    });
  }

  void receiveEmail24HoursBeforeAppointmentChanged(bool newVal) {
    setState(() {
      model.dto.receiveEmail24HoursBeforeAppointment =
          !model.dto.receiveEmail24HoursBeforeAppointment;
    });
  }

  void receiveSms24HoursBeforeAppointmentChanged(bool newVal) {
    setState(() {
      model.dto.receiveSms24HoursBeforeAppointment =
          !model.dto.receiveSms24HoursBeforeAppointment;
    });
  }

  void receiveGeneralEmailNotificationsChanged(bool newVal) {
    setState(() {
      model.dto.receiveGeneralEmailNotifications =
          !model.dto.receiveGeneralEmailNotifications;
    });
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validateMobile(String value) {
    if (value.length < 6 || value.length > 15)
      return 'invalid';
    else
      return null;
  }

  void changePhoneNumberDialog() async {
    await showDialog<String>(
      context: context,
      child: new AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextFormField(
                autofocus: true,
                controller: model.newPhoneNumberTextController,
                keyboardType: TextInputType.phone,
                decoration: new InputDecoration(
                    labelText: model.newPhoneNumber, hintText: ''),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child: Text(model.cancel),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(
              child: Text(model.done),
              onPressed: () {
                Navigator.pop(context);
                setState(() {
                  if (validateMobile(model.newPhoneNumberTextController.text) ==
                      null) {
                    model.dto.phoneNumber =
                        model.newPhoneNumberTextController.text;
                  } else {
                    // show format error
                    ErrorMessage.show(
                        _scaffoldKey, model.phoneNumberIncorrectFormat);
                  }
                });
              })
        ],
      ),
    );
  }

  void changeEmailDialog() async {
    await showDialog<String>(
      context: context,
      child: new AlertDialog(
        contentPadding: const EdgeInsets.all(16.0),
        content: new Row(
          children: <Widget>[
            new Expanded(
              child: new TextFormField(
                autofocus: true,
                controller: model.newEmailTextController,
                keyboardType: TextInputType.emailAddress,
                decoration: new InputDecoration(
                    labelText: model.newEmailAddress, hintText: ''),
              ),
            )
          ],
        ),
        actions: <Widget>[
          new FlatButton(
              child: Text(model.cancel),
              onPressed: () {
                Navigator.pop(context);
              }),
          new FlatButton(
              child: Text(model.done),
              onPressed: () {
                Navigator.pop(context);
                setState(() {
                  if (validateEmail(model.newEmailTextController.text) ==
                      null) {
                    model.dto.email = model.newEmailTextController.text;
                  } else {
                    // show format error
                    ErrorMessage.show(
                        _scaffoldKey, model.emailAddressIncorrectFormat);
                  }
                });
              })
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new Container(
        child: new ListView(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                color: new Color.fromARGB(255, 245, 245, 245),
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                  color: new Color.fromARGB(255, 240, 240, 240),
                  width: 1.0,
                ),
              ),
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              child: new Column(
                children: [
                  new Container(
                    margin: EdgeInsets.only(bottom: 15.0),
                    child: new Center(
                      child: new Text(
                        model.profilePageHeading,
                        style: new TextStyle(
                          color: ColorHelper.fromHex(model.primaryColor),
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                  ),
                  buildPersonalInfoList(),
                  new Padding(
                    padding: const EdgeInsets.only(
                        top: 0.0, left: 20.0, right: 20.0),
                    child: new SizedBox(
                        width: double.infinity,
                        height: 1.0,
                        // height: double.infinity,
                        child: new Container(
                            color: new Color.fromARGB(255, 227, 227, 227))),
                  ),
                  new Row(
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(
                            bottom: 10.0, left: 10.0, right: 10.0, top: 15.0),
                        padding: const EdgeInsets.only(),
                        height: 30.0,
                        decoration: new BoxDecoration(
                          color: ColorHelper.fromHex(model.primaryColor),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: new FlatButton(
                          onPressed: () => changeEmailDialog(),
                          textColor: Colors.white,
                          color: Colors.transparent,
                          padding: const EdgeInsets.all(0.0),
                          child: new Text(
                            model.edit,
                            style: new TextStyle(fontSize: 12.0),
                          ),
                        ),
                      ),
                      new Expanded(
                        child: new Text(model.dto.email),
                      ),
                    ],
                  ),
                  new Row(
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(
                            bottom: 10.0, left: 10.0, right: 10.0, top: 15.0),
                        padding: const EdgeInsets.only(),
                        height: 30.0,
                        decoration: new BoxDecoration(
                          color: ColorHelper.fromHex(model.primaryColor),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: new FlatButton(
                          onPressed: () => changePhoneNumberDialog(),
                          textColor: Colors.white,
                          color: Colors.transparent,
                          padding: const EdgeInsets.all(0.0),
                          child: new Text(
                            model.edit,
                            style: new TextStyle(fontSize: 12.0),
                          ),
                        ),
                      ),
                      new Expanded(
                        child: new Text(model.dto.phoneNumber == null
                            ? ''
                            : model.dto.phoneNumber),
                      ),
                    ],
                  ),
                  new Row(
                    children: <Widget>[
                      new Checkbox(
                          value:
                              model.dto.receiveWeekOverviewPlannedAppointments,
                          onChanged:
                              receiveWeekOverviewPlannedAppointmentsChanged),
                      new Container(
                        width: MediaQuery.of(context).size.width - 80,
                        child: new Text(model.profileBoxWeeklyOverview),
                      ),
                    ],
                  ),
                  new Row(
                    children: <Widget>[
                      new Checkbox(
                          value: model.dto.receiveEmail24HoursBeforeAppointment,
                          onChanged:
                              receiveEmail24HoursBeforeAppointmentChanged),
                      new Container(
                        width: MediaQuery.of(context).size.width - 80,
                        child: new Text(model.profileBoxEmailReminder),
                      ),
                    ],
                  ),
                  new Row(
                    children: <Widget>[
                      new Checkbox(
                          value: model.dto.receiveSms24HoursBeforeAppointment,
                          onChanged: receiveSms24HoursBeforeAppointmentChanged),
                      new Container(
                        width: MediaQuery.of(context).size.width - 80,
                        child: new Text(model.profileBoxSmsReminder),
                      ),
                    ],
                  ),
                  new Row(
                    children: <Widget>[
                      new Checkbox(
                          value: model.dto.receiveGeneralEmailNotifications,
                          onChanged: receiveGeneralEmailNotificationsChanged),
                      new Container(
                        width: MediaQuery.of(context).size.width - 80,
                        child:
                            new Text(model.profileBoxReceiveEMailNotification),
                      ),
                    ],
                  ),
                  new Container(
                    margin: EdgeInsets.only(
                        bottom: 10.0, left: 10.0, right: 10.0, top: 15.0),
                    padding: const EdgeInsets.only(),
                    height: 30.0,
                    width: double.infinity,
                    decoration: new BoxDecoration(
                      color: ColorHelper.fromHex(model.primaryColor),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child: new FlatButton(
                      onPressed: () => saveData(),
                      textColor: Colors.white,
                      color: Colors.transparent,
                      padding: const EdgeInsets.all(0.0),
                      child: new Text(
                        model.save,
                        style: new TextStyle(fontSize: 12.0),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container buildPersonalInfoList() {
    return new Container(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                child: new Text(
                  model.username,
                  style: new TextStyle(fontWeight: FontWeight.w600),
                ),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
              new Container(
                child: new Text(
                  model.firstName,
                  style: new TextStyle(fontWeight: FontWeight.w600),
                ),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
              new Container(
                child: new Text(
                  model.lastName,
                  style: new TextStyle(fontWeight: FontWeight.w600),
                ),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
              new Container(
                child: new Text(
                  model.birthDate,
                  style: new TextStyle(fontWeight: FontWeight.w600),
                ),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
              new Container(
                child: new Text(
                  model.gender,
                  style: new TextStyle(fontWeight: FontWeight.w600),
                ),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
            ],
          ),
          new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                child: new Text(model.dto.userName),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
              new Container(
                child: new Text(model.dto.firstName),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
              new Container(
                child: new Text(model.dto.lastName),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
              new Container(
                child: new Text(
                    FormatHelper.formatDateNumerical(model.dto.birthDate)),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
              new Container(
                child: new Text(model.dto.gender),
                margin: EdgeInsets.only(bottom: 10.0),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
