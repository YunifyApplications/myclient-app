import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/profile_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class ProfilePageViewModel {
  String primaryColor;
  String username;
  String firstName;
  String lastName;
  String birthDate;
  String gender;
  String save;
  String edit;
  String profilePageHeading;
  String title;
  String cancel;
  String emailAddressIncorrectFormat;
  String done;
  String phoneNumberIncorrectFormat;
  String newEmailAddress;
  String newPhoneNumber;
  String dataSaveFailed;

  String profileBoxWeeklyOverview;
  String profileBoxEmailReminder;
  String profileBoxSmsReminder;
  String profileBoxReceiveEMailNotification;

  TextEditingController newEmailTextController;
  TextEditingController newPhoneNumberTextController;

  final ProfileDTO dto;

  ProfilePageViewModel({this.dto});

  void loadLocalization() {
    newEmailTextController = new TextEditingController();
    newPhoneNumberTextController = new TextEditingController();

    title = 'Mijn Profiel';

    emailAddressIncorrectFormat =
        LocalizationService.get("EmailAddressIncorrectFormat");
    done = LocalizationService.get("Done");
    phoneNumberIncorrectFormat =
        LocalizationService.get("PhoneNumberIncorrectFormat");
    cancel = LocalizationService.get("Cancel");
    primaryColor = LocalizationService.get("PrimaryColor");
    username = LocalizationService.get("Username");
    firstName = LocalizationService.get("FirstName");
    lastName = LocalizationService.get("LastName");
    birthDate = LocalizationService.get("Birthdate");
    gender = LocalizationService.get("Gender");
    save = LocalizationService.get("Save");
    edit = LocalizationService.get("Edit");
    profilePageHeading = LocalizationService.get("ProfilePageHeading");
    newEmailAddress = LocalizationService.get("NewEmailAddress");
    newPhoneNumber = LocalizationService.get("NewPhoneNumber");
    profileBoxWeeklyOverview =
        LocalizationService.get("ProfileBoxWeeklyOverview");
    profileBoxEmailReminder =
        LocalizationService.get("ProfileBoxEmailReminder");
    profileBoxSmsReminder = LocalizationService.get("ProfileBoxSmsReminder");
    profileBoxReceiveEMailNotification =
        LocalizationService.get("ProfileBoxReceiveEMailNotification");
    dataSaveFailed = LocalizationService.get("DataSaveFailed");
  }
}
