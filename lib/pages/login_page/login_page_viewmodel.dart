import 'dart:async';

import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/login_attempt_dto.dart';
import 'package:myclient_app/dtos/login_result_dto.dart';
import 'package:myclient_app/events/login_result_event.dart';
import 'package:myclient_app/events/wiggle_error_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/services/localization_service.dart';

class LoginPageViewModel {
  // text controllers
  final emailTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  // subscribed events
  StreamSubscription loginResultEvent;
  StreamSubscription wiggleErrorEvent;

  // wiggle animations
  AnimationController errorMessageWiggleAnimationController;

  // properties
  bool errorMessageVisible = false;
  bool isRughuis = LocalizationService.tenant == Tenant.RUG;
  String emailPlaceholder = 'E-mail';
  String passwordPlaceholder = 'Wachtwoord';
  String backgroundGradientStart = '#9C3D99';
  String backgroundGradientEnd = '#732A71';
  String tenantName = 'Mijn Rughuis';
  String errorMessage = '';
  String loginButtonText = 'Aanmelden';
  String errorOccurred = 'Er heeft een fout opgetreden in de applicatie';

  LoginPageViewModel() {
    // debug data
    emailTextController.text = '1@1.com';
    passwordTextController.text = '123';
  }

  void dispose() {
    // text controllers
    emailTextController.dispose();
    passwordTextController.dispose();

    // unsubscribe from events
    loginResultEvent.cancel();
    wiggleErrorEvent.cancel();
  }

  void attemptLogin() {
    if (emailTextController.text == '') {
      setError('Email is verplicht');
      return;
    } else if (passwordTextController.text == '') {
      setError('Wachtwoord is verplicht');
      return;
    }

    try {
      httpService
          .attemptLogin(new LoginAttemptDTO(
              emailTextController.text, passwordTextController.text))
          .then((result) {
        String errorMessage = '';
        switch (result.result) {
          case LoginResult.SUCCESS:
            errorMessage = 'Success';
            break;
          case LoginResult.FAILURE:
            errorMessage = 'Ongeldige email/wachtwoord combinatie';
            break;
          case LoginResult.SERVER_ERROR:
            errorMessage = 'Er is een fout opgetreden in de applicatie';
            break;
        }
        eventBus.fire(new LoginResultEvent(result, errorMessage));
      });
    } catch (ex) {
      eventBus.fire(new LoginResultEvent(
          new LoginResultDTO(LoginResult.FAILURE, ''),
          'Er is een fout opgetreden in de applicatie'));
    }
  }

  void setError(String message) {
    errorMessage = message;
    errorMessageVisible = true;

    // send event to view
    eventBus.fire(new WiggleErrorEvent());
  }
}
