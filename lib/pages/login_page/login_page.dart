import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myclient_app/dtos/login_result_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/events/login_result_event.dart';
import 'package:myclient_app/events/wiggle_error_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/dashboard_page/dashboard_page.dart';
import 'package:myclient_app/pages/login_page/login_page_viewmodel.dart';
import 'package:myclient_app/services/localization_service.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/bordered_button.dart';
import 'package:myclient_app/widgets/bordered_textform.dart';
import 'package:vector_math/vector_math_64.dart' as v;

class LoginPage extends StatefulWidget {
  final bool showUnauthorizedError;
  LoginPage({Key key, this.showUnauthorizedError}) : super(key: key);

  @override
  _LoginPageState createState() =>
      new _LoginPageState(showUnauthorizedError: showUnauthorizedError);
}

class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  LoginPageViewModel model = new LoginPageViewModel();
  final bool showUnauthorizedError;

  _LoginPageState({this.showUnauthorizedError});

  @override
  void dispose() {
    // dispose model
    model.dispose();

    // dispose animations
    model.errorMessageWiggleAnimationController.dispose();

    super.dispose();
  }

  @override
  void initState() {
    // load localization file
    loadLocalization();

    // start listening for events
    startListening();

    // create animations
    createAnimations();

    // only allow portrait mode
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    // show authorization error.
    if (showUnauthorizedError != null && showUnauthorizedError) {
      model.setError('U bent niet ingelogd');
    }

    super.initState();
  }

  void createAnimations() {
    model.errorMessageWiggleAnimationController = AnimationController(
      duration: const Duration(milliseconds: 600),
      vsync: this,
    )..addListener(() => setState(() {}));
  }

  void startListening() {
    // listen for events
    model.wiggleErrorEvent = eventBus.on<WiggleErrorEvent>().listen((result) {
      startWiggleAnimation();
    });

    // redirect to dashboard on success
    model.loginResultEvent = eventBus.on<LoginResultEvent>().listen((result) {
      if (result.result.result == LoginResult.SUCCESS) {
        UserHelper.authToken = result.result.authToken;
        httpService.getDashboard().then((result) {
          if (result.resultStatus == RequestResult.SUCCESS) {
            // set user data
            UserHelper.displayName = result.displayName;
            UserHelper.avatarUrl = result.avatarUrl;
            UserHelper.userId = result.userId;

            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => new DashboardPage(dto: result)));
          } else {
            // retrieving dashboard failed, show error.
            model.setError(model.errorOccurred);
          }
        });
      } else
        // sending login attempt failed or an error occurred on the server side, show error.
        model.setError(result.errorMessage);
    });
  }

  void startWiggleAnimation() {
    try {
      model.errorMessageWiggleAnimationController?.reset();
      model.errorMessageWiggleAnimationController?.forward();
    } catch (ex) {
      // doesn't matter..
    }
  }

  v.Vector3 getTranslation() {
    double progress = model.errorMessageWiggleAnimationController.value;
    double offset = sin(progress * pi * 4);
    return v.Vector3(offset, 0.0, 0.0);
  }

  // load in the localization file for the first time.
  void loadLocalization() async {
    // view is displayed immediately so we cant wait for the localization file
    // to be loaded.
    setState(() {
      switch (LocalizationService.tenant) {
        case Tenant.PHI:
          {
            model.backgroundGradientStart = '#ff671b';
            model.backgroundGradientEnd = '#F5863B';
            model.tenantName = 'MijnPhi';
          }
          break;

        case Tenant.RUG:
          {
            model.backgroundGradientStart = '#9C3D99';
            model.backgroundGradientEnd = '#732A71';
            model.tenantName = 'Mijn Rughuis';
          }
          break;
      }
    });

    await LocalizationService.load();

    // set status bar color
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: ColorHelper.fromHex(model.backgroundGradientStart),
      statusBarIconBrightness: Brightness.light,
    ));

    setState(() {
      model.emailPlaceholder = LocalizationService.get('Email');
      model.passwordPlaceholder = LocalizationService.get('Password');
      model.loginButtonText = LocalizationService.get('LogIn');
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: new Key('login_page'),
      body: new Container(
        // Add box decoration
        decoration: new BoxDecoration(
          gradient: new LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: [0.0, 1.0],
            colors: [
              ColorHelper.fromHex(model.backgroundGradientStart),
              ColorHelper.fromHex(model.backgroundGradientEnd),
            ],
          ),
        ),

        child: new Stack(fit: StackFit.expand, children: <Widget>[
          // background image
          new Visibility(
            visible: model.isRughuis,
            child: new Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.only(top: 50.0, bottom: 50.0),
              child: new Image.asset(
                'assets/images/rug_skeleton.png',
                fit: BoxFit.fitHeight,
              ),
            ),
          ),

          // logo
          new Visibility(
            visible: !model.isRughuis,
            child: new Container(
              width: MediaQuery.of(context).size.height * 0.1,
              height: MediaQuery.of(context).size.height * 0.1,
              alignment: Alignment.topCenter,
              margin: EdgeInsets.only(top: 70.0, bottom: 0.0),
              child: new Image.asset(
                'assets/images/phi_small.png',
                fit: BoxFit.fitHeight,
                width: MediaQuery.of(context).size.height * 0.12,
                height: MediaQuery.of(context).size.height * 0.12,
              ),
            ),
          ),

          // tenant name
          new Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(top: 145.0, bottom: 0.0),
            child: new Text(
              model.tenantName,
              style: new TextStyle(
                  fontSize: 45.0,
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontFamily: "Roboto"),
            ),
          ),

          // container for input elements
          new ClipRect(
            child: new Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.30,
                  bottom: 0.0,
                  left: 25.0,
                  right: 25.0),
              alignment: Alignment.center,
              color: Colors.transparent,
              child: new ListView(
                physics: const NeverScrollableScrollPhysics(),
                children: <Widget>[
                  // error message
                  new Transform(
                    transform: Matrix4.translation(getTranslation()),
                    child: new Visibility(
                      visible: model.errorMessageVisible,
                      child: new Container(
                        alignment: Alignment.center,
                        height: 32.0,
                        decoration: new BoxDecoration(
                          borderRadius:
                              new BorderRadius.all(Radius.circular(5.0)),
                          color: Colors.red,
                        ),
                        margin: const EdgeInsets.only(top: 10.0),
                        child: new Text(
                          model.errorMessage,
                          key: new Key('error_message'),
                          style: new TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),

                  // email text field
                  new Container(
                    margin: EdgeInsets.only(
                        top: (model.errorMessageVisible ? 10.0 : 42.0)),
                    child: new BorderedTextInput(
                      key: new Key('email_field'),
                      controller: model.emailTextController,
                      placeHolder: model.emailPlaceholder,
                      isPassword: false,
                      fullBorder: !model.isRughuis,
                    ),
                  ),

                  // password text field
                  new Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: new BorderedTextInput(
                      key: new Key('password_field'),
                      controller: model.passwordTextController,
                      placeHolder: model.passwordPlaceholder,
                      isPassword: true,
                      fullBorder: !model.isRughuis,
                    ),
                  ),

                  // login button
                  new BorderedButton(
                    key: new Key('button_login'),
                    fill: !model.isRughuis,
                    onPressed: model.attemptLogin,
                  ),
                ],
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
