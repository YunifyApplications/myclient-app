import 'dart:async';

import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class ConversationsPageViewModel {
  MessageCollectionDTO dto;
  String primaryColor;
  String title;
  String messageRetrievalFailed;

  String newConversation;
  String envelopeIcon;

  StreamSubscription messageReadEvent;
  StreamSubscription messageSentEvent;

  ConversationsPageViewModel({@required this.dto});

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');

    title = 'Mijn gesprekken';
    envelopeIcon = 'assets/icons/envelope_64px_White.png';
    newConversation = LocalizationService.get('NewConversation');
    messageRetrievalFailed = LocalizationService.get('MessageRetrievalFailed');
  }

  void dispose() {
    messageReadEvent.cancel();
    messageSentEvent.cancel();
  }
}
