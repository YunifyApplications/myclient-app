import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/events/message_read_event.dart';
import 'package:myclient_app/events/message_sent_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/conversation_detail_page/conversation_detail_page.dart';
import 'package:myclient_app/pages/conversations_page/conversations_page_viewmodel.dart';
import 'package:myclient_app/pages/create_conversation_page/create_conversation_page.dart';
import 'package:myclient_app/pages/login_page/login_page.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/format_helper.dart';
import 'package:myclient_app/utils/user_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';

class ConversationsPage extends StatefulWidget {
  final MessageCollectionDTO dto;

  ConversationsPage({Key key, @required this.dto}) : super(key: key);

  @override
  _ConversationsPage createState() => new _ConversationsPage(this.dto);
}

class _ConversationsPage extends State<ConversationsPage> {
  ConversationsPageViewModel model;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _ConversationsPage(MessageCollectionDTO dto) {
    sortMessages(dto);
    model = new ConversationsPageViewModel(dto: dto);
  }

  @override
  void dispose() {
    model.dispose();
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    startListening();
    super.initState();
  }

  void sortMessages(MessageCollectionDTO dto) {
    // sort messages by newest
    dto.messages.sort((m1, m2) => m2.sendDate.microsecondsSinceEpoch
        .compareTo(m1.sendDate.microsecondsSinceEpoch));
  }

  void startListening() {
    // marks messages read when navigated to.
    model.messageReadEvent = eventBus.on<MessageReadEvent>().listen((result) {
      setState(() {
        for (var message in model.dto.messages) {
          if (message.id == result.messageId) {
            for (var recipient in message.recipients) {
              if (recipient.receiverId == result.recipientId) {
                recipient.isRead = true;
              }
            }
          }
        }
      });
    });

    model.messageSentEvent = eventBus.on<MessageSentEvent>().listen((result) {
      setState(() {
        if (result.message.parentId != null) {
          int index = model.dto.messages
              .indexWhere((e) => e.id == result.message.parentId);
          model.dto.messages.insert(index, result.message);
          model.dto.messages.removeAt(index + 1);
          sortMessages(model.dto);
        } else {
          model.dto.messages.insert(0, result.message);
          sortMessages(model.dto);
        }
      });
    });
  }

  void createNewConversation() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => new CreateConversationPage(
                availableRecipients: model.dto.availableRecipients)));
  }

  void navigateToConversation(Message message) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => new ConversationDetailPage(dto: message)));
  }

  Future<Null> _handleRefresh() async {
    try {
      var latestMessages = await httpService.getMessages();

      setState(() {
        if (latestMessages.resultStatus == RequestResult.SUCCESS) {
          sortMessages(latestMessages);
          model.dto = latestMessages;
        } else if (latestMessages.resultStatus == RequestResult.UNAUTHORIZED) {
          UserHelper.clear();
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      new LoginPage(showUnauthorizedError: true)));
        } else {
          // show error message because retrieving messages failed
          ErrorMessage.show(_scaffoldKey, model.messageRetrievalFailed);
        }
      });
    } catch (ex) {
      // show error message because retrieving messages failed
      ErrorMessage.show(_scaffoldKey, model.messageRetrievalFailed);
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      bottomNavigationBar: new FlatButton(
        onPressed: () => createNewConversation(),
        color: ColorHelper.fromHex(model.primaryColor),
        child: new Container(
          color: Colors.transparent,
          height: 50.0,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Image.asset(
                  model.envelopeIcon,
                  width: MediaQuery.of(context).size.width * 0.13,
                  height: MediaQuery.of(context).size.width * 0.13,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: new Text(
                    model.newConversation,
                    style: new TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w300),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      body: new Container(
        color: Colors.white,
        child: new RefreshIndicator(
          onRefresh: _handleRefresh,
          child: Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: new ListView(
              children: createConversationList(),
            ),
          ),
        ),
      ),
    );
  }

  List<FlatButton> createConversationList() {
    return model.dto.messages
        .map(
          (message) => new FlatButton(
                onPressed: () => navigateToConversation(message),
                padding: EdgeInsets.all(0.0),
                color: Colors.white,
                child: new Container(
                  height: 65.0,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.transparent,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: new Column(
                      children: <Widget>[
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                              margin: EdgeInsets.only(
                                  left: 10.0,
                                  bottom: 0.0,
                                  top: 0.0,
                                  right: 0.0),
                              child: new CircleAvatar(
                                radius: 90.0,
                                backgroundColor:
                                    new Color.fromARGB(255, 227, 227, 227),
                                backgroundImage: new AdvancedNetworkImage(
                                  message.senderAvatarUrl,
                                  useDiskCache: true,
                                ),
                              ),
                              width: 50.0,
                              height: 50.0,
                              padding: EdgeInsets.all(1.0), // border width
                              decoration: new BoxDecoration(
                                color: new Color.fromARGB(
                                    255, 227, 227, 227), // border color
                                shape: BoxShape.circle,
                              ),
                            ),
                            new Expanded(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Padding(
                                    padding: const EdgeInsets.only(
                                        left: 18.0, right: 18.0, top: 3.0),
                                    child: new Text(
                                      message.subject,
                                      maxLines: 1,
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style: new TextStyle(
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  new Padding(
                                    padding: const EdgeInsets.only(
                                        left: 18.0, right: 0.0, top: 5.0),
                                    child: new Text(
                                      message.messageBody,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: new TextStyle(
                                        color:
                                            new Color.fromARGB(255, 80, 80, 80),
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            new Container(
                              height: 45.0,
                              child: new Stack(
                                children: <Widget>[
                                  new Container(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 10.0, top: 3.0),
                                      child: new Text(
                                        FormatHelper.formatTimeShort(
                                            message.sendDate),
                                        textAlign: TextAlign.right,
                                        style: new TextStyle(
                                            color: new Color.fromARGB(
                                                255, 100, 100, 100),
                                            fontWeight: FontWeight.w300,
                                            fontSize: 12.0),
                                      ),
                                    ),
                                  ),
                                  new Positioned(
                                    bottom: 6.0,
                                    right: 9.0,
                                    child: new Icon(
                                      messageIsRead(message)
                                          ? Icons.done_all
                                          : Icons.markunread,
                                      size: 16.0,
                                      color: Colors.black38,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),

                        // separator
                        new Padding(
                          padding: const EdgeInsets.only(top: 6.0, left: 68.0),
                          child: new SizedBox(
                              width: double.infinity,
                              height: 1.0,
                              // height: double.infinity,
                              child: new Container(
                                  color:
                                      new Color.fromARGB(255, 227, 227, 227))),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
        )
        .toList();
  }

  bool messageIsRead(Message message) {
    // if sender of message, mark not read if any recipients hasn't read yet.
    if (message.senderId == UserHelper.userId)
      return message.recipients.every((e) => e.isRead);
    else
      // if not sender, mark read when current user has read.
      return message.recipients
          .any((e) => e.isRead && e.receiverId == UserHelper.userId);
  }
}
