import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:myclient_app/dtos/ehealth_dto.dart';
import 'package:myclient_app/events/dashboard_module_item_count_changed_event.dart';
import 'package:myclient_app/main.dart';
import 'package:myclient_app/pages/ehealth_page/ehealth_page_viewmodel.dart';
import 'package:myclient_app/pages/ehealth_take_page/ehealth_page_take.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:myclient_app/utils/dashboard_helper.dart';
import 'package:myclient_app/widgets/error_message.dart';

class EhealthPage extends StatefulWidget {
  final EhealthModsCollection dto;
  EhealthPage({Key key, @required this.dto}) : super(key: key);

  @override
  _EhealthPage createState() => new _EhealthPage(dto: this.dto);
}

class _EhealthPage extends State<EhealthPage> {
  EhealthPageViewModel model;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _EhealthPage({@required EhealthModsCollection dto}) {
    model = new EhealthPageViewModel(dto: dto);
  }

  @override
  void dispose() {
    try {
      httpService.getDashboard().then((result) {
        DashboardHelper.dashboardInfo = result;
        eventBus.fire(new DashboardModuleItemCountChangedEvent());
      });
    } catch (ex) {
      // do nothing..
    }

    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  void takeEhealthMod(EhealthModsViewModel mod) async {
    // http://localhost:8281/EhealthMod/take/1
// "https://myclient-webui.conveyor.cloud/EhealthMod/take/1"
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => new WebviewScaffold(
              url: mod.url,
              withLocalStorage: true,
              withJavascript: true,
              clearCache: false,
              clearCookies: false,
              withZoom: false,
              scrollBar: false,
              appBar: new AppBar(
                title: new Text("E-Health oefening"),
                backgroundColor: ColorHelper.fromHex(model.primaryColor),
                centerTitle: true,
              ),
            ),
      ),
    );

    return;
    try {
      EhealthModViewModel ehealthMod = await httpService.getEhealthMod(mod.id);

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new EhealthTakePage(
                dto: ehealthMod,
              ),
        ),
      );
    } catch (ex) {
      ErrorMessage.show(_scaffoldKey, model.dataRetrievalFailed);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: createPanels(),
            ),
          )
        ],
      ),
    );
  }

  List<Widget> createPanels() {
    return model.dto.ehealthMods
        .map(
          (ehealthMod) => new Container(
                decoration: new BoxDecoration(
                  color: ColorHelper.getBackgroundColorForWorkflowState(
                      ehealthMod.workflowState),
                  borderRadius: BorderRadius.circular(4.0),
                  border: Border.all(
                    color: new Color.fromARGB(255, 240, 240, 240),
                    width: 1.0,
                  ),
                ),
                width: double.infinity,
                height: 60.0,
                margin: EdgeInsets.only(bottom: 7.0),
                child: new Stack(
                  children: <Widget>[
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            new Container(
                              width: 60.0,
                              height: 60.0,
                              color: Color.fromARGB(255, 91, 197, 242),
                              child: new Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  new Text(
                                    ehealthMod.moduleOrMoment.toUpperCase(),
                                    style: new TextStyle(
                                      fontSize: 13.0,
                                      fontWeight: FontWeight.w400,
                                      letterSpacing: -0.5,
                                      color: Colors.white,
                                    ),
                                  ),
                                  new Text(
                                    ehealthMod.number,
                                    style: new TextStyle(
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: -0.5,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            new Container(
                              padding: EdgeInsets.only(left: 10.0),
                              width: MediaQuery.of(context).size.width - 185.0,
                              child: new Text(
                                ehealthMod.ehealthModLibDescription,
                                style: new TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            )
                          ],
                        ),
                        new Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Container(
                                width: 70.0,
                                child: new Text(
                                  formatWorkFlowState(ehealthMod.workflowState),
                                  style: new TextStyle(fontSize: 11.0),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            new Container(
                              width: 10.0,
                              color: ColorHelper.getColorForWorkflowState(
                                  ehealthMod.workflowState),
                            )
                          ],
                        ),
                      ],
                    ),
                    new Container(
                      width: MediaQuery.of(context).size.width,
                      height: 60.0,
                      child: new FlatButton(
                        onPressed: () => takeEhealthMod(ehealthMod),
                        child: new Text(''),
                      ),
                    ),
                  ],
                ),
              ),
        )
        .toList();
  }

  String formatWorkFlowState(WorkflowState state) {
    if (state == WorkflowState.WaitForClient) {
      return model.ehealth_WorkFlowStateWaitForClient;
    } else if (state == WorkflowState.WaitForUserApproval) {
      return model.ehealth_WorkFlowStateWaitForUserApproval;
    } else if (state == WorkflowState.Completed) {
      return model.ehealth_WorkFlowStateCompleted;
    } else if (state == WorkflowState.Closed) {
      return model.ehealth_WorkFlowStateClosed;
    }
    return '';
  }
}
