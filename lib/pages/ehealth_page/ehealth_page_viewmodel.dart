import 'package:myclient_app/dtos/ehealth_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class EhealthPageViewModel {
  String primaryColor;
  final EhealthModsCollection dto;
  String title;
  String dataRetrievalFailed;

  String ehealth_WorkFlowStateWaitForUserApproval;
  String ehealth_WorkFlowStateWaitForClient;
  String ehealth_WorkFlowStateClosed;
  String ehealth_WorkFlowStateCompleted;

  EhealthPageViewModel({this.dto});

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');
    title = 'Mijn E-Health';

    ehealth_WorkFlowStateWaitForUserApproval =
        LocalizationService.get('Ehealth_WorkFlowStateWaitForUserApproval');
    ehealth_WorkFlowStateWaitForClient =
        LocalizationService.get('Ehealth_WorkFlowStateWaitForClient');
    ehealth_WorkFlowStateClosed =
        LocalizationService.get('Ehealth_WorkFlowStateClosed');
    ehealth_WorkFlowStateCompleted =
        LocalizationService.get('Ehealth_WorkFlowStateCompleted');

    dataRetrievalFailed = LocalizationService.get('DataRetrievalFailed');
  }
}
