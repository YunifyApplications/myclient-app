import 'package:myclient_app/dtos/contact_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class ContactPageViewModel {
  String primaryColor;
  String title;
  final ContactDTO dto;

  String contactInfo;
  String companyPhone;
  String companySiteUrl;
  String infoMail;

  String email;
  String website;
  String address;
  String phone;

  ContactPageViewModel(this.dto);

  void loadLocalization() {
    primaryColor = LocalizationService.get("PrimaryColor");

    title = "Contact";

    contactInfo = LocalizationService.get("ContactInfo");
    companyPhone = LocalizationService.get("CompanyPhone");
    companySiteUrl = LocalizationService.get("CompanySiteUrl");
    infoMail = LocalizationService.get("InfoMail");

    email = LocalizationService.get("Email");
    website = LocalizationService.get("Website");
    address = LocalizationService.get("Address");
    phone = LocalizationService.get("Phone");
  }
}
