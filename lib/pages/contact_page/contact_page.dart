import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/contact_dto.dart';
import 'package:myclient_app/pages/contact_page/contact_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactPage extends StatefulWidget {
  final ContactDTO dto;
  ContactPage({Key key, @required this.dto}) : super(key: key);

  @override
  _ContactPage createState() => new _ContactPage(this.dto);
}

class _ContactPage extends State<ContactPage> {
  ContactPageViewModel model;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  _ContactPage(ContactDTO dto) {
    model = new ContactPageViewModel(dto);
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new Container(
        child: new ListView(
          children: <Widget>[
            new Container(
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
              decoration: new BoxDecoration(
                color: new Color.fromARGB(255, 245, 245, 245),
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                  color: new Color.fromARGB(255, 240, 240, 240),
                  width: 1.0,
                ),
              ),
              child: new Column(
                children: <Widget>[
                  new Center(
                    child: new Text(
                      model.contactInfo,
                      style: new TextStyle(
                        color: ColorHelper.fromHex(model.primaryColor),
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(
                        top: 10.0, left: 20.0, right: 20.0, bottom: 10.0),
                    child: new SizedBox(
                        width: double.infinity,
                        height: 1.0,
                        // height: double.infinity,
                        child: new Container(
                            color: new Color.fromARGB(255, 227, 227, 227))),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            child: new Text(
                              model.phone,
                              style: new TextStyle(fontWeight: FontWeight.w600),
                            ),
                            margin: EdgeInsets.only(bottom: 10.0),
                          ),
                          new Container(
                            child: new Text(
                              model.address,
                              style: new TextStyle(fontWeight: FontWeight.w600),
                            ),
                            margin: EdgeInsets.only(bottom: 10.0),
                          ),
                          new Container(
                            child: new Text(
                              model.website,
                              style: new TextStyle(fontWeight: FontWeight.w600),
                            ),
                            margin: EdgeInsets.only(bottom: 10.0),
                          ),
                          new Container(
                            child: new Text(
                              model.email,
                              style: new TextStyle(fontWeight: FontWeight.w600),
                            ),
                            margin: EdgeInsets.only(bottom: 10.0),
                          ),
                        ],
                      ),
                      new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new GestureDetector(
                            onTap: () =>
                                launch("tel://" + model.dto.phoneNumber),
                            child: new Container(
                              child: new Text(model.dto.phoneNumber),
                              margin: EdgeInsets.only(bottom: 10.0),
                            ),
                          ),
                          new Container(
                            child: new Text(model.dto.address),
                            margin: EdgeInsets.only(bottom: 10.0),
                          ),
                          new GestureDetector(
                            onTap: () => launch(model.companySiteUrl),
                            child: new Container(
                              child: new Text(model.companySiteUrl),
                              margin: EdgeInsets.only(bottom: 10.0),
                            ),
                          ),
                          new GestureDetector(
                            onTap: () => launch("mailto:" + model.infoMail),
                            child: new Container(
                              child: new Text(model.infoMail),
                              margin: EdgeInsets.only(bottom: 10.0),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
