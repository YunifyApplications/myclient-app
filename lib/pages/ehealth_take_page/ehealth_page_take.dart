import 'package:flutter/material.dart';
import 'package:myclient_app/dtos/ehealth_dto.dart';
import 'package:myclient_app/pages/ehealth_take_page/ehealth_page_take_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';

/*
placeholder types:
fixedtext
dynamictext
answer
url
button-collapse
addplaceholder
questionanswer
survey

 */

class EhealthTakePage extends StatefulWidget {
  final EhealthModViewModel dto;
  EhealthTakePage({Key key, @required this.dto}) : super(key: key);

  @override
  _EhealthTakePage createState() => new _EhealthTakePage(dto: this.dto);
}

class _EhealthTakePage extends State<EhealthTakePage> {
  EhealthTakePageViewModel model;

  _EhealthTakePage({@required EhealthModViewModel dto}) {
    model = new EhealthTakePageViewModel(dto: dto);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: new Key('ehealth_take_page'),
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 15.0),
          ),
          new Column(
            children: [
              new Column(
                children: [],
              ),
              new Container(
                width: MediaQuery.of(context).size.width - 20,
                margin: EdgeInsets.only(bottom: 10.0),
                child: new FlatButton(
                  onPressed: () => {},
                  child: new Text(
                    'Inleveren',
                    style: new TextStyle(color: Colors.white),
                  ),
                  color: ColorHelper.fromHex(model.primaryColor),
                  disabledColor: ColorHelper.fromHex(model.primaryColor),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
