import 'package:myclient_app/dtos/ehealth_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class EhealthTakePageViewModel {
  String primaryColor;
  final EhealthModViewModel dto;
  String title;

  EhealthTakePageViewModel({this.dto});

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');
    title = 'E-Health oefening';
  }
}
