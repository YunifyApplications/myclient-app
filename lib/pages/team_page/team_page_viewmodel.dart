import 'package:myclient_app/dtos/team_dto.dart';
import 'package:myclient_app/services/localization_service.dart';

class TeamPageViewModel {
  String primaryColor;
  final TeamDTO dto;
  String title;
  String sendMessageTo;

  TeamPageViewModel({this.dto});

  void loadLocalization() {
    primaryColor = LocalizationService.get('PrimaryColor');
    title = 'Mijn Team';
    sendMessageTo = LocalizationService.get('SendMessageTo');
  }
}
