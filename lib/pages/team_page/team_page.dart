import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/dtos/team_dto.dart';
import 'package:myclient_app/pages/create_conversation_page/create_conversation_page.dart';
import 'package:myclient_app/pages/team_page/team_page_viewmodel.dart';
import 'package:myclient_app/utils/color_helper.dart';

class TeamPage extends StatefulWidget {
  final TeamDTO dto;
  TeamPage({Key key, @required this.dto}) : super(key: key);

  @override
  _TeamPage createState() => new _TeamPage(dto: this.dto);
}

class _TeamPage extends State<TeamPage> {
  TeamPageViewModel model;

  _TeamPage({@required TeamDTO dto}) {
    model = new TeamPageViewModel(dto: dto);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    model.loadLocalization();
    super.initState();
  }

  void sendMessage(TeamMember member) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                new CreateConversationPage(availableRecipients: [
                  new Recipient(
                      receiverId: member.userId,
                      avatarUrl: member.avatarUrl,
                      displayName: member.displayName,
                      isRead: false)
                ], selectedRecipientId: member.userId)));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: new Key('team_page'),
      appBar: new AppBar(
        backgroundColor: ColorHelper.fromHex(model.primaryColor),
        title: new Text(model.title),
        centerTitle: true,
      ),
      body: new Container(
        child: new ListView(
          padding: EdgeInsets.only(bottom: 10.0),
          children: createPanels(),
        ),
      ),
    );
  }

  List<Container> createPanels() {
    return model.dto.members
        .map(
          (member) => new Container(
                width: double.infinity,
                margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                child: new Container(
                  decoration: new BoxDecoration(
                    color: new Color.fromARGB(255, 245, 245, 245),
                    borderRadius: BorderRadius.circular(4.0),
                    border: Border.all(
                      color: new Color.fromARGB(255, 240, 240, 240),
                      width: 1.0,
                    ),
                  ),
                  child: new Column(
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          new Container(
                            margin: EdgeInsets.only(
                                left: 14.0, bottom: 14.0, top: 14.0),
                            child: new CircleAvatar(
                              radius: 90.0,
                              backgroundColor:
                                  ColorHelper.fromHex(model.primaryColor),
                              backgroundImage: new AdvancedNetworkImage(
                                member.avatarUrl,
                                useDiskCache: true,
                              ),
                            ),
                            width: 64.0,
                            height: 64.0,
                            padding: EdgeInsets.all(1.0), // border width
                            decoration: new BoxDecoration(
                              color: new Color.fromARGB(
                                  255, 196, 196, 196), // border color
                              shape: BoxShape.circle,
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.only(left: 15.0),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(
                                  member.displayName,
                                  style: new TextStyle(
                                      color: new Color.fromARGB(
                                          255, 115, 202, 236),
                                      fontStyle: FontStyle.italic,
                                      fontSize: 17.0),
                                ),
                                new Text(
                                  member.expertiseDisplayName,
                                  style: new TextStyle(
                                      color: Colors.black45,
                                      fontStyle: FontStyle.italic,
                                      fontSize: 15.0),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      new Container(
                        margin: EdgeInsets.only(
                            left: 10.0, right: 10.0, bottom: 10.0),
                        child: new Text(
                          member.expertiseDescription,
                          style: new TextStyle(
                              color: new Color.fromARGB(255, 70, 70, 70)),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(
                            bottom: 10.0, left: 10.0, right: 10.0, top: 5.0),
                        padding: const EdgeInsets.only(),
                        height: 30.0,
                        width: double.infinity,
                        decoration: new BoxDecoration(
                          color: ColorHelper.fromHex(model.primaryColor),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: new FlatButton(
                          onPressed: () => sendMessage(member),
                          textColor: Colors.white,
                          color: Colors.transparent,
                          padding: const EdgeInsets.all(0.0),
                          child: new Text(
                            model.sendMessageTo + ' ' + member.displayName,
                            style: new TextStyle(fontSize: 12.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
        )
        .toList();
  }
}
