import 'dart:async';

import 'package:myclient_app/dtos/contact_dto.dart';
import 'package:myclient_app/dtos/dashboard_info_dto.dart';
import 'package:myclient_app/dtos/ehealth_dto.dart';
import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/dtos/login_attempt_dto.dart';
import 'package:myclient_app/dtos/login_result_dto.dart';
import 'package:myclient_app/dtos/mark_message_read_dto.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/dtos/profile_dto.dart';
import 'package:myclient_app/dtos/program_dto.dart';
import 'package:myclient_app/dtos/results_dto.dart';
import 'package:myclient_app/dtos/send_message_result_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/dtos/team_dto.dart';

abstract class IHTTPService {
  Future<LoginResultDTO> attemptLogin(LoginAttemptDTO data);
  Future<DashboardInfoDTO> getDashboard();
  Future<SessionCollectionDTO> getSessions();
  Future<MessageCollectionDTO> getMessages();
  Future markMessageRead(MarkMessageReadDTO data);
  Future<SendMessageResultDTO> sendMessage(Message message);
  Future<TeamDTO> getTeam();
  Future<ProfileDTO> getProfile();
  Future<void> updateProfile(ProfileDTO dto);
  Future<ContactDTO> getContactInfo();
  Future<ProgramCollectionDTO> getPrograms();
  Future<void> markProgramAsRead(int id);
  Future<ExerciseCollectionDTO> getExercises();
  Future<SaveQuestionAnswerResultDTO> saveQuestionAnswer(
      SaveQuestionAnswerDTO dto);
  Future<void> saveSurveyTakeDuration(SaveSurveyTakeDurationDTO dto);
  Future<void> startSurvey(StartSurveyDTO dto);
  Future<FinishSurveyResultDTO> finishSurvey(FinishSurveyDTO dto);
  Future<FinishExerciseResultDTO> finishExercise(FinishExerciseDTO dto);
  Future<ResultCollectionDTO> getResults();
  Future<EhealthModsCollection> getEhealthMods();
  Future<EhealthModViewModel> getEhealthMod(int id);
}
