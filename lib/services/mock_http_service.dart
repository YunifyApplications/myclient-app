import 'dart:async';

import 'package:myclient_app/dtos/contact_dto.dart';
import 'package:myclient_app/dtos/dashboard_info_dto.dart';
import 'package:myclient_app/dtos/ehealth_dto.dart';
import 'package:myclient_app/dtos/exercise_dto.dart';
import 'package:myclient_app/dtos/login_attempt_dto.dart';
import 'package:myclient_app/dtos/login_result_dto.dart';
import 'package:myclient_app/dtos/mark_message_read_dto.dart';
import 'package:myclient_app/dtos/message_dto.dart';
import 'package:myclient_app/dtos/profile_dto.dart';
import 'package:myclient_app/dtos/program_dto.dart';
import 'package:myclient_app/dtos/results_dto.dart';
import 'package:myclient_app/dtos/send_message_result_dto.dart';
import 'package:myclient_app/dtos/session_dto.dart';
import 'package:myclient_app/dtos/team_dto.dart';
import 'package:myclient_app/services/ihttp_service.dart';
import 'package:myclient_app/services/localization_service.dart';

class MockHTTPService implements IHTTPService {
  Future<LoginResultDTO> attemptLogin(LoginAttemptDTO data) async {
    if (data.email == '1@1.com' && data.password == '123') {
      return new LoginResultDTO(LoginResult.SUCCESS, '123');
    } else {
      return new LoginResultDTO(LoginResult.FAILURE, '');
    }
  }

  Future<EhealthModViewModel> getEhealthMod(int id) async {
    return new EhealthModViewModel();
  }

  Future<void> markProgramAsRead(int id) async {
    return;
  }

  Future<EhealthModsCollection> getEhealthMods() async {
    return new EhealthModsCollection(ehealthMods: [
      new EhealthModsViewModel(
          id: 1,
          ehealthModLibDescription: 'Cool and good 1',
          ehealthModLibName: 'Ehealth Module 01',
          startedOn: null,
          appointmentTime: new DateTime(2019, 2, 12, 12, 0),
          closedOn: null,
          completedOn: null,
          messageCount: 2,
          moduleOrMoment: 'module',
          number: '02',
          workflowState: WorkflowState.WaitForClient,
          url: "https://myclient-webui.conveyor.cloud/EhealthMod/take/1"),
      new EhealthModsViewModel(
          id: 2,
          ehealthModLibDescription: 'Cool and good 2',
          ehealthModLibName: 'Ehealth Module 02',
          startedOn: null,
          appointmentTime: new DateTime(2019, 3, 12, 12, 0),
          closedOn: null,
          completedOn: null,
          messageCount: 0,
          moduleOrMoment: 'moment',
          number: '08',
          workflowState: WorkflowState.WaitForUserApproval,
          url: "https://myclient-webui.conveyor.cloud/EhealthMod/take/1"),
      new EhealthModsViewModel(
          id: 3,
          ehealthModLibDescription: 'Cool and good 3',
          ehealthModLibName: 'Ehealth Module 03',
          startedOn: null,
          appointmentTime: new DateTime(2019, 4, 12, 12, 0),
          closedOn: null,
          completedOn: null,
          messageCount: 6,
          moduleOrMoment: 'module',
          number: '16',
          workflowState: WorkflowState.Completed,
          url: "https://myclient-webui.conveyor.cloud/EhealthMod/take/1"),
      new EhealthModsViewModel(
          id: 4,
          ehealthModLibDescription: 'Cool and good 4',
          ehealthModLibName: 'Ehealth Module 04',
          startedOn: null,
          appointmentTime: new DateTime(2019, 5, 12, 12, 0),
          closedOn: null,
          completedOn: null,
          messageCount: 0,
          moduleOrMoment: 'module',
          number: '16',
          workflowState: WorkflowState.Closed,
          url: "https://myclient-webui.conveyor.cloud/EhealthMod/take/1"),
    ], resultStatus: RequestResult.SUCCESS);
  }

  Future<FinishExerciseResultDTO> finishExercise(FinishExerciseDTO dto) async {
    return new FinishExerciseResultDTO(resultStatus: RequestResult.SUCCESS);
  }

  Future<FinishSurveyResultDTO> finishSurvey(FinishSurveyDTO dto) async {
    return new FinishSurveyResultDTO(
        'Vraag 1 is verplicht.\nVraak 2 is verplicht.',
        RequestResult.SUCCESS,
        FinishSurveyResult.FAILURE);
  }

  Future<SaveQuestionAnswerResultDTO> saveQuestionAnswer(
      SaveQuestionAnswerDTO dto) async {
    return new SaveQuestionAnswerResultDTO(resultStatus: RequestResult.SUCCESS);
  }

  Future<void> saveSurveyTakeDuration(SaveSurveyTakeDurationDTO dto) async {
    return;
  }

  Future<void> startSurvey(StartSurveyDTO dto) async {
    return;
  }

  Future<ExerciseCollectionDTO> getExercises() async {
    BinaryData pdfFile = new BinaryData(
      id: 220,
      fileName: 'Pijneducatie.pdf',
      extension: '.pdf',
      idFile: '605ABFD8-8582-4978-842E-53AFA6E571F9',
      retrieveUrl:
          'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf', // http://demo.hetrughuis.nl/api/program/file/[program id]
    );
    BinaryData pdfFile2 = new BinaryData(
      id: 221,
      fileName: 'Pijneducatie.pdf',
      extension: '.pdf',
      idFile: '605ABFD8-8582-4978-842E-53AFA6E571F9',
      retrieveUrl:
          'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf', // http://demo.hetrughuis.nl/api/program/file/[program id]
    );
    Expertise cgt = new Expertise(
        id: 1,
        name: 'Psycholoog CGT',
        code: 'CGT',
        description:
            'De psycholoog CGT onderzoekt in eerste instantie met u of en welke');

    Expertise ps = new Expertise(
        id: 1,
        name: 'Psycholoog PS',
        code: 'PS',
        description:
            'De psycholoog PS onderzoekt in eerste instantie met u of en welke');
    ExerciseLib e1 = new ExerciseLib(
        id: 1,
        description: 'Cool exercise',
        moduleNumber: 0,
        name: 'EXERCISE - 001',
        isForm: false,
        isVimeo: false,
        isYoutube: false,
        expertise: cgt,
        survey: null,
        url: null,
        imageData: new BinaryData(
          id: 221,
          fileName: '9f12acaa-cd27-490e-a066-5032ed95b6b7.jpg',
          extension: '.jpg',
          idFile: '5870ADDA-9E13-4675-9BBB-4A00F29B0625',
          retrieveUrl:
              'http://demo.hetrughuis.nl/images/thumb/221_9f12acaa-cd27-490e-a066-5032ed95b6b7.jpg', // http://demo.hetrughuis.nl/images/thumb/[id]_[idFile]
        ),
        fileData: pdfFile);
    ExerciseLib e2 = new ExerciseLib(
        id: 2,
        description: 'Cool exercise 2',
        moduleNumber: 4,
        name: 'EXERCISE - 002',
        isForm: false,
        isVimeo: false,
        isYoutube: false,
        expertise: ps,
        survey: null,
        url: null,
        imageData: new BinaryData(
          id: 221,
          fileName: '9f12acaa-cd27-490e-a066-5032ed95b6b7.jpg',
          extension: '.jpg',
          idFile: '5870ADDA-9E13-4675-9BBB-4A00F29B0625',
          retrieveUrl:
              'http://demo.hetrughuis.nl/images/thumb/221_9f12acaa-cd27-490e-a066-5032ed95b6b7.jpg', // http://demo.hetrughuis.nl/images/thumb/[id]_[idFile]
        ),
        fileData: pdfFile2);

    QuestionTypeProperty qtp1 = new QuestionTypeProperty(
      id: 8,
      name: 'RepeatMatrixHeader',
    );

    QuestionTypePropertyValue qtpv1 = new QuestionTypePropertyValue(
      id: 1,
      value: '0',
      questionTypeProperty: qtp1,
    );

    QuestionTypeGroup qtg1 = new QuestionTypeGroup(
        id: 1,
        name: '0-10',
        description: null,
        questionTypePropertyValues: [qtpv1]);

    QuestionType qt1 = new QuestionType(
        id: 8, name: 'SingleSelectMatrix', questionTypeProperties: [qtp1]);

    QuestionType qt2 = new QuestionType(id: 1, name: 'MultiLineTextBox');
    QuestionType qt6 = new QuestionType(id: 61, name: 'SingleLineTextBox');

    QuestionType qt3 = new QuestionType(id: 2, name: 'HeaderInfo');

    OptionChoice oc1 = new OptionChoice(
        id: 1,
        name: '0',
        sortOrder: 1,
        value: 1,
        dataLocalizationJsonCollection: null);
    OptionChoice oc2 = new OptionChoice(
        id: 2,
        name: '2',
        sortOrder: 2,
        value: 2,
        dataLocalizationJsonCollection: null);
    OptionChoice oc3 = new OptionChoice(
        id: 3,
        name: '3',
        sortOrder: 3,
        value: 3,
        dataLocalizationJsonCollection: null);

    OptionGroup og1 = new OptionGroup(
        id: 1,
        name: 'groep2',
        description: null,
        optionChoices: [oc1, oc2, oc3]);

    Question q1 = new Question(
        id: 1,
        sortOrder: 1,
        name: 'Wat vindt u, over het algemeen genomen, van uw gezondheid?',
        code: null,
        required: true,
        answerText: null,
        selectedValue: null,
        questionNumber: '1',
        parentQuestion: null,
        questionType: qt1,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: og1,
        questionSubclasses: null,
        questionTypeGroup: qtg1);

    Question q2 = new Question(
        id: 2,
        sortOrder: 3,
        name: 'Wat is dit?',
        code: null,
        required: true,
        answerText: 'hehe',
        selectedValue: null,
        questionNumber: '2',
        parentQuestion: null,
        questionType: qt2,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: null,
        questionSubclasses: null,
        questionTypeGroup: null);

    Question q61 = new Question(
        id: 7,
        sortOrder: 7,
        name: 'Waarom is een banaan krom?',
        code: null,
        required: true,
        answerText: null,
        selectedValue: null,
        questionNumber: '8',
        parentQuestion: null,
        questionType: qt6,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: null,
        questionSubclasses: null,
        questionTypeGroup: null);

    Question q3 = new Question(
        id: 3,
        sortOrder: 2,
        name:
            'Vul het weekschema in met de gedachte een weekschema op te stellen waarbij u zo min mogelijk pijnklachten zou hebben. Het is wel belangrijk realistisch te blijven bij het opstellen van het schema! U heeft immers niet iedere week vakantie.',
        code: null,
        required: true,
        answerText: null,
        selectedValue: null,
        questionNumber: 'a',
        parentQuestion: null,
        questionType: qt3,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: null,
        questionSubclasses: null,
        questionTypeGroup: null);

    QuestionType qt4 = new QuestionType(id: 2, name: 'Header');

    Question q5 = new Question(
        id: 6,
        sortOrder: 2,
        name: 'header egriopwgheirw wq[eoirqwer wqe t wqehit pweq htweq twqe f',
        code: null,
        required: true,
        answerText: null,
        selectedValue: null,
        questionNumber: 'b',
        parentQuestion: null,
        questionType: qt4,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: null,
        questionSubclasses: null,
        questionTypeGroup: null);

    QuestionType trackbarType = new QuestionType(
        id: 13, name: 'Trackbar', questionTypeProperties: null);

    var QuestionTypePropertyValue1 = new QuestionTypePropertyValue(
        id: 1,
        questionTypeProperty: new QuestionTypeProperty(id: 1, name: 'MinVal'),
        value: '5');
    var QuestionTypePropertyValue2 = new QuestionTypePropertyValue(
        id: 1,
        questionTypeProperty: new QuestionTypeProperty(id: 1, name: 'MaxVal'),
        value: '15');
    var QuestionTypePropertyValue3 = new QuestionTypePropertyValue(
        id: 1,
        questionTypeProperty:
            new QuestionTypeProperty(id: 1, name: 'LargeTickInterval'),
        value: '5');

    QuestionTypeGroup qtgTrackbar = new QuestionTypeGroup(
        id: 2,
        name: 'TrackbarGroup',
        description: null,
        questionTypePropertyValues: [
          QuestionTypePropertyValue1,
          QuestionTypePropertyValue2,
          QuestionTypePropertyValue3
        ]);

    Question trackbarQuestion = new Question(
        id: 5,
        sortOrder: 5,
        name: 'Vul iets in van 0 tot 10',
        code: null,
        required: true,
        answerText: null,
        selectedValue: null,
        questionNumber: '5',
        parentQuestion: null,
        questionType: trackbarType,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: null,
        questionSubclasses: null,
        trackbarValue: 5.0,
        questionTypeGroup: qtgTrackbar);

    OptionChoice optionChoiceSingleSelect1 = new OptionChoice(
        id: 488,
        name:
            "hardlopen of wielrennen op wedstrijdniveau (extra zware belasting)",
        value: 1,
        sortOrder: 1,
        dataLocalizationJsonCollection: null);

    OptionChoice optionChoiceSingleSelect2 = new OptionChoice(
        id: 489,
        name: "balsporten op wedstrijdniveau (zware belasting)",
        value: 1,
        sortOrder: 2,
        dataLocalizationJsonCollection: null);

    OptionChoice optionChoiceSingleSelect3 = new OptionChoice(
        id: 490,
        name:
            "recreatief fietsen, recreatiesporten, sportief wandelen, fitness (matig intensieve belastingen)",
        value: 1,
        sortOrder: 3,
        dataLocalizationJsonCollection: null);

    OptionChoice optionChoiceSingleSelect4 = new OptionChoice(
        id: 491,
        name: "rustig wandelen, rustig fietsen (lichte belastingen)",
        value: 1,
        sortOrder: 4,
        dataLocalizationJsonCollection: null);

    OptionGroup optionGroupSingleSelect = new OptionGroup(
        id: 96,
        name: 'NNGB vraag1',
        description: null,
        optionChoices: [
          optionChoiceSingleSelect1,
          optionChoiceSingleSelect2,
          optionChoiceSingleSelect3,
          optionChoiceSingleSelect4
        ]);

    OptionGroup optionGroupDropDownList = new OptionGroup(
        id: 97,
        name: 'NNGB vraag2',
        description: null,
        optionChoices: [
          optionChoiceSingleSelect1,
          optionChoiceSingleSelect2,
          optionChoiceSingleSelect3,
          optionChoiceSingleSelect4
        ]);

    QuestionType singleSelectType =
        new QuestionType(id: 6, name: 'SingleSelect');

    QuestionType dropDownListType =
        new QuestionType(id: 6, name: 'DropDownList');

    QuestionType dropDownListWithInputType =
        new QuestionType(id: 6, name: 'DropDownListWithInput');

    QuestionType multiSelectType = new QuestionType(id: 6, name: 'MultiSelect');

    Question multiSelect = new Question(
        id: 11,
        sortOrder: 4,
        name: 'vraag met meerdere antwoorden',
        code: null,
        required: true,
        answerText: '488,491',
        selectedValue: null,
        questionNumber: '9',
        parentQuestion: q2,
        questionType: multiSelectType,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: optionGroupSingleSelect,
        questionSubclasses: null,
        questionTypeGroup: null);

    Question singleSelect = new Question(
        id: 12,
        sortOrder: 4,
        name: 'Aan welke beweegactiviteiten doet u?',
        code: null,
        required: true,
        answerText: null,
        selectedValue: null,
        questionNumber: '4',
        parentQuestion: q2,
        questionType: singleSelectType,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: optionGroupSingleSelect,
        questionSubclasses: null,
        questionTypeGroup: null);

    var QuestionTypePropertyValue5 = new QuestionTypePropertyValue(
        id: 1,
        questionTypeProperty:
            new QuestionTypeProperty(id: 1, name: 'NumMinVal'),
        value: '2');
    var QuestionTypePropertyValue6 = new QuestionTypePropertyValue(
        id: 1,
        questionTypeProperty:
            new QuestionTypeProperty(id: 1, name: 'NumMaxVal'),
        value: '15');

    QuestionType integerNumericBoxType =
        new QuestionType(id: 6, name: 'IntegerNumericBox');

    QuestionTypeGroup qtgIntegerNumericBox = new QuestionTypeGroup(
        id: 2,
        name: 'IntegerNumericBoxGroup',
        description: null,
        questionTypePropertyValues: [
          QuestionTypePropertyValue5,
          QuestionTypePropertyValue6,
        ]);

    Question integerNumericBox = new Question(
        id: 13,
        sortOrder: 8,
        name: 'Aan welke beweegactiviteiten doet u in nummers.',
        code: null,
        required: true,
        answerText: null,
        selectedValue: null,
        questionNumber: '7',
        parentQuestion: null,
        questionType: integerNumericBoxType,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: null,
        questionSubclasses: null,
        questionTypeGroup: qtgIntegerNumericBox);

    Question dropDownList = new Question(
        id: 14,
        sortOrder: 7,
        name: 'Aan welke beweegactiviteiten doet u?',
        code: null,
        required: true,
        answerText: null,
        selectedValue: 489,
        questionNumber: '6',
        parentQuestion: null,
        questionType: dropDownListType,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: optionGroupDropDownList,
        questionSubclasses: null,
        questionTypeGroup: null);

    Question dropDownListWithInput = new Question(
        id: 15,
        sortOrder: 7,
        name: 'Aan welke beweegactiviteiten doet u?',
        code: null,
        required: true,
        answerText: 'H E L L O',
        selectedValue: 489,
        questionNumber: '6',
        parentQuestion: null,
        questionType: dropDownListWithInputType,
        parentOptionChoice: null,
        dataLocalizationJsonCollection: null,
        optionGroup: optionGroupDropDownList,
        questionSubclasses: null,
        questionTypeGroup: null);

    SurveySection sec1 = new SurveySection(
        id: 1,
        name: 'Sectie1',
        description:
            'In dit deel van de vragenlijst wordt naar uw gezondheid gevraagd. Wilt u elke vraag beantwoorden door het juiste hokje aan te kruisen. Wanneer u twijfelt over het antwoord op een vraag, probeer dan het antwoord te geven dat het meest van toepassing is.',
        otherHeaderInfo: null,
        sortOrder: 1,
        questions: [q3, q1, dropDownList, multiSelect, q61]);

    SurveySection sec2 = new SurveySection(
        id: 1,
        name: 'Sectie2',
        description:
            'Denkt u aan uw huidige episode met rugpijn en geeft u aan in welke mate een aantal gedachten of gevoelens bij u opkomt. Zet u een rondje om het cijfer dat hierop van toepassing is.',
        otherHeaderInfo: null,
        sortOrder: 2,
        questions: [
          q5,
          q2,
          singleSelect,
          trackbarQuestion,
          integerNumericBox,
          dropDownListWithInput
        ]);

    SurveyLib sl1 = new SurveyLib(
        id: 1,
        expertise: cgt,
        description: 'Patiënt Specifieke Klachten (Beurskens 1996)',
        code: 'PSK',
        isForm: true,
        name: 'PSK',
        durationInMinutes: 90,
        instructions: 'druk op de knop :)',
        sections: [sec1, sec2]);

    Survey s1 = new Survey(
        id: 1,
        conclusion: null,
        progressInSeconds: null,
        releaseScoreToClient: false,
        startedOn: null,
        surveyLib: sl1);

    ExerciseLib e3 = new ExerciseLib(
        id: 3,
        description: 'Cool survey',
        moduleNumber: 16,
        name: 'EXERCISE - 003 with a pretty long name that needs 2 lines',
        isForm: true,
        isVimeo: false,
        isYoutube: false,
        expertise: cgt,
        survey: s1,
        url: null,
        imageData: new BinaryData(
          id: 221,
          fileName: '9f12acaa-cd27-490e-a066-5032ed95b6b7.jpg',
          extension: '.jpg',
          idFile: '5870ADDA-9E13-4675-9BBB-4A00F29B0625',
          retrieveUrl:
              'http://demo.hetrughuis.nl/images/thumb/221_9f12acaa-cd27-490e-a066-5032ed95b6b7.jpg', // http://demo.hetrughuis.nl/images/thumb/[id]_[idFile]
        ),
        fileData: null);

    Exercise ex1 = new Exercise(id: 1, done: false, exerciseLib: e1);
    Exercise ex2 = new Exercise(id: 2, done: true, exerciseLib: e2);
    Exercise ex3 = new Exercise(id: 3, done: false, exerciseLib: e3);

    ExerciseCollectionDTO dto = new ExerciseCollectionDTO(
        exercises: [ex1, ex2, ex3], resultStatus: RequestResult.SUCCESS);

    return dto;
  }

  Future<ProgramCollectionDTO> getPrograms() async {
    return new ProgramCollectionDTO(
      resultStatus: RequestResult.SUCCESS,
      programs: [
        new Program(
          id: 949,
          done: false,
          assignedHtmlContentId: null,
          memo: null,
          programLibId: 1,
          programLib: new ProgramLib(
            id: 1,
            subjectId: 1,
            subject: new Subject(
                id: 1,
                name: 'Algemeen',
                description: null,
                isExercise: false,
                isProgram: true,
                isSurvey: false),
            name: '01.1 Pijn educatie',
            imageDataId: 221,
            imageData: new BinaryData(
              id: 221,
              fileName: '9f12acaa-cd27-490e-a066-5032ed95b6b7.jpg',
              extension: '.jpg',
              idFile: '5870ADDA-9E13-4675-9BBB-4A00F29B0625',
              retrieveUrl:
                  'http://demo.hetrughuis.nl/images/thumb/221_9f12acaa-cd27-490e-a066-5032ed95b6b7.jpg', // http://demo.hetrughuis.nl/images/thumb/[id]_[idFile]
            ),
            fileDataId: 220,
            fileData: new BinaryData(
              id: 220,
              fileName: 'Pijneducatie.pdf',
              extension: '.pdf',
              idFile: '605ABFD8-8582-4978-842E-53AFA6E571F9',
              retrieveUrl:
                  'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf', // http://demo.hetrughuis.nl/api/program/file/[program id]
            ),
            expertiseId: 1,
            expertise: new Expertise(
              id: 1,
              name: 'Fysiotherapeut',
              code: 'FT',
              description:
                  'De fysiotherapeut houdt zich bezig met het fysiek/ lichamelijke aspect van uw klachten. Samen met de fysiotherapeut werkt u aan het verbeteren van de mobiliteit, kracht en stabiliteit van uw rug- en/of nekklachten. Er worden oefeningen aangeboden die u zelfstandig thuis kunt doornemen, met als uiteindelijke doel optimaal functioneren in het dagelijks leven. ',
              mustBookCareContact: true,
            ),
            sectionId: 5,
            section: new Section(
              id: 5,
              isExercise: false,
              description: null,
              name: 'Behandeling',
              isSurvey: false,
              isProgram: true,
            ),
            description:
                'Om te leren omgaan met uw pijn is het belangrijk om te weten wat pijn is',
            htmlContentId: null,
            htmlContent: null,
            moduleNumber: 0,
          ),
        ),
        new Program(
          id: 950,
          done: false,
          assignedHtmlContentId: null,
          memo: null,
          programLibId: 2,
          programLib: new ProgramLib(
            id: 2,
            subjectId: 2,
            subject: new Subject(
                id: 2,
                name: 'Algemeen',
                description: null,
                isExercise: false,
                isProgram: true,
                isSurvey: false),
            name: '01.3 Introductie in trainen',
            imageDataId: 251,
            imageData: new BinaryData(
              id: 251,
              fileName: '92a2352d-f15e-4ba0-972d-558bd0668b32.jpg',
              extension: '.jpg',
              idFile: '5870ADDA-9E13-4675-9BBB-4A00F29B0626',
              retrieveUrl:
                  'http://demo.hetrughuis.nl/images/thumb/251_92a2352d-f15e-4ba0-972d-558bd0668b32.jpg', // http://demo.hetrughuis.nl/images/thumb/[id]_[idFile]
            ),
            fileDataId: null,
            fileData: null,
            expertiseId: 2,
            expertise: new Expertise(
              id: 2,
              name: 'Psycholoog CGT',
              code: 'CGT',
              description:
                  'De fysiotherapeut houdt zich bezig met het fysiek/ lichamelijke aspect van uw klachten. Samen met de fysiotherapeut werkt u aan het verbeteren van de mobiliteit, kracht en stabiliteit van uw rug- en/of nekklachten. Er worden oefeningen aangeboden die u zelfstandig thuis kunt doornemen, met als uiteindelijke doel optimaal functioneren in het dagelijks leven. ',
              mustBookCareContact: true,
            ),
            sectionId: 5,
            section: new Section(
              id: 5,
              isExercise: false,
              description: null,
              name: 'Behandeling',
              isSurvey: false,
              isProgram: true,
            ),
            description:
                'Om te leren omgaan met uw pijn is het belangrijk om te weten wat pijn is',
            htmlContentId: 15,
            htmlContent: new HtmlContent(
              id: 15,
              version: 1,
              title: 'Hallo jumbo',
              description: 'description',
              code: '<html><h1>meme 123</h1></html>',
            ),
            moduleNumber: 2,
          ),
        ),
        new Program(
          id: 955,
          done: false,
          assignedHtmlContentId: null,
          memo: null,
          programLibId: 3,
          programLib: new ProgramLib(
            id: 3,
            subjectId: 4,
            subject: new Subject(
                id: 4,
                name: 'Algemeen',
                description: null,
                isExercise: false,
                isProgram: true,
                isSurvey: false),
            name: '01.4 Introductie in koken',
            imageDataId: 255,
            imageData: new BinaryData(
              id: 255,
              fileName: '92a2352d-f15e-4ba0-972d-558bd0668b32.jpg',
              extension: '.jpg',
              idFile: '5870ADDA-9E13-4675-9BBB-4A00F29B0626',
              retrieveUrl:
                  'http://demo.hetrughuis.nl/images/thumb/251_92a2352d-f15e-4ba0-972d-558bd0668b32.jpg', // http://demo.hetrughuis.nl/images/thumb/[id]_[idFile]
            ),
            fileDataId: null,
            fileData: null,
            expertiseId: 2,
            expertise: new Expertise(
              id: 2,
              name: 'Psycholoog PS',
              code: 'PS',
              description:
                  'De fysiotherapeut houdt zich bezig met het fysiek/ lichamelijke aspect van uw klachten. Samen met de fysiotherapeut werkt u aan het verbeteren van de mobiliteit, kracht en stabiliteit van uw rug- en/of nekklachten. Er worden oefeningen aangeboden die u zelfstandig thuis kunt doornemen, met als uiteindelijke doel optimaal functioneren in het dagelijks leven. ',
              mustBookCareContact: true,
            ),
            sectionId: 5,
            section: new Section(
              id: 5,
              isExercise: false,
              description: null,
              name: 'Behandeling',
              isSurvey: false,
              isProgram: true,
            ),
            description:
                'Om te leren omgaan met uw pijn is het belangrijk om te weten wat pijn is',
            htmlContentId: 16,
            htmlContent: new HtmlContent(
              id: 16,
              version: 1,
              title: 'Hallo jumbo',
              description: 'description',
              code: '<html><h1>meme 123</h1></html>',
            ),
            moduleNumber: 2,
          ),
        ),
      ],
    );
  }

  Future<void> updateProfile(ProfileDTO dto) async {
    return;
  }

  Future<ContactDTO> getContactInfo() async {
    return new ContactDTO(
        phoneNumber: '+311233211233',
        address: 'Scharnerweg 127 6224 JC Maastricht',
        resultStatus: RequestResult.SUCCESS);
  }

  Future<ProfileDTO> getProfile() async {
    return new ProfileDTO(
        userName: 'ruud.bruls@mail.com',
        firstName: 'Ruud',
        lastName: 'Bruls',
        birthDate: new DateTime(1950, 5, 12),
        gender: 'Man',
        phoneNumber: '+31234123123',
        email: 'ruud.bruls@mail.com',
        receiveWeekOverviewPlannedAppointments: false,
        receiveEmail24HoursBeforeAppointment: false,
        receiveSms24HoursBeforeAppointment: true,
        receiveGeneralEmailNotifications: false,
        resultStatus: RequestResult.SUCCESS);
  }

  Future<TeamDTO> getTeam() async {
    return new TeamDTO([
      new TeamMember(
        userId: "258D229F-21BD-4725-8F75-627994685137",
        displayName: "Hans Klok",
        avatarUrl:
            "https://www.citrix.com/blogs/wp-content/uploads/2017/11/Citrix-Blog-User-Bio-Photo-6.png",
        expertiseDisplayName: "Psycholoog PS",
        expertiseDescription:
            "De psycholoog psychosomatiek legt een link tussen het fysieke en de psyche. Aandachtspunten binnen deze sessies zijn: ontspanningsmogelijkheden, ademhalingsoefeningen, fysieke en mentale grenzen leren erkennen en bewaken en het creëren van een goede dagindeling waarin rust en activiteit afgestemd zijn. Tevens wordt gekeken naar in stand houdende factoren zoals coping, karakter en stressoren.",
      ),
      new TeamMember(
          userId: "258D229F-21BD-4725-8F75-627994685139",
          displayName: "John van Johnson",
          avatarUrl:
              "https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg",
          expertiseDisplayName: "Psycholoog CGT",
          expertiseDescription:
              "De psycholoog CGT onderzoekt in eerste instantie met u of en welke link er bestaat tussen psychologische factoren en uw pijnklachten. Hierbij kan gedacht worden aan persoonlijkheid, coping, stressoren en verleden. Cognitieve gedragstherapie bestaat uit het uitdagen van niet-helpende gedachtegangen en/of het aanleren van ander gedrag, waarbij bovengenoemde gebieden als hoofdthema functioneren. Door aan bovengenoemde factoren te werken, zal dit ook een positieve invloed kunnen hebben op uw lichamelijke gesteldheid."),
      new TeamMember(
          userId: "258D229F-21BD-4725-8F75-627994685138",
          displayName: "Herman Guacamole",
          avatarUrl:
              "https://cdn.images.express.co.uk/img/dynamic/galleries/x701/152480.jpg",
          expertiseDisplayName: "Psycholoog DDE",
          expertiseDescription:
              "De CSM is het aanspreekpersoon op de locatie voor zaken met betrekking tot planning, wijzigingen doorgeven en dergelijke. Ook voor alle andere vragen die niet met uw behandelaar besproken kunnen worden is de CSM uw aanspreekpunt. De CSM vervult de rol van gastvrouw op de locatie en informeert u over allerlei praktische zaken. Verder zorgt zij er voor dat u verwijzer en/of huisarts tijdig de rapportages ontvangt over uw behandeltraject bij Het Rughuis."),
    ], RequestResult.SUCCESS);
  }

  Future<SessionCollectionDTO> getSessions() async {
    return new SessionCollectionDTO(
      RequestResult.SUCCESS,
      [
        new Session(
            'HRBP14B - Module 00',
            1,
            'John Van Johnson',
            'https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg',
            '258D229F-21BD-4725-8F75-627994685139',
            0,
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.',
            'Maastricht',
            3,
            new DateTime(2018, 11, 3, 22, 0, 0),
            new DateTime(2018, 11, 3, 23, 0, 0),
            'Psycholoog CGT',
            'OV',
            false),
        new Session(
            'HRBP14B - Module 01',
            2,
            'John Van Johnson',
            'https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg',
            '258D229F-21BD-4725-8F75-627994685139',
            0,
            'Afspraak Psycholoog PS intake gesprek voor eerste behandeling.',
            'Maastricht',
            3,
            new DateTime(2018, 11, 8, 12, 0, 0),
            new DateTime(2018, 11, 8, 14, 0, 0),
            'Psycholoog FT',
            'FT',
            false),
        new Session(
            'HRBP14B - Module 02',
            3,
            'Hans Klok',
            'https://www.citrix.com/blogs/wp-content/uploads/2017/11/Citrix-Blog-User-Bio-Photo-6.png',
            '258D229F-21BD-4725-8F75-627994685137',
            0,
            'Afspraak Psycholoog PS intake gesprek voor eerste behandeling.',
            'Maastricht',
            3,
            new DateTime(2018, 11, 9, 8, 0, 0),
            new DateTime(2018, 11, 9, 9, 30, 0),
            'Psycholoog PS',
            'PS',
            false),
        new Session(
            'HRBP14B - Module 03',
            4,
            'Herman Guacamole',
            'https://cdn.images.express.co.uk/img/dynamic/galleries/x701/152480.jpg',
            '258D229F-21BD-4725-8F75-627994685138',
            0,
            'Afspraak Psycholoog DDE intake gesprek voor eerste behandeling.',
            'Maastricht',
            3,
            new DateTime(2018, 11, 13, 8, 0, 0),
            new DateTime(2018, 11, 13, 9, 30, 0),
            'Psycholoog DDE',
            'DDE',
            false),
      ],
    );
  }

  Future<DashboardInfoDTO> getDashboard() async {
    return new DashboardInfoDTO(
        avatarUrl: 'https://i.cricketcb.com/stats/img/faceImages/8364.jpg',
        displayName: 'Ruud Bruls',
        email: 'ruud.bruls@mail.com',
        userId: '96E644C8-E520-4AAE-93F0-3EF3C3A7637F',
        messageCount: 3.0,
        resultCount: 2.0,
        appointmentCount: 4.0,
        programCount: 3.0,
        exerciseCount: 2.0,
        ehealthCount: 0.0,
        resultStatus: RequestResult.SUCCESS);
  }

  Future<ResultCollectionDTO> getResults() async {
    List<TestResult> results = new List();

    if (LocalizationService.tenant == Tenant.RUG) {
      results.addAll([
        new TestResult(
          name: 'ClinimetryDashboard',
          url:
              'https://myclient-webui.conveyor.cloud/Chart/Clinimetry/FE2B0564-A2E6-404F-AACD-DF0F1E61E312',
          isPdf: false,
        ),
        new TestResult(
          name: 'VitalityDashboard',
          url:
              'https://myclient-webui.conveyor.cloud/Chart/Clinimetry/FE2B0564-A2E6-404F-AACD-DF0F1E61E312',
          isPdf: false,
        ),
        new TestResult(
          name: 'Gevolgmodellen',
          url:
              'https://myclient-webui.conveyor.cloud/Chart/Clinimetry/FE2B0564-A2E6-404F-AACD-DF0F1E61E312',
          isPdf: false,
        ),
      ]);
    }

    results.addAll([
      // PDFs
      new TestResult(
        name: 'Life Style Plan',
        url: 'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf',
        isPdf: true,
      ),
      new TestResult(
        name: 'Resultaten en conclusies',
        url: 'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf',
        isPdf: true,
      ),
      new TestResult(
        name: 'David 12',
        url: 'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf',
        isPdf: true,
      ),
      new TestResult(
        name: 'Resultaten en conclusies snapshot 19-05-2016',
        url: 'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf',
        isPdf: true,
      ),
    ]);

    return new ResultCollectionDTO(results, RequestResult.SUCCESS);
  }

  Future<MessageCollectionDTO> getMessages() async {
    return new MessageCollectionDTO(RequestResult.SUCCESS, [
      new Message(
        id: 3,
        messageBody: "Dit is een bericht 3!",
        parentId: 1,
        sendDate: new DateTime(2018, 8, 25, 15, 12, 0),
        subject: "Hallo jumbo",
        senderId: "258D229F-21BD-4725-8F75-627994685139",
        senderAvatarUrl:
            "https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg",
        senderDisplayName: "John Van Johnson",
        sentToTeam: false,
        recipients: [
          new Recipient(
              isRead: false,
              receiverId: "96E644C8-E520-4AAE-93F0-3EF3C3A7637F",
              displayName: "Ruud Bruls",
              avatarUrl:
                  "https://i.cricketcb.com/stats/img/faceImages/8364.jpg"),
        ],
        parent: new Message(
          id: 1,
          messageBody: "Dit is bericht nummer 1!",
          parentId: null,
          sendDate: new DateTime(2018, 8, 22, 22, 22, 10),
          subject: "Hallo jumbo 3",
          senderId: "258D229F-21BD-4725-8F75-627994685139",
          senderAvatarUrl:
              "https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg",
          senderDisplayName: "John Van Johnson",
          sentToTeam: false,
          recipients: [
            new Recipient(
                isRead: true,
                receiverId: "96E644C8-E520-4AAE-93F0-3EF3C3A7637F",
                displayName: "Ruud Bruls",
                avatarUrl:
                    "https://i.cricketcb.com/stats/img/faceImages/8364.jpg"),
          ],
        ),
      ),
      new Message(
        id: 2,
        messageBody: "Dit is een tweede bericht!",
        parentId: null,
        sendDate: new DateTime(2018, 10, 22, 14, 42, 0),
        subject: "Hallo jumbo 2",
        senderId: "258D229F-21BD-4725-8F75-627994685139",
        senderAvatarUrl:
            "https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg",
        senderDisplayName: "John Van Johnson",
        sentToTeam: false,
        recipients: [
          new Recipient(
              isRead: true,
              receiverId: "96E644C8-E520-4AAE-93F0-3EF3C3A7637F",
              displayName: "Ruud Bruls",
              avatarUrl:
                  "https://i.cricketcb.com/stats/img/faceImages/8364.jpg"),
        ],
      ),
      new Message(
        id: 5,
        messageBody: "Dit is een groeps bericht met 2 ongelezen berichten",
        parentId: 4,
        sendDate: new DateTime(2018, 8, 30, 21, 12, 6),
        subject: "Hallo jumbo groep",
        senderId: "258D229F-21BD-4725-8F75-627994685139",
        senderAvatarUrl:
            "https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg",
        senderDisplayName: "John Van Johnson",
        sentToTeam: true,
        recipients: [
          new Recipient(
            isRead: false,
            receiverId: "96E644C8-E520-4AAE-93F0-3EF3C3A7637F",
            displayName: "Ruud Bruls",
            avatarUrl: "https://i.cricketcb.com/stats/img/faceImages/8364.jpg",
          ),
          new Recipient(
            isRead: false,
            receiverId: "258D229F-21BD-4725-8F75-627994685137",
            displayName: "Hans Klok",
            avatarUrl:
                "https://www.citrix.com/blogs/wp-content/uploads/2017/11/Citrix-Blog-User-Bio-Photo-6.png",
          ),
          new Recipient(
            isRead: true,
            receiverId: "258D229F-21BD-4725-8F75-627994685138",
            displayName: "Herman Guacamole",
            avatarUrl:
                "https://cdn.images.express.co.uk/img/dynamic/galleries/x701/152480.jpg",
          ),
        ],
        parent: new Message(
          id: 4,
          messageBody: "Dit is een groeps bericht !!!!!!!!!",
          parentId: null,
          sendDate: new DateTime(2018, 8, 29, 11, 47, 54),
          subject: "Hallo jumbo groep",
          senderId: "258D229F-21BD-4725-8F75-627994685138",
          senderAvatarUrl:
              "https://cdn.images.express.co.uk/img/dynamic/galleries/x701/152480.jpg",
          senderDisplayName: "Herman Guacamole",
          sentToTeam: true,
          recipients: [
            new Recipient(
              isRead: false,
              receiverId: "96E644C8-E520-4AAE-93F0-3EF3C3A7637F",
              displayName: "Ruud Bruls",
              avatarUrl:
                  "https://i.cricketcb.com/stats/img/faceImages/8364.jpg",
            ),
            new Recipient(
              isRead: true,
              receiverId: "258D229F-21BD-4725-8F75-627994685137",
              displayName: "Hans Klok",
              avatarUrl:
                  "https://www.citrix.com/blogs/wp-content/uploads/2017/11/Citrix-Blog-User-Bio-Photo-6.png",
            ),
            new Recipient(
                isRead: true,
                receiverId: "258D229F-21BD-4725-8F75-627994685139",
                displayName: "John van Johnson",
                avatarUrl:
                    "https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg"),
          ],
        ),
      ),
    ], [
      new Recipient(
        isRead: false,
        receiverId: "258D229F-21BD-4725-8F75-627994685137",
        displayName: "Hans Klok",
        avatarUrl:
            "https://www.citrix.com/blogs/wp-content/uploads/2017/11/Citrix-Blog-User-Bio-Photo-6.png",
      ),
      new Recipient(
          isRead: false,
          receiverId: "258D229F-21BD-4725-8F75-627994685139",
          displayName: "John van Johnson",
          avatarUrl:
              "https://static1.squarespace.com/static/54f74f23e4b0952b4e0011c0/t/5ad5431e88251baeaac75f49/1523925845937/chris+hanna+bb.jpg"),
      new Recipient(
        isRead: false,
        receiverId: "258D229F-21BD-4725-8F75-627994685138",
        displayName: "Herman Guacamole",
        avatarUrl:
            "https://cdn.images.express.co.uk/img/dynamic/galleries/x701/152480.jpg",
      ),
    ]);
  }

  Future markMessageRead(MarkMessageReadDTO data) async {
    return null;
  }

  int currentId = 6;
  Future<SendMessageResultDTO> sendMessage(Message message) async {
    return new SendMessageResultDTO(currentId++, RequestResult.SUCCESS);
  }
}
