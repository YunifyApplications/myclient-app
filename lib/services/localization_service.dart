import 'dart:async' show Future;
import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;

enum Tenant { PHI, RUG }

/// This class is responsible for loading localization files and retrieving localized text values.
class LocalizationService {
  static Tenant tenant = Tenant.RUG;
  static Map localeValues;

  // empty constructor
  LocalizationService();

  /// Asynchronously loads the localization file for the selected tenant from the bundled assets.
  static Future load() async {
    var url = '';
    switch (tenant) {
      case Tenant.PHI:
        url = 'assets/localization/nl-nl-phi.json';
        break;
      case Tenant.RUG:
        url = 'assets/localization/nl-nl-rug.json';
        break;
    }

    /// load file async and set locale array when retrieved
    await rootBundle.loadString(url).then((result) {
      localeValues = jsonDecode(result);
    });
  }

  /// get a localized value of the given identifier key from the localization dictionary.
  /// returns the localized value on success, or an error message if the identifier is not found.
  static String get(String identifier) {
    if (localeValues != null) {
      if (localeValues.containsKey(identifier)) {
        return localeValues[identifier];
      } else {
        return "[NOT FOUND]";
      }
    } else {
      return "[NOT FOUND]";
    }
  }
}
