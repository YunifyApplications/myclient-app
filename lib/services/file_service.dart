import 'dart:io';

import 'package:path_provider/path_provider.dart';

class FileService {
  static Future<File> writeCacheFile(String dir, String text) async {
    final file = await _localFile(dir);

    // Write the file
    return file.writeAsString(text);
  }

  static Future<void> deleteCacheFile(String dir) async {
    if (await cacheFileExists(dir)) {
      final file = await _localFile(dir);
      return file.deleteSync();
    }
  }

  static Future<String> readCacheFile(String dir) async {
    try {
      final file = await _localFile(dir);

      // Read the file
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      return null;
    }
  }

  static Future<bool> cacheFileExists(String dir) async {
    final file = await _localFile(dir);
    return file.exists();
  }

  static Future<String> get _localPath async {
    final directory = await getTemporaryDirectory();

    return directory.path;
  }

  static Future<File> _localFile(String dir) async {
    final path = await _localPath;
    return File('$path/$dir');
  }
}
