import 'package:flutter/material.dart';
import 'dart:async';
import 'main.dart';
import 'registry.dart';
import 'UserForm.dart';

// page
class formPage extends StatefulWidget {
  formPage({Key key}) : super(key: key);

  @override
  _formPageState createState() => new _formPageState();
}

// state
class _formPageState extends State<formPage> {
  final _nameController = TextEditingController();
  final _ageController = TextEditingController();
  final _favColorController = TextEditingController();

  void _submit() async {
    String name = _nameController.text;
    String age = _ageController.text;
    String favColor = _favColorController.text;

    UserForm userForm = new UserForm();
    userForm.name = name;
    userForm.age = age;
    userForm.favColor = favColor;

    db.insert(userForm);

    //print("submit");

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("newForm"),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Column(
          children: <Widget>[
            new Text("Name"),
            // login form
            new TextField(
              controller: _nameController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "John"
              ),
            ),
            new Text("Age"),
            new TextField(
              controller: _ageController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "21"
              ),
            ),
            new Text("Favorite color"),
            new TextField(
              controller: _favColorController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Blue"
              ),
            ),
            new RaisedButton(
                child: new Text("Submit"),
                onPressed: (){
                  _submit();
                }
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    _ageController.dispose();
    _favColorController.dispose();
    super.dispose();
  }


}