import 'package:flutter_string_encryption/flutter_string_encryption.dart';
import 'main.dart';

// this is a form which stores a user's name, age and favorite color.
class UserForm {
  int id;
  String name;
  String age;
  String favColor;

  UserForm();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map<String, dynamic>();
    if (id != null) {
      map["userFormId"] = id;
    }
    map["name"] = name;
    map["age"] = age;
    map["favColor"] = favColor;

    return map;
  }

  UserForm.fromMap(Map<String, dynamic> map) {
    this.id = map["userFormId"];
    this.name = map["name"];
    this.age = map["age"];
    this.favColor = map["favColor"];
  }

  UserForm.map(dynamic map) {
    this.id = map["userFormId"];
    this.name = map["name"];
    this.age = map["age"];
    this.favColor = map["favColor"];
  }

  Future<void> encrypt() async {
    this.name = await cryptor.encrypt(this.name, encryptionKey);
    this.age = await cryptor.encrypt(this.age, encryptionKey);
    this.favColor = await cryptor.encrypt(this.favColor, encryptionKey);
  }

  Future<void> decrypt() async {
    this.name = await cryptor.decrypt(this.name, encryptionKey);
    this.age = await cryptor.decrypt(this.age, encryptionKey);
    this.favColor = await cryptor.decrypt(this.favColor, encryptionKey);
  }
}