import 'package:flutter/material.dart';
import 'dart:ui';
import 'grid.dart';
import 'main.dart';

// page
class drawPatternPage extends StatefulWidget {
  drawPatternPage({Key key}) : super(key: key);
  @override
  _drawPatternPageState createState() => new _drawPatternPageState();
}

// state
class _drawPatternPageState extends State<drawPatternPage> with WidgetsBindingObserver {
  Offset _p = new Offset(0.0, 0.0);
  int draw = 0;
  Grid grid = new Grid(3, 3, 35, 50);
  int gridWidth = 3;
  int gridHeight = 3;
  double gridPadding = 50.0;
  String error = "";
  String remainingAttemptsMessage = HTTPService.getRemainingPinLoginAttempts().toString() + " attempts remaining";

  int attempts = 5;
  int remainingAttempts = 5;

  // the user has stopped drawing a pattern
  void drawEnd() {
    // decide whether the pattern is valid(long enough)
    // check if the pattern is valid
    if (HTTPService.loginWithPin(grid.pin)) {
      setState(() {
        error = "";
      });

      encryptionKey = HTTPService.getKey();
      Navigator.pushNamedAndRemoveUntil(context, "/dashboard", (Route<dynamic> route) => false);
    }
    else {
      remainingAttempts = HTTPService.getRemainingPinLoginAttempts();

      if (remainingAttempts == null || remainingAttempts <= 0) {
        // logout
        //HTTPService.logOut();
        Navigator.pushNamedAndRemoveUntil(context, "/login", (Route<dynamic> route) => false);
      }

      setState(() {
        error = "Wrong pattern";
        if (remainingAttempts == null) {
          remainingAttemptsMessage = "0 attempts remaining";
        }
        else {
          remainingAttemptsMessage = HTTPService.getRemainingPinLoginAttempts().toString() + " attempts remaining";
        }

      });
      grid.pin = new List<int>();
    }
  }

  void pan(Offset offset) {
    grid.pan(offset);
  }


  Widget gridBuilder(BuildContext context) {
    return new LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      // Calculate grid element dimensions
      int nodeWidth = ((constraints.maxWidth ~/ gridWidth) ~/ (gridWidth - 1));
      int spaceBetweenNodes = ((constraints.maxWidth - (nodeWidth * gridWidth)) ~/ (gridWidth - 1)).toInt();
      grid.changeGridDimensions(gridWidth, gridWidth, nodeWidth, spaceBetweenNodes);

      RenderBox box = context.findRenderObject();
      assert(box != null);
      return new Container(
          height: constraints.maxWidth,
          width: constraints.maxWidth,
          child: new Builder(builder: (context) {
            return new GestureDetector(
                onPanDown: (details) {
                  RenderBox object = context.findRenderObject();
                  _p = object.globalToLocal(details.globalPosition);
                  draw = 1;
                  pan(_p);
                },
                onPanUpdate: (DragUpdateDetails details) {
                  setState(() {
                    RenderBox object = context.findRenderObject();
                    Offset temp = object.globalToLocal(details.globalPosition);
                    _p = new Offset(temp.dx, temp.dy);
                    pan(_p);
                  });
                },
                onPanEnd: (details) {
                  setState(() {
                    draw = 0;
                    drawEnd();
                  });
                },
                child: new CustomPaint(
                  painter: new MyPainter(
                      p: _p,
                      draw: draw,
                      grid: grid
                  ),
                  size: Size.infinite,
                )
            );
          })
      );
    });
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text("Draw pattern"),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Column(
          children: <Widget>[
            new Text("Draw pattern", style: TextStyle(fontSize: 30.0)),
            new Text(error, style: new TextStyle(color: Color.fromARGB(255, 255, 0, 0))),
            new Text(remainingAttemptsMessage/*, style: new TextStyle(color: Color.fromARGB(255, 255, 0, 0))*/),
            new Container(
              //height: 475.0,
              //width: 500.0,
              child:
                new Padding(
                  padding: new EdgeInsets.all(gridPadding),
                  child: gridBuilder(context),
                )
            ),
          ],
        )
      )
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch(state) {
      case AppLifecycleState.inactive:
        break;

      case AppLifecycleState.paused:
        HTTPService.closeApp();
        encryptionKey = "empty";

        if (HTTPService.canLoginWithPin()) {
          Navigator.pushNamedAndRemoveUntil(context, "/pattern", (Route<dynamic> route) => false);
        }
        else {
          Navigator.pushNamedAndRemoveUntil(context, "/login", (Route<dynamic> route) => false);
        }

        break;

      case AppLifecycleState.resumed:
        break;

      case AppLifecycleState.suspending:
        break;

    }
  }
}
