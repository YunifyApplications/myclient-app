import 'package:flutter/material.dart';

class MyPainter extends CustomPainter {
  Offset p;
  int draw;
  Grid grid;

  MyPainter({this.p,this.draw,this.grid});

  @override
  void paint(Canvas canvas, Size size) {
    double nodePaintStrokeWidth = 6.0;

    Paint linePaint = new Paint()
      ..color = Color.fromARGB(255, 46, 204, 113)
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10.0;

    Paint nodePaint = new Paint()
      ..color = Color.fromARGB(255, 52, 73, 94)
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = nodePaintStrokeWidth;

    Paint filledNodePaint = new Paint()
      ..color = Color.fromARGB(255, 46, 204, 113)
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = grid.nodes[0].width.toDouble() / 2;

    // draw nodes
    for (int i = 0; i < grid.nodes.length; i++) {
      canvas.drawCircle(
          new Offset(
              grid.nodes[i].location.dx + (grid.nodes[i].width / 2) + (nodePaintStrokeWidth / 2),
              grid.nodes[i].location.dy + (grid.nodes[i].width / 2) + (nodePaintStrokeWidth / 2)
          ),
          grid.nodes[i].width.toDouble() / 2,
          nodePaint
      );
    }

    // drawConnected nodes
    for (int i = 0; i < grid.pin.length; i++) {
      canvas.drawCircle(
          new Offset(
            grid.nodes[grid.pin[i]].location.dx + (grid.nodes[grid.pin[i]].width / 2) + (nodePaintStrokeWidth / 2),
            grid.nodes[grid.pin[i]].location.dy + (grid.nodes[grid.pin[i]].width / 2) + (nodePaintStrokeWidth / 2)
          ),
          //grid.nodes[i].width.toDouble() / 2,
          1.0,
          filledNodePaint
      );
    }

    // draw unconnected line
    if (grid.pin.length > 0 && draw == 1 && grid.readOnly == false) {
      canvas.drawLine(
          new Offset(
              grid.nodes[grid.pin.last].location.dx + (grid.nodes[grid.pin.last].width / 2) + (nodePaintStrokeWidth / 2),
              grid.nodes[grid.pin.last].location.dy + (grid.nodes[grid.pin.last].width / 2) + (nodePaintStrokeWidth / 2)
          ),
          p,
          linePaint
      );
    }

    // draw connections between nodes
    for (int i = 0; i < grid.pin.length - 1; i++) {
      canvas.drawLine(
          new Offset(
              grid.nodes[grid.pin[i]].location.dx + (grid.nodes[grid.pin[i]].width / 2) + (nodePaintStrokeWidth / 2),
              grid.nodes[grid.pin[i]].location.dy + (grid.nodes[grid.pin[i]].width / 2) + (nodePaintStrokeWidth / 2)
          ),
          new Offset(
              grid.nodes[grid.pin[i + 1]].location.dx + (grid.nodes[grid.pin[i + 1]].width / 2) + (nodePaintStrokeWidth / 2),
              grid.nodes[grid.pin[i + 1]].location.dy + (grid.nodes[grid.pin[i + 1]].width / 2) + (nodePaintStrokeWidth / 2)
          ),
          linePaint
      );
    }
  }

  @override
  bool shouldRepaint(MyPainter oldDelegate) => oldDelegate.p != p;

}

class Grid {
  List<Node> nodes;
  List<int> pin;
  bool readOnly = false;

  Grid(int width, int height, int nodeWidth, int spaceBetweenNodes) {
    nodes = new List<Node>();
    pin = new List<int>();

    int index = 0;
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        Offset nodeLocation = new Offset(
            ((j * spaceBetweenNodes) + (j * nodeWidth)).toDouble(),
            ((i * spaceBetweenNodes) + (i * nodeWidth)).toDouble()
        );
        Node newNode = new Node(nodeLocation, nodeWidth, index);
        nodes.add(newNode);
        index++;
      }
    }
  }

  void changeGridDimensions(int width, int height, int nodeWidth, int spaceBetweenNodes) {
    int index = 0;
    for (int i = 0; i < width; i++) {
      for (int j = 0; j < height; j++) {
        Offset nodeLocation = new Offset(
            ((j * spaceBetweenNodes) + (j * nodeWidth)).toDouble(),
            ((i * spaceBetweenNodes) + (i * nodeWidth)).toDouble()
        );
        nodes[index].width = nodeWidth;
        nodes[index].location = nodeLocation;
        index++;
      }
    }
  }


  void pan(Offset offset) {
    if (readOnly == false) {
      for (int i = 0; i < nodes.length; i++) {
        if (
        offset.dx >= nodes[i].location.dx &&
            offset.dx <= nodes[i].location.dx + nodes[i].width &&
            offset.dy >= nodes[i].location.dy &&
            offset.dy <= nodes[i].location.dy + nodes[i].width
        ) {
          bool foundIndex = false;
          for (int j = 0; j < pin.length; j++) {
            if (pin[j] == nodes[i].index) {
              foundIndex = true;
            }
          }
          if (foundIndex == false) {
            pin.add(nodes[i].index);
          }
        }
      }
    }
  }
}

class Node {
  Offset location;
  int width;
  int index;

  Node(Offset _location, int _width, int _index) {
    location = _location;
    width = _width;
    index = _index;
  }
}