import 'package:flutter/material.dart';
import 'dart:async';
import 'menu.dart';
import 'MocHTTPService.dart';
import 'registry.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as Path;
import 'Database.dart';
import 'dart:convert';
import 'drawNewPatternPage.dart';
import 'drawPatternPage.dart';

import 'package:flutter_string_encryption/flutter_string_encryption.dart';



void main() => runApp(new MyApp());


String encryptionKey = "empty";
myDatabase db;

PlatformStringCryptor cryptor = new PlatformStringCryptor();

MocHTTPService HTTPService = new MocHTTPService();

// main
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(
        title: 'Login',
        reg: new registry()
      ),
      routes: <String, WidgetBuilder>{
        "/dashboard": (BuildContext context) => new MyNewPage(title: "menu",),
        "/login": (BuildContext context) => new MyHomePage(title: "login", reg: new registry()),
        "/pattern": (BuildContext context) => new drawPatternPage()
      },
    );
  }
}



// page
class MyHomePage extends StatefulWidget {
  MyHomePage({
    Key key,
    this.title,
    this.reg
  }) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  final registry reg;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

//state
class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  String _registryContent = '';
  String _loginError = '';

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();


  Future<Null> _dialogPrint(String title) async {
    await showDialog(
        context: context,
        child: new SimpleDialog(
          title: new Text(title),
          children: <Widget>[
            //new Text("test test test..."), // how to inherit style (padding)?
            new SimpleDialogOption(
              onPressed: (){Navigator.pop(context, false);},
              child: const Text("Close"),
            )
          ],
        )
    );
  }

  Future<String> generateDatabasePath(String username) async {
    String path = await getDatabasesPath();
    return Path.join(path, username + ".db");
  }

  void _selectDatabase(String username) async {
    String encodedRegistry = await widget.reg.read();
    Map<String, dynamic> decodedRegistry;
    if (encodedRegistry == "error") {
      // Create new registry
      decodedRegistry = new Map<String, String>();
      // Add username to new registry
      decodedRegistry[username] = (await generateDatabasePath(username));
      // Save registry
      widget.reg.write(json.encode(decodedRegistry));

    }
    else {
      // Decode registry
      decodedRegistry = json.decode(encodedRegistry);

      // Check if name is in local registry
      if (decodedRegistry[username] == null) {
        // Add name and new dbPath to registry
        decodedRegistry[username] = (await generateDatabasePath(username));
        // Save registry
        widget.reg.write(json.encode(decodedRegistry));
      }
    }

    db = new myDatabase(decodedRegistry[username]);
  }

  void _login() {
    String username = _usernameController.text;
    String password = _passwordController.text;

    // check credentials
    if (username != "" && password != "") {
      String login = HTTPService.login(username, password);

      if (login == "") {
        // save encryption key in memory
        encryptionKey = HTTPService.getKey();

        // select database
        _selectDatabase(username);

        setState(() {
          _loginError = "";
        });

//        List<int> pin = HTTPService.getPin();
//        if (pin.length == 0) {
//          // goto drawNewPatternPage
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) => drawNewPatternPage(
//            )),
//          );
//        }
//        else {
//          // goto DrawPatternPage
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) => drawPatternPage(
//            )),
//          );
//        }

        if (HTTPService.hasPin() == false) {
          // goto drawNewPatternPage
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => drawNewPatternPage(
            )),
          );
        }
        else {
//          // goto DrawPatternPage
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) => drawPatternPage(
//            )),
//          );
          Navigator.pushNamedAndRemoveUntil(context, "/dashboard", (Route<dynamic> route) => false);
        }

//        // go to dashboard
//        Navigator.push(
//          context,
//          MaterialPageRoute(builder: (context) => MyNewPage(
//            title: username,
//          )),
//        );

      }
      else {
        // 'login' contains the error code
        setState(() {
          _loginError = login;
        });
      }
    }
    else {
      // username or password is empty
      setState(() {
        _loginError = 'username or password is empty';
      });
    }

  }

  void _readFromFile() {
    widget.reg.read().then((String value) {
      setState(() {
        _registryContent = value;
      });
    });
  }

  void _writeToFile() {
    String username = _usernameController.text;
    widget.reg.write(username);
  }

  void _generateKey() async {
    print(await cryptor.generateRandomKey());
    print(await cryptor.generateRandomKey());
    print(await cryptor.generateRandomKey());
  }
  
  void _encryptionTest() async {
    encryptionKey = await cryptor.generateRandomKey();
    String str = "Hello World";
    String encryptedStr = await cryptor.encrypt(str, encryptionKey);
    print(encryptedStr);
    print(await cryptor.decrypt(encryptedStr, encryptionKey));
  }


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(widget.title),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Column(
          children: <Widget>[
            // login form
            new TextField(
              controller: _usernameController,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Username"
              ),
            ),
            new TextField(
              controller: _passwordController,
              obscureText: true,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Password"
              ),
            ),
            new Text(_loginError, style: new TextStyle(color: Color.fromARGB(255, 255, 0, 0))),
            new RaisedButton(
                child: new Text("Login"),
                onPressed: (){
                  _login();
                }
            ),
            /*
            new RaisedButton(
                child: new Text("encryption test"),
                onPressed: (){
                  _encryptionTest();
                }
            ),
            */
            /*
            new RaisedButton(
                child: new Text("Generate key"),
                onPressed: (){
                  _generateKey();
                }
            ),
            */
            /*
            new RaisedButton(
                child: new Text("Write username to file"),
                onPressed: (){
                  _writeToFile();
                }
            ),
            new RaisedButton(
                child: new Text("Read file content"),
                onPressed: (){
                  _readFromFile();
                }
            ),
            new Text(_registryContent),
            */


            /*
            new RaisedButton(
                child: new Text('Click me'),
                onPressed: (){_dialogPrint("test");}
            )
            */
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    widget.reg.read().then((String value) { // change to method: _ReadFromFile
      _registryContent = value;
    });
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    //print("*** State changed: ${state.toString()}");
    print("test1");

    switch(state) {
      case AppLifecycleState.inactive:
        break;

      case AppLifecycleState.paused:
        //while (Navigator.canPop(context)) {
        //  Navigator.pop(context);
        //}
        //Navigator.of(context).pushReplacementNamed('/login');
        HTTPService.closeApp();
        encryptionKey = "empty";

        print("test2");
        if (HTTPService.canLoginWithPin()) {
          Navigator.pushNamedAndRemoveUntil(context, "/pattern", (Route<dynamic> route) => false);
        }
        else {
          Navigator.pushNamedAndRemoveUntil(context, "/login", (Route<dynamic> route) => false);
        }

        break;

      case AppLifecycleState.resumed:
        break;

      case AppLifecycleState.suspending:
        break;

    }
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
}
