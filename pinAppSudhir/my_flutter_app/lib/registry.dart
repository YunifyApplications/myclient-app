import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class registry {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;

    return File('$path/registry.txt');
  }

  Future<File> write(String content) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsString(content);
  }

  Future<String> read() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      //return "Error: " + e.toString();
      return "error";
    }
  }

  Future<String> getFilePath() {
    return _localPath;
  }
}