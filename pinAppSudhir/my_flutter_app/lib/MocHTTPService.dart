import 'HTTPService.dart';
import 'dart:math';
import 'dart:core';

class MocHTTPService implements HTTPService {

  int primaryTokenShelfLife = 3650;
  int secondaryTokenShelfLife = 3650;

  int pinLoginAttempts = 5;

  // mock user database
  List<User> users;

  int userId;
  bool loggedIn = false;

  MocHTTPService() {
    users = new List();
    User user;

    user = new User();
    user.id = 0;
    user.username = "sudhir";
    user.password = "root";
    user.encryptionKey = "36SuKJQVhjsoLjyQ40kvFw==:xrsX9t3o7h03tnga26t/xGHOrs++8kojCz83eba9INE=";
//    user.pin.add(0);
//    user.pin.add(1);
//    user.pin.add(2);
//    user.pin.add(3);
    users.add(user);

    user = new User();
    user.id = 1;
    user.username = "Jan";
    user.password = "root";
    user.encryptionKey = "GOuVdJ/OZA3KCf5Kyv+WkQ==:OO8ogd501UwY1bAq9X80ieOztENYrIUIG4rCb5yTV60=";
    users.add(user);

    user = new User();
    user.id = 2;
    user.username = "Bob";
    user.password = "root";
    user.encryptionKey = "FTMfalzcCy4zSiEp0P6zdg==:0Tq/69Gk5Yyq4OPqiIESU+NsjzEKt9T5EBfEVxCx7ik=";
    users.add(user);
  }

  String login(String username, String password) {
    bool usernameFound = false;
    bool correctPassword = false;

    users.forEach((user) {
      if (user.username == username) {
        usernameFound = true;
        if (user.password == password) {
          // login
          correctPassword = true;
          loggedIn = true;
          userId = user.id;
          user.primaryLoginToken = new Token(DateTime.now().add(new Duration(days: primaryTokenShelfLife)));
          if (hasPin()) {
            user.secondaryLoginToken = new Token(DateTime.now().add(new Duration(days: primaryTokenShelfLife)));
            user.remainingPinLoginAttempts = pinLoginAttempts;
          }
        }
      }
    });
    if (usernameFound == true && correctPassword == true) {
      return "";
    }
    else if (usernameFound == true && correctPassword == false) {
      return "Wrong password";
    }
    else if (usernameFound == false) {
      return "User not found";
    }

    return "Something went wrong";
  }

  String getKey() {
    String key = "";

    if (loggedIn == true) {
      users.forEach((user) {
        if (user.id == userId) {
          key = user.encryptionKey;
        }
      });
    }

    return key;
  }

  List<int> getPin() {
    List<int> pin = new List<int>();

    if (loggedIn == true) {
      users.forEach((user) {
        if (user.id == userId) {
          pin = user.pin;
        }
      });
    }

    return pin;
  }

  bool validatePin(List<int> pin /*add secondary token*/) {
    if (comparePin(users[userId].pin, pin)) {
      return true;
    }
    return false;
  }

  void setPin(List<int> pin) {
    if (loggedIn == true && validatePrimaryToken(users[userId].primaryLoginToken)) {
      users.forEach((user) {
        if (user.id == userId) {
          user.pin = pin;
          user.secondaryLoginToken = new Token(DateTime.now().add(new Duration(days: secondaryTokenShelfLife)));
          user.remainingPinLoginAttempts = pinLoginAttempts;
        }
      });
    }
  }

  void logOut() {
    loggedIn = false;
    users[userId].primaryLoginToken = null;
    users[userId].secondaryLoginToken = null;
  }

  void closeApp() {
    loggedIn = false;
    users[userId].primaryLoginToken = null;
    //users[userId].secondaryLoginToken = null;
  }

  bool canLoginWithPin() {
    if (userId != null) {
      if (users[userId].secondaryLoginToken != null && hasPin()) {
        if (validateSecondaryToken(users[userId].secondaryLoginToken)) {
          return true;
        }
      }
    }
    return false;
  }

  bool loginWithPin(List<int> pin) {
    if (canLoginWithPin()) {
      if (comparePin(users[userId].pin, pin)) {
        // login
        loggedIn = true;
        users[userId].primaryLoginToken = new Token(DateTime.now().add(new Duration(days: primaryTokenShelfLife)));
        users[userId].remainingPinLoginAttempts = pinLoginAttempts;
        return true;
      }
      else {
        users[userId].remainingPinLoginAttempts--;
        if (users[userId].remainingPinLoginAttempts <= 0) {
          // logout user
          logOut();
        }
      }
    }
    return false;
  }

  int getRemainingPinLoginAttempts() {
    if (canLoginWithPin()) {
      return users[userId].remainingPinLoginAttempts;
    }
    return null;
  }

  bool validatePrimaryToken(Token token) {
    if (users[userId].primaryLoginToken.key == token.key && DateTime.now().isBefore(token.expiryDate)) {
      return true;
    }
    return false;
  }

  bool validateSecondaryToken(Token token) {
    if (users[userId].secondaryLoginToken.key == token.key && DateTime.now().isBefore(token.expiryDate)) {
      return true;
    }
    return false;
  }

  bool comparePin(List<int> pin1, List<int> pin2) {
    bool match = true;
    if (pin1.length != pin2.length) {
      match = false;
    }
    else {
      for (int i = 0; i < pin1.length; i++) {
        if (pin1[i] != pin2[i]) {
          match = false;
        }
      }
    }
    return match;
  }

  bool hasPin() {
    //print(((users[userId].pin.length <= 0) == false));
    if (users[userId].pin != null && users[userId].pin.length > 0) {
      return true;
    }
    return false;
  }


}

class User {
  int id;
  String username;
  String password;
  String encryptionKey;
  List<int> pin = new List<int>();
  Token primaryLoginToken;
  Token secondaryLoginToken;
  int remainingPinLoginAttempts;
}

class Token {
  String key;
  DateTime expiryDate;

  Token(DateTime ExpiryDate) {
    expiryDate = ExpiryDate;
    key = generateRandomString(64);
  }

  // this method only generates numbers
  String generateRandomString(int length) {
    Random random = new Random();
    String randomStr = "";
    for (int i = 0; i < length; i++) {
      randomStr += random.nextInt(9).toString();
    }
    return randomStr;
  }
}