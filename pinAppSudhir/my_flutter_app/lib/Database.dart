import 'package:flutter/material.dart';
import 'dart:async';
import 'menu.dart';
import 'MocHTTPService.dart';
import 'registry.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as Path;
import 'main.dart' as main;
import 'UserForm.dart';

class myDatabase {
  Database _database;

  myDatabase(String path) {
    _selectDatabase(path);
  }

  void _selectDatabase(String path) async {
    _database = await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async { // onCreate is only called
        // when the database is actually created and not when it is simply
        // loaded
        await db.execute('''
          create table userFormTable (
            userFormId integer primary key autoincrement,
            name text,
            age text,
            favColor text
          )
        ''');
      }
    );
  }

  Future insert(UserForm userForm) async {
    await userForm.encrypt();
    return _database.insert("userFormTable", userForm.toMap());
  }

  Future<List<UserForm>> getAll() async {
    List<UserForm> userForms = new List<UserForm>();

    List<Map<String, dynamic>> querryResult = await _database.query(
        "userFormTable",
        columns: [
          "userFormId",
          "name",
          "age",
          "favColor"
        ]
    );

    for (int i = 0; i < querryResult.length; i++) {
      UserForm form = UserForm.fromMap(querryResult[i]);
      print(querryResult[i]);
      await form.decrypt();
      userForms.add(form);
    }

    return userForms;
  }

  Future<UserForm> getById(int id) async {
    List<Map<String, dynamic>> querryResult = await _database.query(
        "userFormTable",
        columns: [
          "userFormId",
          "name",
          "age",
          "favColor"
        ],
        where: 'userFormId = ?',
        whereArgs: [id]
    );

    if (querryResult.length > 0) {
      UserForm form = new UserForm.fromMap(querryResult.first);
      await form.decrypt();
      return form;
    }

    return null;
  }

  Future<int> update(UserForm userForm) async {
    await userForm.encrypt();
    return await _database.update(
        "userFormTable",
        userForm.toMap(),
        where: "userFormId = ?",
        whereArgs: [userForm.id]
    );
  }

  Future<int> delete(int id) async {
    return await _database.delete(
        "userFormTable",
        where: "userFormId = ?",
        whereArgs: [id]
    );
  }

}

