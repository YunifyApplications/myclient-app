abstract class HTTPService {
  String login(String username, String password);
  String getKey();
}