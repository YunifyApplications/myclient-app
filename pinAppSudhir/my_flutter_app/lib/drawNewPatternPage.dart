import 'package:flutter/material.dart';
import 'main.dart';
import 'grid.dart';

// page
class drawNewPatternPage extends StatefulWidget {
  drawNewPatternPage({Key key}) : super(key: key);
  @override
  _drawNewPatternPageState createState() => new _drawNewPatternPageState();
}

// state
class _drawNewPatternPageState extends State<drawNewPatternPage> {

  String title = "Draw new pattern";
  String error = "";
  List<int> pin;
  bool pinSet = false;


  Offset _p = new Offset(0.0, 0.0);
  int draw = 0;
  int gridWidth = 3;
  int gridHeight = 3;
  Grid grid = new Grid(3, 3, 35, 50);
  double gridPadding = 50.0;

  bool clearButtonEnable = false;
  bool continueButtonEnable = false;

  // the user has stopped drawing a pattern
  void drawEnd() {
    /*
    if (pinSet && !comparePin(grid.pin, pin)) {
      setState(() {
        error = "patterns do not match";
      });
      print("test");
    }
    */
    /*
    if (pin != null && grid.pin != null && !comparePin(grid.pin, pin)) {
      print("test1");
    }
    else {
      print("test2");
    }
    */
    // decide whether the pattern is valid(long enough)
    // if the pattern is set, enable the confirm button and the clear button
    // if the pin is not valid reset it
    if (grid.pin.length < 4) {
      // pin is to short
      setState(() {
        error = "Pattern is to short!";
      });
      // clear pin
      grid.pin = new List<int>();
    }
    else {
      // pin is valid
      setState(() {
        error = "";
      });
      grid.readOnly = true;

      if (pinSet && !comparePin(grid.pin, pin)) {
        error = "patterns do not match";
        // enable clear button
        clearButtonEnable = true;
      }
      else {
        // enable confirm button
        continueButtonEnable = true;
        // enable clear button
        clearButtonEnable = true;
      }

    }
  }

  // confirm button
  void confirm() {
    // if the pattern is set, move to the next screen
    // store the pin in memory
    if (pin == null) {
      // pin is not set yet (the pin will now be set)
      pin = grid.pin;
      // change UI to 'confirmed' screen
      // reset grid
      grid.pin = new List<int>();
      grid.readOnly = false;
      // change UI
      title = "Confirm pattern";

      // disable clear button
      clearButtonEnable = false;
      // disable confirm button
      continueButtonEnable = false;

      pinSet = true;
    }
    else {
      // pin is set (the pin will now be confirmed)
      if (comparePin(grid.pin, pin)) {
        // pin confirmed
        // store user's pin in database
        HTTPService.setPin(grid.pin);
        // goto dashboard
        Navigator.pushNamedAndRemoveUntil(context, "/dashboard", (Route<dynamic> route) => false);
      }
      else {
        // pins do not match
        error = "Patterns do not match";
        // do not clear 'pin' variable, ask user to draw pin again
        grid.pin = new List<int>();
        grid.readOnly = false;
      }
    }
  }

  // clear button
  void clear() {
    // clear the pattern if it is set
    grid.pin = new List<int>();
    grid.readOnly = false;

    // disable clear button
    clearButtonEnable = false;
    // disable confirm button
    continueButtonEnable = false;
  }

  void pan(Offset offset) {
    grid.pan(offset);
  }

  bool comparePin(List<int> pin1, List<int> pin2) {
    bool match = true;
    if (pin1.length != pin2.length) {
      match = false;
    }
    else {
      for (int i = 0; i < pin1.length; i++) {
        if (pin1[i] != pin2[i]) {
          match = false;
        }
      }
    }
    return match;
  }


  Widget gridBuilder(BuildContext context) {
    return new LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      // Calculate grid element dimensions
      int nodeWidth = ((constraints.maxWidth ~/ gridWidth) ~/ (gridWidth - 1));
      int spaceBetweenNodes = ((constraints.maxWidth - (nodeWidth * gridWidth)) ~/ (gridWidth - 1)).toInt();
      grid.changeGridDimensions(gridWidth, gridWidth, nodeWidth, spaceBetweenNodes);

      RenderBox box = context.findRenderObject();
      assert(box != null);
      return new Container(
          height: constraints.maxWidth,
          width: constraints.maxWidth,
          child: new Builder(builder: (context) {
            return new GestureDetector(
                onPanDown: (details) {
                  RenderBox object = context.findRenderObject();
                  _p = object.globalToLocal(details.globalPosition);
                  draw = 1;
                  pan(_p);
                },
                onPanUpdate: (DragUpdateDetails details) {
                  setState(() {
                    RenderBox object = context.findRenderObject();
                    Offset temp = object.globalToLocal(details.globalPosition);
                    _p = new Offset(temp.dx, temp.dy);
                    pan(_p);
                  });
                },
                onPanEnd: (details) {
                  setState(() {
                    draw = 0;
                    drawEnd();
                  });
                },
                child: new CustomPaint(
                  painter: new MyPainter(
                      p: _p,
                      draw: draw,
                      grid: grid
                  ),
                  size: Size.infinite,
                )
            );
          })
      );
    });
  }

  Widget clearButton() {
    if (clearButtonEnable) {
      return new RaisedButton(
        child: new Text("Clear"),
        onPressed: () {
          setState(() {
            // clear the pin and draw again
            clear();
          });
        },
        color: Color.fromARGB(255, 231, 76, 60),

      );
    }
    else {
      return new RaisedButton(
        child: new Text("Clear"),
        onPressed: null,
        color: Color.fromARGB(255, 231, 76, 60),
      );
    }
  }

  Widget continueButton() {
    if (continueButtonEnable) {
      return new RaisedButton(
        child: new Text("Continue"),
        onPressed: () {
          setState(() {
            confirm();
          });
        },
        color: Color.fromARGB(255, 46, 204, 113),
      );
    }
    else {
      return new RaisedButton(
        child: new Text("Continue"),
        onPressed: null,
        color: Color.fromARGB(255, 46, 204, 113),
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text(title),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Column(
          children: <Widget>[
            new Text(title, style: TextStyle(fontSize: 30.0)),
            new Text(error, style: new TextStyle(color: Color.fromARGB(255, 255, 0, 0))),
            new Container(
              padding: new EdgeInsets.fromLTRB(gridPadding, gridPadding, gridPadding, 0.0),
              child: new Column(
                children: <Widget>[
                  gridBuilder(context),
                  new Padding(padding: new EdgeInsets.fromLTRB(0.0, gridPadding, 0.0, 0.0),
                    child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          new ButtonTheme(
                            height: 50.0,
                            child: clearButton(),
                          ),new ButtonTheme(
                            height: 50.0,
                            child: continueButton(),
                          ),
                        ]),
                  )

                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}