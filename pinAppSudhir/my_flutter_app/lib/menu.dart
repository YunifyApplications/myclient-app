import 'package:flutter/material.dart';
import 'dart:async';
import 'main.dart';
import 'UserForm.dart';
import 'form.dart';

// page
class MyNewPage extends StatefulWidget {
  MyNewPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyNewPageState createState() => new _MyNewPageState();

}

// state
class _MyNewPageState extends State<MyNewPage> with WidgetsBindingObserver {
  String databaseOutput = "";

  Future<String> get() async {
    String output = "";
    List<UserForm> userForms = await db.getAll();
    if (userForms.length > 0) {
      for (int i = 0; i < userForms.length; i++) {
        output += userForms[i].toMap().toString() + ";\n";
      }
    }
    else {
      output = "empty";
    }

    setState(() {
      databaseOutput = output;
    });

    print("output:\n" + output);
    return output;
  }

  Future<String> getById() async {
    String output = "";
    UserForm userForm = await db.getById(1);
    if (userForm != null) {
      output = userForm.toMap().toString();
    }
    else {
      output = "empty";
    }

    setState(() {
      databaseOutput = output;
    });

    print("output: " + output);
    return output;
  }

  Future<String> update() async {
    String output = "";
    UserForm userForm = await db.getById(2);
    userForm.favColor = "purple";
    output = (await db.update(userForm)).toString();

    setState(() {
      databaseOutput = output;
    });

    print("output: " + output);
    return output;
  }

  Future<String> delete() async {
    String output = "";
    output = (await db.delete(6)).toString();

    setState(() {
      databaseOutput = output;
    });

    print("output: " + output);
    return output;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Container(
        padding: new EdgeInsets.all(32.0),
        child: new Column(
          children: <Widget>[
            //new Text("Menu"),
            //new Text("encryptionKey: " + encryptionKey),
            new Text("database output:\n" + databaseOutput),
            /*
            new RaisedButton(
                child: new Text("insert"),
                onPressed: (){
                  UserForm form = new UserForm();
                  //form.id = 2;
                  form.name = "random";
                  form.age = "19";
                  form.favColor = "weird color";
                  db.insert(form);
                }
            ),
            */
            new RaisedButton(
                child: new Text("get"),
                onPressed: (){
                  get();
                }
            ),
            /*
            new RaisedButton(
                child: new Text("get by id"),
                onPressed: (){
                  getById();
                }
            ),
            */
            /*
            new RaisedButton(
                child: new Text("update"),
                onPressed: (){
                  setState(() {
                    update().then((str) {
                      databaseOutput = str;
                    });
                  });
                }
            ),
            */
            /*
            new RaisedButton(
                child: new Text("delete"),
                onPressed: (){
                  setState(() {
                    delete().then((str) {
                      databaseOutput = str;
                    });
                  });
                }
            ),
            */
            new RaisedButton(
                child: new Text("newForm"),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => formPage()),
                  );
                }
            ),
            new RaisedButton(
                child: new Text("Logout"),
                onPressed: () {
                  HTTPService.logOut();
                  Navigator.pushNamedAndRemoveUntil(context, "/login", (Route<dynamic> route) => false);
                }
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    //print("*** State changed: ${state.toString()}");

    switch(state) {
      case AppLifecycleState.inactive:
        break;

      case AppLifecycleState.paused:
      //while (Navigator.canPop(context)) {
      //  Navigator.pop(context);
      //}
      //Navigator.of(context).pushReplacementNamed('/login');
        HTTPService.closeApp();
        encryptionKey = "empty";

        if (HTTPService.canLoginWithPin()) {
          Navigator.pushNamedAndRemoveUntil(context, "/pattern", (Route<dynamic> route) => false);
        }
        else {
          Navigator.pushNamedAndRemoveUntil(context, "/login", (Route<dynamic> route) => false);
        }

        break;

      case AppLifecycleState.resumed:
        break;

      case AppLifecycleState.suspending:
        break;

    }
  }
}